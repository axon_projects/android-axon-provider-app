package com.axon.axonprovider.Interfaces

import com.axon.axonprovider.Model.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface APiInterface {
    /*@Query -> GET*/
    /*@Path*/ // --> id

    /**
     * Provider Login
     */
    @FormUrlEncoded
    @POST("/api/account/authenticate")
    fun login(
        @Field("accountVerification") accountVerification: String,
        @Field("usernameOrEmailAddress") usernameOrEmailAddress: String,
        @Field("password") password: String,
        @Header("LanguageCode") language: String
    ): Call<LoginResponse>

    /**
     * Provider Types
     */
    @GET("/api/providerapp/GetProviderTypes")
    fun getProviderTypes(
        @Header("LanguageCode") language: String
    ): Call<ProviderTypesResponse>

    /**
     * Cities
     */
    @FormUrlEncoded
    @POST("/api/providerapp/cities")
    fun getCities(
        @Field("countryId") countryId: Int,
        @Header("LanguageCode") language: String
    ): Call<CitiesResponse>

    /**
     * Areas
     */
    @FormUrlEncoded
    @POST("/api/providerapp/areas")
    fun getAreas(
        @Field("cityId") cityId: Int,
        @Header("LanguageCode") language: String
    ): Call<AreasResponse>

    /**
     * Join US
     */
    @FormUrlEncoded
    @POST("/api/providerapp/JoinUs")
    fun postJoinUs(
        @Field("name") name: String,
        @Field("mobileNumber") mobileNumber: String,
        @Field("emailAddress") emailAddress: String,
        @Field("address") address: String,
        @Field("areaId") areaId: Int,
        @Field("providersTypeId") providersTypeId: Int,
        @Header("LanguageCode") language: String
    ): Call<JoinUsResponse>

    /**
     * Get Notifications
     */
    @GET("/api/providerapp/getnotifications")
    fun getNotifications(
        @Header("Authorization") authorization: String,
        @Header("LanguageCode") language: String
    ): Call<GetNotificationsResponse>

    /**
     * Previous Appointments
     */
    @FormUrlEncoded
    @POST("/api/providerapp/GetPreviousAppointments")
    fun postPreviousAppointments(
        @Field("memberCode") memberCode: String,
        @Field("reservationDate") reservationDate: String,
        @Field("pageNumber") pageNumber: Int,
        @Field("pageSize") pageSize: Int,
        @Header("Authorization") authorization: String,
        @Header("LanguageCode") language: String
    ): Call<PreviousAppointmentsResponse>

    /**
     * Next Appointments
     */
    @FormUrlEncoded
    @POST("/api/providerapp/GetNextAppointments")
    fun postNextAppointments(
        @Field("memberCode") memberCode: String,
        @Field("reservationDate") reservationDate: String,
        @Field("pageNumber") pageNumber: Int,
        @Field("pageSize") pageSize: Int,
        @Header("Authorization") authorization: String,
        @Header("LanguageCode") language: String
    ): Call<NextAppointmentsResponse>

    /**
     * All Reviews
     */
    @FormUrlEncoded
    @POST("/api/providerapp/GetAllReviews")
    fun postAllReviews(
        @Field("pageNumber") pageNumber: Int,
        @Field("pageSize") pageSize: Int,
        @Header("Authorization") authorization: String,
        @Header("LanguageCode") language: String
    ): Call<AllReviewsResponse>

    /**
     * Profile Info
     */
    @GET("/api/providerapp/GetProfile")
    fun getProfile(
        @Header("Authorization") authorization: String,
        @Header("LanguageCode") language: String
    ): Call<ProfileResponse>

    /**
     * Dashboard Counts
     */
    @GET("/api/providerapp/GetDashboardCounts")
    fun getDashboardCounts(
        @Header("Authorization") authorization: String,
        @Header("LanguageCode") language: String
    ): Call<DashboardCountsResponse>

    /**
     * Today Appointments
     */
    @GET("/api/providerapp/GetTodayAppointments")
    fun getTodayAppointments(
        @Header("Authorization") authorization: String,
        @Header("LanguageCode") language: String
    ): Call<TodayAppointmentsResponse>

    /**
     * Latest Reviews
     */
    @GET("/api/providerapp/GetLatestReviews")
    fun getLatestReviews(
        @Header("Authorization") authorization: String,
        @Header("LanguageCode") language: String
    ): Call<LatestReviewsResponse>

    /**
     * Update Description
     */
    @FormUrlEncoded
    @POST("/api/providerapp/UpdateMyDescription")
    fun postUpdateDescription(
        @Field("Information") Information: String,
        @Header("Authorization") authorization: String,
        @Header("LanguageCode") language: String
    ): Call<ErrorResponse>

    /**
     * Upload Avatar
     */
    @Multipart
    @POST("/api/providerapp/SaveProfileAvatar")
    fun postUploadAvatar(
        @Part image: MultipartBody.Part,
        @Header("Authorization") authorization: String,
        @Header("LanguageCode") language: String
    ): Call<ErrorResponse>

    /**
     * Upload Gallery
     */
    @Multipart
    @POST("/api/providerapp/AddGallery")
    fun postUploadGallery(
        @Part image: MultipartBody.Part,
        @Header("Authorization") authorization: String,
        @Header("LanguageCode") language: String
    ): Call<ErrorResponse>
    /**
     * Get Gallery
     */
    @GET("/api/providerapp/GetProfileGallery")
    fun getProfileGallery(
        @Header("Authorization") authorization: String,
        @Header("LanguageCode") language: String
    ): Call<GetProfileGalleryResponse>
    /**
     * Delete Gallery
     */
    @FormUrlEncoded
    @POST("/api/providerapp/DeleteGallery")
    fun postDeleteGallery(
        @Field("Id") Id: String,
        @Header("Authorization") authorization: String,
        @Header("LanguageCode") language: String
    ): Call<ErrorResponse>
    /**
     * Add Offer
     */
    @FormUrlEncoded
    @POST("/api/providerapp/AddOffer")
    fun postAddOffer(
        @Field("nameAr") nameAr: String,
        @Field("nameEn") nameEn: String,
        @Field("startDate") startDate: String,
        @Field("endDate") endDate: String,
        @Field("discount") discount: String,
        @Header("Authorization") authorization: String,
        @Header("LanguageCode") language: String
    ): Call<AddOfferResponse>
    /**
     * Upload Offer Image
     */
    @Multipart
    @POST("/api/providerapp/UploadOfferImage")
    fun postUploadOfferImage(
        @Part image: MultipartBody.Part,
        @Part("Id") Id: RequestBody,
        @Header("Authorization") authorization: String,
        @Header("LanguageCode") language: String
    ): Call<ErrorResponse>
    /**
     * Get Offers
     */
    @GET("/api/providerapp/GetProfileOffers")
    fun getProfileOffers(
        @Header("Authorization") authorization: String,
        @Header("LanguageCode") language: String
    ): Call<GetProfileOffersResponse>
    /**
     * Delete Offer
     */
    @FormUrlEncoded
    @POST("/api/providerapp/DeleteOffer")
    fun postDeleteOffer(
        @Field("id") id: String,
        @Header("Authorization") authorization: String,
        @Header("LanguageCode") language: String
    ): Call<ErrorResponse>

    /**
     * Update All Notification
     */
    @PUT("/api/providerapp/updateallnotificationstates")
    fun putUpdateAllNotification(
        @Header("Authorization") authorization: String,
        @Header("LanguageCode") language: String
    ): Call<ErrorResponse>

    /**
     * Update One Notification
     */
    @FormUrlEncoded
    @PUT("/api/providerapp/updatenotificationstate")
    fun putUpdateNotificationState(
        @Field("id") id: String,
        @Header("Authorization") authorization: String,
        @Header("LanguageCode") language: String
    ): Call<ErrorResponse>
    /**
     * Check-in Appointment
     */
    @FormUrlEncoded
    @POST("/api/providerapp/CheckInAppointment")
    fun postCheckInAppointment(
        @Field("id") id: String,
        @Header("Authorization") authorization: String,
        @Header("LanguageCode") language: String
    ): Call<ErrorResponse>
    /**
     * Get AppointmentInfoById
     */
    @FormUrlEncoded
    @POST("/api/providerapp/GetAppointmentInfoById")
    fun getAppointmentInfoById(
        @Field("id") id: String,
        @Header("Authorization") authorization: String,
        @Header("LanguageCode") language: String
    ): Call<GetAppointmentInfoByIDResponse>
    /**
     * Get CheckOutAppointment
     */
    @FormUrlEncoded
    @POST("/api/providerapp/CheckOutAppointment")
    fun postCheckOutAppointment(
        @Field("reservationId") reservationId: String,
        @Field("totalPrice") totalPrice: String,
        @Field("totalNetPrice") totalNetPrice: String,
        @Header("Authorization") authorization: String,
        @Header("LanguageCode") language: String
    ): Call<ErrorResponse>
    /**
     * Get ProviderServices
     */
    @GET("/api/providerapp/GetProviderServices")
    fun getProviderServices(
        @Header("Authorization") authorization: String,
        @Header("LanguageCode") language: String
    ): Call<GetProviderServicesResponse>
    /**
     * Post Add Service To Reservation
     */
    @FormUrlEncoded
    @POST("/api/providerapp/AddServiceToReservation")
    fun postAddServiceToReservation(
        @Field("id") id: String,
        @Field("price") price: String,
        @Field("discountPercentage") discountPercentage: String,
        @Field("reservationId") reservationId: String,
        @Header("Authorization") authorization: String,
        @Header("LanguageCode") language: String
    ): Call<AddServiceToReservationResponse>
    /**
     * Delete Service From Reservation
     */
    @FormUrlEncoded
    @POST("/api/providerapp/DeleteServiceFromReservation")
    fun deleteServiceFromReservation(
        @Field("reservationServiceId") reservationServiceId: String,
        @Field("reservationId") reservationId: String,
        @Header("Authorization") authorization: String,
        @Header("LanguageCode") language: String
    ): Call<DeleteServiceFromReservationResponse>
}