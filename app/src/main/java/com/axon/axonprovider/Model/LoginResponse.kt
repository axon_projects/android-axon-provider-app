package com.axon.axonprovider.Model

import com.google.gson.annotations.SerializedName

class LoginResponse {

    @field:SerializedName("result")
    val result: Result? = null

    @field:SerializedName("success")
    val success: Boolean? = null

    @field:SerializedName("__abp")
    val abp: Boolean? = null

    @field:SerializedName("error")
    val error: Any? = null

    @field:SerializedName("targetUrl")
    val targetUrl: Any? = null

    @field:SerializedName("unAuthorizedRequest")
    val unAuthorizedRequest: Boolean? = null

    inner class Result {
        @field:SerializedName("result")
        val result: String? = null
        @field:SerializedName("accountVerification")
        val accountVerification: String? = null
        @field:SerializedName("accountType")
        val accountType: String? = null
        @field:SerializedName("roleName")
        val roleName: String? = null
    }
}