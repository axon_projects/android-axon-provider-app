package com.axon.axonprovider.Model

import com.google.gson.annotations.SerializedName

class LatestReviewsResponse {

    @field:SerializedName("result")
    val result: List<ResultItem?>? = null

    @field:SerializedName("success")
    val success: Boolean? = null

    @field:SerializedName("__abp")
    val abp: Boolean? = null

    @field:SerializedName("error")
    val error: Any? = null

    @field:SerializedName("targetUrl")
    val targetUrl: Any? = null

    @field:SerializedName("unAuthorizedRequest")
    val unAuthorizedRequest: Boolean? = null

    class ResultItem {

        @field:SerializedName("memberCode")
        val memberCode: String? = null

        @field:SerializedName("memberAvatarUrl")
        val memberAvatarUrl: String? = null

        @field:SerializedName("reviewDate")
        val reviewDate: String? = null

        @field:SerializedName("rate")
        val rate: Double? = null

        @field:SerializedName("memberName")
        val memberName: String? = null

        @field:SerializedName("timeFromNow")
        val timeFromNow: String? = null

        @field:SerializedName("reviewText")
        val reviewText: String? = null
    }
}