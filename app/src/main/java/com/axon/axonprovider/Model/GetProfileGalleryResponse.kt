package com.axon.axonprovider.Model

import com.google.gson.annotations.SerializedName

data class GetProfileGalleryResponse(

	@field:SerializedName("result")
	val result: List<String?>? = null,

	@field:SerializedName("success")
	val success: Boolean? = null,

	@field:SerializedName("__abp")
	val abp: Boolean? = null,

	@field:SerializedName("error")
	val error: Any? = null,

	@field:SerializedName("targetUrl")
	val targetUrl: Any? = null,

	@field:SerializedName("unAuthorizedRequest")
	val unAuthorizedRequest: Boolean? = null
)