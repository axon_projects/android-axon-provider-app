package com.axon.axonprovider.Model

import com.google.gson.annotations.SerializedName

class ProfileResponse {
    @field:SerializedName("result")
    val result: Result? = null

    @field:SerializedName("success")
    val success: Boolean? = null

    @field:SerializedName("__abp")
    val abp: Boolean? = null

    @field:SerializedName("error")
    val error: Any? = null

    @field:SerializedName("targetUrl")
    val targetUrl: Any? = null

    @field:SerializedName("unAuthorizedRequest")
    val unAuthorizedRequest: Boolean? = null

    class Result {
        @field:SerializedName("providerServices")
        val providerServices: List<ProviderServicesItem?>? = null

        @field:SerializedName("providerWorkinDays")
        val providerWorkinDays: List<ProviderWorkinDays?>? = null

        @field:SerializedName("providerSpecializations")
        val providerSpecializations: List<ProviderSpecializationsItem?>? = null

        @field:SerializedName("mediaFiles")
        var mediaFiles: List<String?>? = null

        @field:SerializedName("providerOffers")
        var providerOffers: List<ProviderOffersItem?>? = null

        @field:SerializedName("providerProfileData")
        val providerProfileData: ProviderProfileData? = null

        class ProviderServicesItem {
            @field:SerializedName("price")
            val price: Double? = null

            @field:SerializedName("name")
            val name: String? = null

            @field:SerializedName("discount")
            val discount: Int? = null

            @field:SerializedName("id")
            val id: Int? = null

            @field:SerializedName("priceAfterDiscount")
            val priceAfterDiscount: Double? = null
        }

        class ProviderSpecializationsItem {
            @field:SerializedName("name")
            val name: String? = null

            @field:SerializedName("id")
            val id: Int? = null
        }

        class ProviderProfileData {
            @field:SerializedName("country")
            val country: String? = null

            @field:SerializedName("city")
            val city: String? = null

            @field:SerializedName("isInFavorite")
            val isInFavorite: Any? = null

            @field:SerializedName("mobileNumber")
            val mobileNumber: String? = null

            @field:SerializedName("latitude")
            val latitude: Double? = null

            @field:SerializedName("bookingWay")
            val bookingWay: String? = null

            @field:SerializedName("phone2")
            val phone2: Any? = null

            @field:SerializedName("description")
            var description: String? = null

            @field:SerializedName("waitingTime")
            val waitingTime: String? = null

            @field:SerializedName("phone1")
            val phone1: Any? = null

            @field:SerializedName("doctorName")
            val doctorName: String? = null

            @field:SerializedName("rate")
            val rate: Int? = null

            @field:SerializedName("providerId")
            val providerId: Int? = null

            @field:SerializedName("hotLine")
            val hotLine: Any? = null

            @field:SerializedName("providerName")
            val providerName: String? = null

            @field:SerializedName("longitude")
            val longitude: Double? = null

            @field:SerializedName("area")
            val area: String? = null

            @field:SerializedName("address")
            val address: String? = null

            @field:SerializedName("specializationName")
            val specializationName: String? = null

            @field:SerializedName("bookingSystem")
            val bookingSystem: String? = null

            @field:SerializedName("landLine")
            val landLine: Any? = null

            @field:SerializedName("avatar")
            val avatar: String? = null

            @field:SerializedName("doctorTitleName")
            val doctorTitleName: String? = null

            @field:SerializedName("addressOnMap")
            val addressOnMap: String? = null

            @field:SerializedName("providerTypeName")
            val providerTypeName: String? = null

            @field:SerializedName("systemProviderType")
            val systemProviderType: Int? = null
        }

        class ProviderOffersItem {
            @field:SerializedName("avatarName")
            val avatarName: Any? = null

            @field:SerializedName("description")
            val description: String? = null

            @field:SerializedName("discount")
            val discount: Int? = null

            @field:SerializedName("id")
            val id: Int? = null

            @field:SerializedName("avatar")
            val avatar: String? = null
        }

        class ProviderWorkinDays {
            @field:SerializedName("id")
            var id: Int = 0
            @field:SerializedName("dayOfWeek")
            var dayOfWeek: Int = 0
            @field:SerializedName("dateFrom")
            var dateFrom: String? = null
            @field:SerializedName("dateTo")
            var dateTo: String? = null
        }
    }
}