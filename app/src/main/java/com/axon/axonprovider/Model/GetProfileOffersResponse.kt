package com.axon.axonprovider.Model

import com.google.gson.annotations.SerializedName

class GetProfileOffersResponse {

    @field:SerializedName("result")
    val result: List<ProfileResponse.Result.ProviderOffersItem?>? = null

    @field:SerializedName("success")
    val success: Boolean? = null

    @field:SerializedName("__abp")
    val abp: Boolean? = null

    @field:SerializedName("error")
    val error: Any? = null

    @field:SerializedName("targetUrl")
    val targetUrl: Any? = null

    @field:SerializedName("unAuthorizedRequest")
    val unAuthorizedRequest: Boolean? = null
/*
    class ProviderOffersItem {

        @field:SerializedName("avatarName")
        val avatarName: Any? = null

        @field:SerializedName("description")
        val description: String? = null

        @field:SerializedName("discount")
        val discount: Int? = null

        @field:SerializedName("id")
        val id: Int? = null

        @field:SerializedName("avatar")
        val avatar: String? = null
    }*/
}