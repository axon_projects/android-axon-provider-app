package com.axon.axonprovider.Model

import com.google.gson.annotations.SerializedName

class DashboardCountsResponse {

    @field:SerializedName("result")
    val result: Result? = null

    @field:SerializedName("success")
    val success: Boolean? = null

    @field:SerializedName("__abp")
    val abp: Boolean? = null

    @field:SerializedName("error")
    val error: Any? = null

    @field:SerializedName("targetUrl")
    val targetUrl: Any? = null

    @field:SerializedName("unAuthorizedRequest")
    val unAuthorizedRequest: Boolean? = null

    class Result {
        @field:SerializedName("providerTotalVisitors")
        val providerTotalVisitors: ProviderTotalVisitors? = null

        @field:SerializedName("providerVisitsCountPerMonth")
        val providerVisitsCountPerMonth: List<ProviderVisitsCountPerMonthItem?>? = null

        class ProviderTotalVisitors {
            @field:SerializedName("total")
            val total: Int? = null
        }

        class ProviderVisitsCountPerMonthItem {
            @field:SerializedName("month")
            val month: Int? = null

            @field:SerializedName("count")
            val count: Int? = null
        }
    }
}