package com.axon.axonprovider.Model

import com.google.gson.annotations.SerializedName

 class GetAppointmentInfoByIDResponse {
	 @field:SerializedName("result")
	 val result: Result? = null

	 @field:SerializedName("success")
	 val success: Boolean? = null

	 @field:SerializedName("__abp")
	 val abp: Boolean? = null

	 @field:SerializedName("error")
	 val error: Any? = null

	 @field:SerializedName("targetUrl")
	 val targetUrl: Any? = null

	 @field:SerializedName("unAuthorizedRequest")
	 val unAuthorizedRequest: Boolean? = null

	 class ReservationServicesItem {

		 @field:SerializedName("discountPercentage")
		 val discountPercentage: Double? = null

		 @field:SerializedName("price")
		 val price: Double? = null

		 @field:SerializedName("id")
		 val id: Int? = null

		 @field:SerializedName("serviceName")
		 val serviceName: String? = null

		 @field:SerializedName("priceAfterDiscount")
		 val priceAfterDiscount: Double? = null
	 }

	 class Result{

		 @field:SerializedName("memberCode")
		 val memberCode: String? = null

		 @field:SerializedName("timeTo")
		 val timeTo: Any? = null

		 @field:SerializedName("gender")
		 val gender: Int? = null

		 @field:SerializedName("memberAge")
		 val memberAge: Int? = null

		 @field:SerializedName("memberName")
		 val memberName: String? = null

		 @field:SerializedName("membershipCode")
		 val membershipCode: String? = null

		 @field:SerializedName("memberBirthDate")
		 val memberBirthDate: String? = null

		 @field:SerializedName("reservationDate")
		 val reservationDate: String? = null

		 @field:SerializedName("genderName")
		 val genderName: String? = null

		 @field:SerializedName("serviceNames")
		 val serviceNames: String? = null

		 @field:SerializedName("reservationServices")
		 var reservationServices: List<ReservationServicesItem?>? = null

		 @field:SerializedName("timeFrom")
		 val timeFrom: String? = null

		 @field:SerializedName("memberAvatarUrl")
		 val memberAvatarUrl: String? = null

		 @field:SerializedName("price")
		 val price: Double? = null

		 @field:SerializedName("reservationStatusName")
		 val reservationStatusName: String? = null

		 @field:SerializedName("checkInTime")
		 val checkInTime: String? = null

		 @field:SerializedName("id")
		 val id: Int? = null

		 @field:SerializedName("reservationStatus")
		 val reservationStatus: Int? = null

		 @field:SerializedName("isWalkIn")
		 val isWalkIn: Boolean? = null

		 @field:SerializedName("memberId")
		 val memberId: Int? = null
	 }
 }