package com.axon.axonprovider.Model

import com.google.gson.annotations.SerializedName

class AllReviewsResponse {

    @field:SerializedName("result")
    val result: Result? = null

    @field:SerializedName("success")
    val success: Boolean? = null

    @field:SerializedName("__abp")
    val abp: Boolean? = null

    @field:SerializedName("error")
    val error: Any? = null

    @field:SerializedName("targetUrl")
    val targetUrl: Any? = null

    @field:SerializedName("unAuthorizedRequest")
    val unAuthorizedRequest: Boolean? = null

    inner class Result {

        @field:SerializedName("reviews")
        val reviews: Reviews? = null

        @field:SerializedName("rate")
        val rate: Int? = null
    }

    inner class Reviews {

        @field:SerializedName("totalPages")
        val totalPages: Int? = null

        @field:SerializedName("pageSize")
        val pageSize: Int? = null

        @field:SerializedName("currentPage")
        val currentPage: Int? = null

        @field:SerializedName("totalCount")
        val totalCount: Int? = null

        @field:SerializedName("items")
        val items: List<ItemsItem?>? = null
    }

    class ItemsItem {

        @field:SerializedName("memberCode")
        val memberCode: String? = null

        @field:SerializedName("memberAvatarUrl")
        val memberAvatarUrl: String? = null

        @field:SerializedName("reviewDate")
        val reviewDate: String? = null

        @field:SerializedName("rate")
        val rate: Double? = null

        @field:SerializedName("memberName")
        val memberName: String? = null

        @field:SerializedName("timeFromNow")
        val timeFromNow: String? = null

        @field:SerializedName("reviewText")
        val reviewText: String? = null
    }
}

