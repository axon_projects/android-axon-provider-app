package com.axon.axonprovider.Model

import com.google.gson.annotations.SerializedName

class NextAppointmentsResponse {

    @field:SerializedName("result")
    val result: Result? = null

    @field:SerializedName("success")
    val success: Boolean? = null

    @field:SerializedName("__abp")
    val abp: Boolean? = null

    @field:SerializedName("error")
    val error: Any? = null

    @field:SerializedName("targetUrl")
    val targetUrl: Any? = null

    @field:SerializedName("unAuthorizedRequest")
    val unAuthorizedRequest: Boolean? = null

    inner class Result {

        @field:SerializedName("totalPages")
        val totalPages: Int? = null

        @field:SerializedName("pageSize")
        val pageSize: Int? = null

        @field:SerializedName("currentPage")
        val currentPage: Int? = null

        @field:SerializedName("totalCount")
        val totalCount: Int? = null

        @field:SerializedName("items")
        val items: List<ItemsItem?>? = null
    }

    class ItemsItem {

        @field:SerializedName("memberCode")
        val memberCode: String? = null

        @field:SerializedName("gender")
        val gender: Int? = null

        @field:SerializedName("timeTo")
        val timeTo: String? = null

        @field:SerializedName("memberAge")
        val memberAge: Int? = null

        @field:SerializedName("memberName")
        val memberName: String? = null

        @field:SerializedName("membershipCode")
        val membershipCode: String? = null

        @field:SerializedName("memberBirthDate")
        val memberBirthDate: String? = null

        @field:SerializedName("reservationDate")
        val reservationDate: String? = null

        @field:SerializedName("genderName")
        val genderName: String? = null

        @field:SerializedName("memberAvatarUrl")
        val memberAvatarUrl: String? = null

        @field:SerializedName("timeFrom")
        val timeFrom: String? = null

        @field:SerializedName("reservationId")
        val reservationId: Int? = null

        @field:SerializedName("price")
        val price: Double? = null

        @field:SerializedName("reservationStatusName")
        val reservationStatusName: String? = null

        @field:SerializedName("reservationStatus")
        val reservationStatus: Int? = null

        @field:SerializedName("isWalkIn")
        val isWalkIn: Any? = null
    }
}