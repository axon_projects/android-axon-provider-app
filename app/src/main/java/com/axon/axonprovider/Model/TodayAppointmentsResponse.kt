package com.axon.axonprovider.Model
 
import com.google.gson.annotations.SerializedName
 
  class TodayAppointmentsResponse {
	  @field:SerializedName("result")
	  val result: List<ResultItem?>? = null

	  @field:SerializedName("success")
	  val success: Boolean? = null

	  @field:SerializedName("__abp")
	  val abp: Boolean? = null

	  @field:SerializedName("error")
	  val error: Any? = null

	  @field:SerializedName("targetUrl")
	  val targetUrl: Any? = null

	  @field:SerializedName("unAuthorizedRequest")
	  val unAuthorizedRequest: Boolean? = null

	  class ResultItem {
		  @field:SerializedName("memberCode")
		  val memberCode: String? = null

		  @field:SerializedName("timeTo")
		  val timeTo: String? = null

		  @field:SerializedName("gender")
		  val gender: Int? = null

		  @field:SerializedName("memberAge")
		  val memberAge: Int? = null

		  @field:SerializedName("memberName")
		  val memberName: String? = null

		  @field:SerializedName("membershipCode")
		  val membershipCode: String? = null

		  @field:SerializedName("memberBirthDate")
		  val memberBirthDate: String? = null

		  @field:SerializedName("reservationDate")
		  val reservationDate: String? = null

		  @field:SerializedName("genderName")
		  val genderName: String? = null

		  @field:SerializedName("serviceNames")
		  val serviceNames: String? = null

		  @field:SerializedName("timeFrom")
		  val timeFrom: String? = null

		  @field:SerializedName("memberAvatarUrl")
		  val memberAvatarUrl: String? = null

		  @field:SerializedName("price")
		  val price: Double? = null

		  @field:SerializedName("reservationStatusName")
		  val reservationStatusName: String? = null

		  @field:SerializedName("id")
		  val id: Int? = null

		  @field:SerializedName("reservationStatus")
		  val reservationStatus: Int? = null

		  @field:SerializedName("isWalkIn")
		  val isWalkIn: Boolean? = null
	  }
  }