package com.axon.axonprovider.Model

import com.google.gson.annotations.SerializedName

 class GetNotificationsResponse {

	 @field:SerializedName("result")
	 val result: List<ResultItem?>? = null

	 @field:SerializedName("success")
	 val success: Boolean? = null

	 @field:SerializedName("__abp")
	 val abp: Boolean? = null

	 @field:SerializedName("error")
	 val error: Any? = null

	 @field:SerializedName("targetUrl")
	 val targetUrl: Any? = null

	 @field:SerializedName("unAuthorizedRequest")
	 val unAuthorizedRequest: Boolean? = null
	 class ResultItem{

		 @field:SerializedName("creationTime")
		 val creationTime: String? = null

		 @field:SerializedName("id")
		 val id: String? = null

		 @field:SerializedName("text")
		 val text: String? = null

		 @field:SerializedName("state")
		 val state: Int? = null

		 @field:SerializedName("message")
		 val message: String? = null

		 @field:SerializedName("severityName")
		 val severityName: String? = null
 }
 }