package com.axon.axonprovider.Model

import com.google.gson.annotations.SerializedName

 class AreasResponse {

	 @field:SerializedName("result")
	 val result: List<ResultItem?>? = null

	 @field:SerializedName("success")
	 val success: Boolean? = null

	 @field:SerializedName("__abp")
	 val abp: Boolean? = null

	 @field:SerializedName("error")
	 val error: Any? = null

	 @field:SerializedName("targetUrl")
	 val targetUrl: Any? = null

	 @field:SerializedName("unAuthorizedRequest")
	 val unAuthorizedRequest: Boolean? = null

	 class ResultItem {

		 @field:SerializedName("name")
		 val name: String? = null

		 @field:SerializedName("id")
		 val id: Int? = null
	 }
 }