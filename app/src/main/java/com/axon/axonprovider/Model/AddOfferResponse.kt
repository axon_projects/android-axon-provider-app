package com.axon.axonprovider.Model

import com.google.gson.annotations.SerializedName

class AddOfferResponse {

    @field:SerializedName("result")
    val result: Result? = null

    @field:SerializedName("success")
    val success: Boolean? = null

    @field:SerializedName("__abp")
    val abp: Boolean? = null

    @field:SerializedName("error")
    val error: Any? = null

    @field:SerializedName("targetUrl")
    val targetUrl: Any? = null

    @field:SerializedName("unAuthorizedRequest")
    val unAuthorizedRequest: Boolean? = null

    class Result {

        @field:SerializedName("nameAr")
        val nameAr: String? = null

        @field:SerializedName("endDate")
        val endDate: String? = null

        @field:SerializedName("discount")
        val discount: Int? = null

        @field:SerializedName("id")
        val id: Int? = null

        @field:SerializedName("nameEn")
        val nameEn: String? = null

        @field:SerializedName("startDate")
        val startDate: String? = null
    }
}