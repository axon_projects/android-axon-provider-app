package com.axon.axonprovider.Model

import com.google.gson.annotations.SerializedName

class ErrorResponse {

    @field:SerializedName("result")
    val result: Any? = null

    @field:SerializedName("success")
    val success: Boolean? = null

    @field:SerializedName("__abp")
    val abp: Boolean? = null

    @field:SerializedName("error")
    val error: Error? = null

    @field:SerializedName("targetUrl")
    val targetUrl: Any? = null

    @field:SerializedName("unAuthorizedRequest")
    val unAuthorizedRequest: Boolean? = null

    class Error {

        @field:SerializedName("code")
        val code: Int? = null

        @field:SerializedName("validationErrors")
        val validationErrors: Any? = null

        @field:SerializedName("details")
        val details: String? = null

        @field:SerializedName("message")
        val message: String? = null
    }
}