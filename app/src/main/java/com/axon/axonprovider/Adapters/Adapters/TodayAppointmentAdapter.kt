package com.axon.axonprovider.Adapters.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.axon.axonprovider.Global.AppCoreCode
import com.axon.axonprovider.Presenter.TodayAppointmentAdapterPresenter
import com.axon.axonprovider.R
import com.axon.axonprovider.View.ProviderMainView.MainActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class TodayAppointmentAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>(),
    TodayAppointmentAdapterPresenter.View {
    private var todayAppointmentAdapterPresenter: TodayAppointmentAdapterPresenter? = null

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.next_appointment_custom_row, viewGroup, false)
        todayAppointmentAdapterPresenter = TodayAppointmentAdapterPresenter(this)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        position: Int,
        payloads: List<Any>
    ) {
        super.onBindViewHolder(holder, position, payloads)

        val currentAppointment =
            AppCoreCode.getInstance?.cache?.todayAppointmentsResponse?.result!![position]

        val txtAppointmentsMemberName =
            holder.itemView.findViewById<TextView>(R.id.txtAppointmentsMemberName)
        val txtAppointmentsAge = holder.itemView.findViewById<TextView>(R.id.txtAppointmentsAge)
        val imgAppointmentsGender =
            holder.itemView.findViewById<ImageView>(R.id.imgAppointmentsGender)
        val txtAppointmentsMemberCode =
            holder.itemView.findViewById<TextView>(R.id.txtAppointmentsMemberCode)
        val txtAppointmentsProviderName =
            holder.itemView.findViewById<TextView>(R.id.txtAppointmentsProviderName)
        val txtAppointmentsDocName =
            holder.itemView.findViewById<TextView>(R.id.txtAppointmentsDocName)
        val txtAppointmentsServices =
            holder.itemView.findViewById<TextView>(R.id.txtAppointmentsServices)
        val txtAppointmentsTimeFromTo =
            holder.itemView.findViewById<TextView>(R.id.txtAppointmentsTimeFromTo)
        val txtAppointmentsPrice = holder.itemView.findViewById<TextView>(R.id.txtAppointmentsPrice)
        val txtAppointmentsDateDayName =
            holder.itemView.findViewById<TextView>(R.id.txtAppointmentsDateDayName)
        val txtAppointmentsDateDayNum =
            holder.itemView.findViewById<TextView>(R.id.txtAppointmentsDateDayNum)
        val txtAppointmentsDateDayMonth =
            holder.itemView.findViewById<TextView>(R.id.txtAppointmentsDateDayMonth)
        val imgAppointmentsMemberAvatar =
            holder.itemView.findViewById<ImageView>(R.id.imgAppointmentsMemberAvatar)
        val btnAppointmentsCheckIn =
            holder.itemView.findViewById<Button>(R.id.btnAppointmentsCheckIn)
        val btnAppointmentsCheckOut =
            holder.itemView.findViewById<Button>(R.id.btnAppointmentsCheckOut)
        val btnAppointmentsCancel = holder.itemView.findViewById<Button>(R.id.btnAppointmentsCancel)
        val btnAppointmentsReport = holder.itemView.findViewById<Button>(R.id.btnAppointmentsReport)

        btnAppointmentsCheckIn.visibility = View.GONE
        btnAppointmentsCheckOut.visibility = View.GONE
        btnAppointmentsCancel.visibility = View.GONE
        btnAppointmentsReport.visibility = View.GONE

        if (currentAppointment?.reservationStatus == 0) {
            btnAppointmentsCheckIn.visibility = View.VISIBLE
        } else if (currentAppointment?.reservationStatus == 5) {
            btnAppointmentsCheckOut.visibility = View.VISIBLE
        }

        txtAppointmentsMemberName.setText(currentAppointment?.memberName)
        txtAppointmentsAge.setText(currentAppointment?.memberAge.toString())
        if (currentAppointment?.gender == 0)
            imgAppointmentsGender.setImageResource(R.drawable.axon_visitors_num_male_row)
        else
            imgAppointmentsGender.setImageResource(R.drawable.axon_visitors_num_female_row)
        txtAppointmentsProviderName.visibility = View.GONE//.setText("")
        txtAppointmentsDocName.visibility = View.GONE //.setText("")
        txtAppointmentsMemberCode.setText("#" + currentAppointment?.memberCode)
        txtAppointmentsServices.setText(currentAppointment?.serviceNames)
        var time: String? = ""
        if (currentAppointment?.timeFrom != null) {
            time = currentAppointment?.timeFrom + " - "
        }
        if (currentAppointment?.timeTo != null) {
            time += currentAppointment?.timeTo
        }
        if (currentAppointment?.isWalkIn == true) {
            time += " (Walk-in)"
        }
        txtAppointmentsTimeFromTo.setText(time)

        try {
            val dateDay = SimpleDateFormat(
                "yyyy-MM-dd",
                Locale.getDefault()
            ).parse(currentAppointment?.reservationDate)
            val outFormat = SimpleDateFormat("EEE")
            val dayName = outFormat.format(dateDay)
            txtAppointmentsDateDayName.setText(dayName)

            txtAppointmentsDateDayNum.setText(
                currentAppointment?.reservationDate?.substring(
                    currentAppointment?.reservationDate.length - 2,
                    currentAppointment?.reservationDate.length
                )
            )

            val month_date = SimpleDateFormat("MMM", Locale.getDefault())
            val sdf = SimpleDateFormat("yyyy-MM-dd")
            val dateMonth = sdf.parse(currentAppointment?.reservationDate)
            val month_name = month_date.format(dateMonth)

            txtAppointmentsDateDayMonth.setText(month_name)
        } catch (e: ParseException) {
            e.printStackTrace()
        }


        txtAppointmentsPrice.setText("EGP " + currentAppointment?.price)
        Glide.with(AppCoreCode.getInstance.cache.mainViewContext?.applicationContext!!)
            .load(AppCoreCode.getInstance.cache.mainURL + "/" + currentAppointment?.memberAvatarUrl)
            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
            .override(100, 100)
            .into(imgAppointmentsMemberAvatar)

        btnAppointmentsCheckIn.setOnClickListener(View.OnClickListener {
            AppCoreCode.getInstance?.showLoadingDialog(AppCoreCode.getInstance.cache.mainViewContext)
            todayAppointmentAdapterPresenter?.postCheckInAppointment(currentAppointment?.id.toString())
        })

        btnAppointmentsCheckOut.setOnClickListener(View.OnClickListener {
            AppCoreCode.getInstance?.showLoadingDialog(AppCoreCode.getInstance.cache.mainViewContext)
            todayAppointmentAdapterPresenter?.getAppointmentInfoById(currentAppointment?.id.toString())
        })
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, i: Int) {

    }

    override fun getItemCount(): Int {
        return AppCoreCode.getInstance?.cache?.todayAppointmentsResponse?.result?.size!!
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    /*------------------------------------Check-In Requests---------------------------------------*/
    override fun onCheckinAppointmentSuccess() {
        todayAppointmentAdapterPresenter?.getTodayAppointments()
    }

    override fun onCheckinAppointmentSuccessWithErrorMsg(msg: String) {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(AppCoreCode.getInstance?.cache?.mainViewContext, msg, Toast.LENGTH_LONG)
            .show()
    }

    override fun onCheckinAppointmentFailure() {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(
            AppCoreCode.getInstance?.cache?.mainViewContext,
            AppCoreCode.getInstance?.cache?.mainViewContext?.getString(R.string.toast_msg_something_went_wrong),
            Toast.LENGTH_LONG
        ).show()
    }

    override fun onTodayAppointmentsSuccess() {
        this.notifyDataSetChanged()
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
    }

    override fun onTodayAppointmentsSuccesWithErrorMsg(msg: String) {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(AppCoreCode.getInstance?.cache?.mainViewContext, msg, Toast.LENGTH_LONG)
            .show()
    }

    override fun onTodayAppointmentsFailure() {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(
            AppCoreCode.getInstance?.cache?.mainViewContext,
            AppCoreCode.getInstance?.cache?.mainViewContext?.getString(R.string.toast_msg_something_went_wrong),
            Toast.LENGTH_LONG
        ).show()
    }


    override fun onAppointmentInfoByIdSuccess() {
        todayAppointmentAdapterPresenter?.getProviderServices()
    }

    override fun onAppointmentInfoByIdSuccesWithErrorMsg(msg: String) {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(AppCoreCode.getInstance?.cache?.mainViewContext, msg, Toast.LENGTH_LONG)
            .show()
    }

    override fun onAppointmentInfoByIdFailure() {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(
            AppCoreCode.getInstance?.cache?.mainViewContext,
            AppCoreCode.getInstance?.cache?.mainViewContext?.getString(R.string.toast_msg_something_went_wrong),
            Toast.LENGTH_LONG
        ).show()
    }

    override fun onProviderServiceSuccess() {
        AppCoreCode.getInstance?.cache?.memberOperationType = ""
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        if (AppCoreCode.getInstance?.cache?.mainViewContext != null)
            (AppCoreCode.getInstance?.cache?.mainViewContext as MainActivity).changeFragment(16)
    }

    override fun onProviderServiceSuccesWithErrorMsg(msg: String) {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(AppCoreCode.getInstance?.cache?.mainViewContext, msg, Toast.LENGTH_LONG)
            .show()
    }

    override fun onProviderServiceFailure() {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(
            AppCoreCode.getInstance?.cache?.mainViewContext,
            AppCoreCode.getInstance?.cache?.mainViewContext?.getString(R.string.toast_msg_something_went_wrong),
            Toast.LENGTH_LONG
        ).show()
    }
}