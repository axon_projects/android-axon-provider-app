package com.axon.axonprovider.Adapters.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.axon.axonprovider.Global.AppCoreCode
import com.axon.axonprovider.Model.NextAppointmentsResponse
import com.axon.axonprovider.Model.PreviousAppointmentsResponse
import com.axon.axonprovider.R
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class AppointmentsAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.next_appointment_custom_row, viewGroup, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int, payloads: List<Any>) {
        super.onBindViewHolder(holder, position, payloads)

        if (AppCoreCode.getInstance?.cache?.isPastAppointmentTabSelected == true) {
            setPastAppointmentData(holder, position)
        } else {
            setNextAppointmentData(holder, position)
        }
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, i: Int) {

    }

    override fun getItemCount(): Int {
        if (AppCoreCode.getInstance?.cache?.isPastAppointmentTabSelected == true) {
            return AppCoreCode.getInstance?.cache?.previousAppointmentsResponse?.result?.items?.size!!
        } else {
            return return AppCoreCode.getInstance?.cache?.nextAppointmentsResponse?.result?.items?.size!!
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    private fun setNextAppointmentData(holder: RecyclerView.ViewHolder, position: Int) {
        var currentReview =
            AppCoreCode.getInstance?.cache?.nextAppointmentsResponse?.result?.items!![position]

        val txtAppointmentsMemberName = holder.itemView.findViewById<TextView>(R.id.txtAppointmentsMemberName)
        val txtAppointmentsAge = holder.itemView.findViewById<TextView>(R.id.txtAppointmentsAge)
        val imgAppointmentsGender = holder.itemView.findViewById<ImageView>(R.id.imgAppointmentsGender)
        val txtAppointmentsMemberCode = holder.itemView.findViewById<TextView>(R.id.txtAppointmentsMemberCode)
        val txtAppointmentsProviderName = holder.itemView.findViewById<TextView>(R.id.txtAppointmentsProviderName)
        val txtAppointmentsDocName = holder.itemView.findViewById<TextView>(R.id.txtAppointmentsDocName)
        val txtAppointmentsServices = holder.itemView.findViewById<TextView>(R.id.txtAppointmentsServices)
        val txtAppointmentsTimeFromTo = holder.itemView.findViewById<TextView>(R.id.txtAppointmentsTimeFromTo)
        val txtAppointmentsPrice = holder.itemView.findViewById<TextView>(R.id.txtAppointmentsPrice)
        val txtAppointmentsDateDayName = holder.itemView.findViewById<TextView>(R.id.txtAppointmentsDateDayName)
        val txtAppointmentsDateDayNum = holder.itemView.findViewById<TextView>(R.id.txtAppointmentsDateDayNum)
        val txtAppointmentsDateDayMonth = holder.itemView.findViewById<TextView>(R.id.txtAppointmentsDateDayMonth)
        val imgAppointmentsMemberAvatar = holder.itemView.findViewById<ImageView>(R.id.imgAppointmentsMemberAvatar)
        val btnAppointmentsCheckIn = holder.itemView.findViewById<Button>(R.id.btnAppointmentsCheckIn)
        val btnAppointmentsCancel = holder.itemView.findViewById<Button>(R.id.btnAppointmentsCancel)
        val btnAppointmentsReport = holder.itemView.findViewById<Button>(R.id.btnAppointmentsReport)

        btnAppointmentsCheckIn.visibility = View.VISIBLE
        btnAppointmentsCancel.visibility = View.VISIBLE
        btnAppointmentsReport.visibility = View.GONE

        txtAppointmentsMemberName.setText(currentReview?.memberName)
        txtAppointmentsAge.setText(currentReview?.memberAge.toString())
        if (currentReview?.gender == 0)
            imgAppointmentsGender.setImageResource(R.drawable.axon_visitors_num_male_row)
        else
            imgAppointmentsGender.setImageResource(R.drawable.axon_visitors_num_female_row)
        txtAppointmentsProviderName.visibility = View.GONE//.setText("")
        txtAppointmentsDocName.visibility = View.GONE //.setText("")
        txtAppointmentsMemberCode.setText("#" + currentReview?.memberCode)
        txtAppointmentsServices.setText("") // currentReview?.serviceNames
        var time: String? = ""
        if (currentReview?.timeFrom != null) {
            time = currentReview?.timeFrom + " - "
        }
        if (currentReview?.timeTo != null) {
            time += currentReview?.timeTo
        }
        if (currentReview?.isWalkIn == true) {
            time += " (Walk-in)"
        }
        txtAppointmentsTimeFromTo.setText(time)

        try {
            val dateDay = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(currentReview?.reservationDate)
            val outFormat = SimpleDateFormat("EEE")
            val dayName = outFormat.format(dateDay)
            txtAppointmentsDateDayName.setText(dayName)

            txtAppointmentsDateDayNum.setText(
                currentReview?.reservationDate?.substring(
                    currentReview?.reservationDate!!.length - 2,
                    currentReview?.reservationDate!!.length
                )
            )

            val month_date = SimpleDateFormat("MMM", Locale.getDefault())
            val sdf = SimpleDateFormat("yyyy-MM-dd")
            val dateMonth = sdf.parse(currentReview?.reservationDate)
            val month_name = month_date.format(dateMonth)

            txtAppointmentsDateDayMonth.setText(month_name)
        } catch (e: ParseException) {
            e.printStackTrace()
        }


        txtAppointmentsPrice.setText("EGP " + currentReview?.price)
        Glide.with(AppCoreCode.getInstance.cache.mainViewContext?.applicationContext!!)
            .load(AppCoreCode.getInstance.cache.mainURL + "/" + currentReview?.memberAvatarUrl)
            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
            .override(100, 100)
            .into(imgAppointmentsMemberAvatar)
    }

    private fun setPastAppointmentData(holder: RecyclerView.ViewHolder, position: Int) {
        var currentReview = AppCoreCode.getInstance?.cache?.previousAppointmentsResponse?.result?.items!![position]

        val txtAppointmentsMemberName = holder.itemView.findViewById<TextView>(R.id.txtAppointmentsMemberName)
        val txtAppointmentsAge = holder.itemView.findViewById<TextView>(R.id.txtAppointmentsAge)
        val imgAppointmentsGender = holder.itemView.findViewById<ImageView>(R.id.imgAppointmentsGender)
        val txtAppointmentsMemberCode = holder.itemView.findViewById<TextView>(R.id.txtAppointmentsMemberCode)
        val txtAppointmentsProviderName = holder.itemView.findViewById<TextView>(R.id.txtAppointmentsProviderName)
        val txtAppointmentsDocName = holder.itemView.findViewById<TextView>(R.id.txtAppointmentsDocName)
        val txtAppointmentsServices = holder.itemView.findViewById<TextView>(R.id.txtAppointmentsServices)
        val txtAppointmentsTimeFromTo = holder.itemView.findViewById<TextView>(R.id.txtAppointmentsTimeFromTo)
        val txtAppointmentsPrice = holder.itemView.findViewById<TextView>(R.id.txtAppointmentsPrice)
        val txtAppointmentsDateDayName = holder.itemView.findViewById<TextView>(R.id.txtAppointmentsDateDayName)
        val txtAppointmentsDateDayNum = holder.itemView.findViewById<TextView>(R.id.txtAppointmentsDateDayNum)
        val txtAppointmentsDateDayMonth = holder.itemView.findViewById<TextView>(R.id.txtAppointmentsDateDayMonth)
        val imgAppointmentsMemberAvatar = holder.itemView.findViewById<ImageView>(R.id.imgAppointmentsMemberAvatar)
        val btnAppointmentsCheckIn = holder.itemView.findViewById<Button>(R.id.btnAppointmentsCheckIn)
        val btnAppointmentsCancel = holder.itemView.findViewById<Button>(R.id.btnAppointmentsCancel)
        val btnAppointmentsReport = holder.itemView.findViewById<Button>(R.id.btnAppointmentsReport)

        btnAppointmentsCheckIn.visibility = View.GONE
        btnAppointmentsCancel.visibility = View.GONE
        btnAppointmentsReport.visibility = View.VISIBLE

        txtAppointmentsMemberName.setText(currentReview?.memberName)
        txtAppointmentsAge.setText(currentReview?.memberAge.toString())
        if (currentReview?.gender == 0)
            imgAppointmentsGender.setImageResource(R.drawable.axon_visitors_num_male_row)
        else
            imgAppointmentsGender.setImageResource(R.drawable.axon_visitors_num_female_row)
        txtAppointmentsProviderName.visibility = View.GONE//.setText("")
        txtAppointmentsDocName.visibility = View.GONE //.setText("")
        txtAppointmentsMemberCode.setText("#" + currentReview?.memberCode)
        txtAppointmentsServices.setText("") // currentReview?.serviceNames
        var time: String? = ""
        if (currentReview?.timeFrom != null) {
            time = currentReview?.timeFrom + " - "
        }
        if (currentReview?.timeTo != null) {
            time += currentReview?.timeTo
        }
        if (currentReview?.isWalkIn == true) {
            time += " (Walk-in)"
        }
        txtAppointmentsTimeFromTo.setText(time)

        try {
            val dateDay = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(currentReview?.reservationDate)
            val outFormat = SimpleDateFormat("EEE")
            val dayName = outFormat.format(dateDay)
            txtAppointmentsDateDayName.setText(dayName)

            txtAppointmentsDateDayNum.setText(
                currentReview?.reservationDate?.substring(
                    currentReview?.reservationDate!!.length - 2,
                    currentReview?.reservationDate!!.length
                )
            )

            val month_date = SimpleDateFormat("MMM", Locale.getDefault())
            val sdf = SimpleDateFormat("yyyy-MM-dd")
            val dateMonth = sdf.parse(currentReview?.reservationDate)
            val month_name = month_date.format(dateMonth)

            txtAppointmentsDateDayMonth.setText(month_name)
        } catch (e: ParseException) {
            e.printStackTrace()
        }


        txtAppointmentsPrice.setText("EGP " + currentReview?.price)
        Glide.with(AppCoreCode.getInstance.cache.mainViewContext?.applicationContext!!)
            .load(AppCoreCode.getInstance.cache.mainURL + "/" + currentReview?.memberAvatarUrl)
            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
            .override(100, 100)
            .into(imgAppointmentsMemberAvatar)
    }
}