package com.axon.axonprovider.Adapters.Adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.axon.axonprovider.Global.AppCoreCode
import com.axon.axonprovider.R
import com.axon.axonprovider.View.ProviderMainView.MainActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy

class ProviderOffersAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var TAG = "ProviderOffersAdapter"
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.provider_profile_info_tab_offers_custom_row, viewGroup, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int, payloads: List<Any>) {
        super.onBindViewHolder(holder, position, payloads)

        val currentItem =
            AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerOffers?.get(position)
        Log.d(TAG, "onBindViewHolder() --> providerOffers: " + currentItem?.avatar)
        val imgProviderProfileOfferImage = holder.itemView.findViewById<ImageView>(R.id.imgProviderProfileOfferImage)
        val txtProviderProfileOfferDiscount = holder.itemView.findViewById<TextView>(R.id.txtProviderProfileOfferDiscount)

        txtProviderProfileOfferDiscount.text = currentItem?.discount.toString() + " %"

        Glide.with(AppCoreCode.getInstance?.cache?.mainViewContext?.applicationContext!!)
            .load(AppCoreCode.getInstance?.cache?.mainURL + "/" + currentItem?.avatar)
            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
            .fitCenter()
            .into(imgProviderProfileOfferImage)

        imgProviderProfileOfferImage.setOnClickListener(View.OnClickListener {
            AppCoreCode.getInstance?.cache?.currentImageSlider = "Offers"
            Log.d(TAG, "onBindViewHolder() --> imgProviderProfileOfferImage --> currentImageSlider: " + AppCoreCode.getInstance?.cache?.currentImageSlider)
            AppCoreCode.getInstance?.cache?.currentImageSliderIndex = position
            if (AppCoreCode.getInstance?.cache?.mainViewContext != null)
                (AppCoreCode.getInstance?.cache?.mainViewContext as MainActivity).changeFragment(10)
        })
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, i: Int) {

    }

    override fun getItemCount(): Int {
        return AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerOffers?.size!!
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}