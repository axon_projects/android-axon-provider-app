package com.axon.axonprovider.Adapters.PagerAdapters

import android.util.Log
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.axon.axonprovider.Global.AppCoreCode
import com.axon.axonprovider.View.Login.Fragments.*

class LoginActivityPagerAdapter : FragmentStatePagerAdapter {
    // Declare Login Fragments
    private var loginJoinUsFragment: LoginJoinUsFragment? = null // 0
    private var loginFragment: LoginFragment? = null; // 1
    private var resetPasswordFragment: ResetPasswordFragment? = null // 2
    private var resetPasswordLinkFragment: ResetPasswordLinkFragment? = null // 3
    // Declare SignUp Fragments
    private var joinUsFragment: JoinUsFragment? = null; // 4

    constructor(fm: FragmentManager) : super(fm) {}

    override fun getItem(position: Int): Fragment? {
        Log.w("LoginSignUpViewPager", "getItem() --> position: $position")
        if (position == 0) {
            if (loginJoinUsFragment == null)
                loginJoinUsFragment = LoginJoinUsFragment()
            return loginJoinUsFragment
        } else if (position == 1) {
            if (loginFragment == null)
                loginFragment = LoginFragment()
            return loginFragment
        } else if (position == 2) {
            if (resetPasswordFragment == null)
                resetPasswordFragment = ResetPasswordFragment()
            return resetPasswordFragment
        } else if (position == 3) {
            if (resetPasswordLinkFragment == null)
                resetPasswordLinkFragment = ResetPasswordLinkFragment()
            return resetPasswordLinkFragment
        } else if (position == 4) {
            if (joinUsFragment == null) {
                joinUsFragment = JoinUsFragment()
            }
            return joinUsFragment
        } else {
            return null
        }

    }

    override fun getCount(): Int {
        return 5
    }
}