package com.axon.axonprovider.Adapters.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.axon.axonprovider.Global.AppCoreCode
import com.axon.axonprovider.Presenter.NotificationAdapterPresenter
import com.axon.axonprovider.R

class NotificationAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>(),
    NotificationAdapterPresenter.View {
    var notificationAdapterPresenter: NotificationAdapterPresenter? = null
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.notification_row_custom_layout, viewGroup, false)
        notificationAdapterPresenter = NotificationAdapterPresenter(this)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        position: Int,
        payloads: List<Any>
    ) {
        super.onBindViewHolder(holder, position, payloads)

        val currentNotification =
            AppCoreCode.getInstance?.cache?.notificationsResponse?.result!![position]

        val imgNotificationImage: ImageView =
            holder.itemView.findViewById(R.id.imgNotificationImage)
        val txtNotificationText: TextView = holder.itemView.findViewById(R.id.txtNotificationText)
        val txtNotificationSeverityName: TextView =
            holder.itemView.findViewById(R.id.txtNotificationSeverityName)
        val txtNotificationMessage: TextView =
            holder.itemView.findViewById(R.id.txtNotificationMessage)
        val imgNotificationState: ImageView =
            holder.itemView.findViewById(R.id.imgNotificationState)
        val txtNotificationCreationTime: TextView =
            holder.itemView.findViewById(R.id.txtNotificationCreationTime)

        txtNotificationText.setText(currentNotification?.text)
        txtNotificationSeverityName.setText(currentNotification?.severityName)
        txtNotificationMessage.setText(currentNotification?.message)
        txtNotificationCreationTime.setText(currentNotification?.creationTime)

        if (currentNotification?.state === 0) {
            imgNotificationState.setVisibility(View.VISIBLE)
        } else {
            imgNotificationState.setVisibility(View.INVISIBLE)
        }

        holder.itemView.setOnClickListener {

            if (currentNotification?.state == 0) {
                AppCoreCode.getInstance?.showLoadingDialog(AppCoreCode.getInstance.cache.mainViewContext)
                notificationAdapterPresenter?.putUpdateNotification(currentNotification.id.toString())
            }
        }
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, i: Int) {

    }

    override fun getItemCount(): Int {
        return AppCoreCode.getInstance?.cache?.notificationsResponse?.result?.size!!
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onUpdateNotificationSuccess() {
        notificationAdapterPresenter?.getNotifications()
    }

    override fun onUpdateNotificationSuccessWithErrorMsg(msg: String) {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(AppCoreCode.getInstance?.cache?.mainViewContext, msg, Toast.LENGTH_LONG).show()
    }

    override fun onUpdateNotificationFailure() {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(
            AppCoreCode.getInstance?.cache?.mainViewContext,
            AppCoreCode.getInstance?.cache?.mainViewContext?.getString(R.string.toast_msg_something_went_wrong),
            Toast.LENGTH_LONG
        ).show()
    }

    override fun onNotificationSuccess() {
        this.notifyDataSetChanged()
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
    }

    override fun onNotificationSuccessWithErrorMsg(msg: String) {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(AppCoreCode.getInstance?.cache?.mainViewContext, msg, Toast.LENGTH_LONG).show()
    }

    override fun onNotificationFailure() {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(
            AppCoreCode.getInstance?.cache?.mainViewContext,
            AppCoreCode.getInstance?.cache?.mainViewContext?.getString(R.string.toast_msg_something_went_wrong),
            Toast.LENGTH_LONG
        ).show()
    }
}