package com.axon.axonprovider.Adapters.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.axon.axonprovider.Global.AppCoreCode
import com.axon.axonprovider.R
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy

class ProviderServicesAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.provider_profile_custom_service, viewGroup, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int, payloads: List<Any>) {
        super.onBindViewHolder(holder, position, payloads)

        val currentService = AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerServices?.get(position)!!

        val txtProviderProfileServiceTitle = holder.itemView.findViewById<TextView>(R.id.txtProviderProfileServiceTitle)
        val txtProviderProfileServiceDiscount = holder.itemView.findViewById<TextView>(R.id.txtProviderProfileServiceDiscount)
        val txtProviderProfileServiceDiscountBefore = holder.itemView.findViewById<TextView>(R.id.txtProviderProfileServiceDiscountBefore)
        val txtProviderProfileServiceDiscountAfter = holder.itemView.findViewById<TextView>(R.id.txtProviderProfileServiceDiscountAfter)

        txtProviderProfileServiceTitle.setText(currentService.name)
        txtProviderProfileServiceDiscount.text = "-" + currentService.discount + " %"
        txtProviderProfileServiceDiscountBefore.setText(AppCoreCode.getInstance.cache.mainViewContext!!.getResources().getString(R.string.main_view_egp) + currentService.price)
        txtProviderProfileServiceDiscountAfter.setText(AppCoreCode.getInstance.cache.mainViewContext!!.getResources().getString(R.string.main_view_egp) + currentService.priceAfterDiscount as Int)
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, i: Int) {

    }

    override fun getItemCount(): Int {
        return AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerServices?.size!!
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}