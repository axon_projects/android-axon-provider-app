package com.axon.axonprovider.Adapters.Adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.axon.axonprovider.Global.AppCoreCode
import com.axon.axonprovider.R
import com.axon.axonprovider.View.ProviderMainView.MainActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy

class ProviderGallaryAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var TAG = "ProviderGallaryAdapter"
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.provider_profile_info_tab_gallary_custom_row, viewGroup, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int, payloads: List<Any>) {
        super.onBindViewHolder(holder, position, payloads)

        val currentItem = AppCoreCode.getInstance?.cache?.profileResponse?.result?.mediaFiles?.get(position)
        Log.d(TAG, "onBindViewHolder() --> mediaFile: " + currentItem)
        val imgProviderProfileImage = holder.itemView.findViewById<ImageView>(R.id.imgProviderProfileImage)

        Glide.with(AppCoreCode.getInstance?.cache?.mainViewContext?.applicationContext!!)
            .load(AppCoreCode.getInstance?.cache?.mainURL + "/" + currentItem)
            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
            .fitCenter()
            .into(imgProviderProfileImage)

        imgProviderProfileImage.setOnClickListener(View.OnClickListener {
            AppCoreCode.getInstance?.cache?.currentImageSlider = "Gallery"
            Log.d(TAG, "onBindViewHolder() --> imgProviderProfileImage --> currentImageSlider: " + AppCoreCode.getInstance?.cache?.currentImageSlider)
            AppCoreCode.getInstance?.cache?.currentImageSliderIndex = position
            if (AppCoreCode.getInstance?.cache?.mainViewContext != null)
                (AppCoreCode.getInstance?.cache?.mainViewContext as MainActivity).changeFragment(10)
        })
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, i: Int) {

    }

    override fun getItemCount(): Int {
        return AppCoreCode.getInstance?.cache?.profileResponse?.result?.mediaFiles?.size!!
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}