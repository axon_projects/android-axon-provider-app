package com.axon.axonprovider.Adapters.PagerAdapters

import android.content.Context
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.viewpager.widget.PagerAdapter
import com.axon.axonprovider.Global.AppCoreCode
import com.axon.axonprovider.Model.ProfileResponse
import com.axon.axonprovider.UI.TouchImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy

class ViewPagerAdapter: PagerAdapter {
    private var context: Context? = null
    private var imageUrls: List<*>? = null
    private var singleImageURL: String? = null
    private var textView: TextView? = null
    private val TAG = "ViewPagerAdapter"

    constructor(context: Context, imageUrls: List<*>, textView: TextView?) {
        Log.d(TAG, "constructor() --> Called")
        this.context = context
        this.imageUrls = imageUrls
        this.textView = textView
    }

    override fun getCount(): Int {
        return imageUrls!!.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        Log.d(TAG, "instantiateItem() --> Called")
        val imageView = TouchImageView(this!!.context!!)
        if (AppCoreCode.getInstance?.cache?.currentImageSlider.equals("Offers")) {
            singleImageURL = (imageUrls?.get(position) as ProfileResponse.Result.ProviderOffersItem).avatar
        } else {
            singleImageURL = imageUrls?.get(position).toString()
        }
        Log.d(TAG, "instantiateItem() --> singleImageURL: " + singleImageURL)
        Glide.with(AppCoreCode.getInstance?.cache?.mainViewContext?.applicationContext!!)
            .load(AppCoreCode.getInstance?.cache?.mainURL + "/" + singleImageURL)
            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
            .fitCenter()
            .into(imageView)
        container.addView(imageView)

        if (textView != null)
            imageView.setOnClickListener(View.OnClickListener {
                Log.d(TAG, "instantiateItem() --> txtDescription getVisibility: " + textView!!.visibility)
                if (textView!!.visibility == View.GONE) {
                    textView!!.visibility = View.VISIBLE
                } else if (textView!!.visibility == View.VISIBLE) {
                    textView!!.visibility = View.GONE
                }
            })

        return imageView
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }
}