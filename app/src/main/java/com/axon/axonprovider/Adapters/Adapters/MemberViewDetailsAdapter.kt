package com.axon.axonprovider.Adapters.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.axon.axonprovider.Global.AppCoreCode
import com.axon.axonprovider.Presenter.MemberViewDetailsAdapterPresenter
import com.axon.axonprovider.Presenter.MemberViewDetailsFragmentPresenter
import com.axon.axonprovider.R

class MemberViewDetailsAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>(),
    MemberViewDetailsAdapterPresenter.View {
    private var memberViewDetailsAdapterPresenter: MemberViewDetailsAdapterPresenter? = null
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.member_services_custom_row, viewGroup, false)
        memberViewDetailsAdapterPresenter = MemberViewDetailsAdapterPresenter(this)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        position: Int,
        payloads: List<Any>
    ) {
        super.onBindViewHolder(holder, position, payloads)

        var txtMemberServiceName = holder.itemView.findViewById<TextView>(R.id.txtMemberServiceName)
        var txtMemberServicePrice =
            holder.itemView.findViewById<TextView>(R.id.txtMemberServicePrice)
        var txtMemberServicePriceAfterDiscount =
            holder.itemView.findViewById<TextView>(R.id.txtMemberServicePriceAfterDiscount)
        var btnMemberServiceDelete =
            holder.itemView.findViewById<Button>(R.id.btnMemberServiceDelete)

        if (position == 0) {
            btnMemberServiceDelete.visibility = View.GONE
        } else {
            btnMemberServiceDelete.visibility = View.VISIBLE
        }

        if (AppCoreCode.getInstance?.cache?.memberOperationType?.equals("")!!) {
            var currentService =
                AppCoreCode.getInstance?.cache?.getAppointmentInfoByIDResponse?.result?.reservationServices!![position]

            txtMemberServiceName.setText(currentService?.serviceName)
            txtMemberServicePrice.setText(currentService?.price.toString())
            txtMemberServicePriceAfterDiscount.setText(currentService?.priceAfterDiscount.toString())
            btnMemberServiceDelete.setOnClickListener(View.OnClickListener {
                AppCoreCode.getInstance?.showLoadingDialog(AppCoreCode.getInstance.cache.mainViewContext)
                memberViewDetailsAdapterPresenter?.deleteServiceFromReservation(
                    currentService?.id.toString(),
                    AppCoreCode.getInstance.cache.getAppointmentInfoByIDResponse?.result?.id.toString()
                )
            })
        } else if(AppCoreCode.getInstance?.cache?.memberOperationType?.equals("AddService")!!){
            var currentService =
                AppCoreCode.getInstance?.cache?.addServiceToReservationResponse?.result!![position]

            txtMemberServiceName.setText(currentService?.serviceName)
            txtMemberServicePrice.setText(currentService?.price.toString())
            txtMemberServicePriceAfterDiscount.setText(currentService?.priceAfterDiscount.toString())
            btnMemberServiceDelete.setOnClickListener(View.OnClickListener {
                AppCoreCode.getInstance?.showLoadingDialog(AppCoreCode.getInstance.cache.mainViewContext)
                memberViewDetailsAdapterPresenter?.deleteServiceFromReservation(
                    currentService?.id.toString(),
                    AppCoreCode.getInstance.cache.getAppointmentInfoByIDResponse?.result?.id.toString()
                )
            })
        } else if(AppCoreCode.getInstance?.cache?.memberOperationType?.equals("DeleteService")!!){
            var currentService =
                AppCoreCode.getInstance?.cache?.deleteServiceFromReservationResponse?.result!![position]

            txtMemberServiceName.setText(currentService?.serviceName)
            txtMemberServicePrice.setText(currentService?.price.toString())
            txtMemberServicePriceAfterDiscount.setText(currentService?.priceAfterDiscount.toString())
            btnMemberServiceDelete.setOnClickListener(View.OnClickListener {
                AppCoreCode.getInstance?.showLoadingDialog(AppCoreCode.getInstance.cache.mainViewContext)
                memberViewDetailsAdapterPresenter?.deleteServiceFromReservation(
                    currentService?.id.toString(),
                    AppCoreCode.getInstance.cache.getAppointmentInfoByIDResponse?.result?.id.toString()
                )
            })
        }
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, i: Int) {

    }

    override fun getItemCount(): Int {
        if (AppCoreCode.getInstance?.cache?.memberOperationType?.equals("AddService")!!) {
            return AppCoreCode.getInstance?.cache?.addServiceToReservationResponse?.result?.size!!
        } else if (AppCoreCode.getInstance?.cache?.memberOperationType?.equals("DeleteService")!!) {
            return AppCoreCode.getInstance?.cache?.deleteServiceFromReservationResponse?.result?.size!!
        } else {
            return AppCoreCode.getInstance?.cache?.getAppointmentInfoByIDResponse?.result?.reservationServices?.size!!
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onDeleteServiceFromReservationSuccess() {
        AppCoreCode.getInstance?.cache?.memberOperationType = "DeleteService"
        this.notifyDataSetChanged()
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
    }

    override fun onDeleteServiceFromReservationSuccesWithErrorMsg(msg: String) {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(AppCoreCode.getInstance?.cache?.mainViewContext, msg, Toast.LENGTH_LONG)
            .show()
    }

    override fun onDeleteServiceFromReservationFailure() {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(
            AppCoreCode.getInstance?.cache?.mainViewContext,
            AppCoreCode.getInstance?.cache?.mainViewContext?.getString(R.string.toast_msg_something_went_wrong),
            Toast.LENGTH_LONG
        ).show()
    }
}