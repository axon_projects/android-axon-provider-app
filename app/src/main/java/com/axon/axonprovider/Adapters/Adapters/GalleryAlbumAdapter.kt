package com.axon.axonprovider.Adapters.Adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.axon.axonprovider.Global.AppCoreCode
import com.axon.axonprovider.Presenter.GalleryAlbumAdapterPresenter
import com.axon.axonprovider.Presenter.GalleryAlbumFragmentPresenter
import com.axon.axonprovider.R
import com.axon.axonprovider.View.ProviderMainView.MainActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy

class GalleryAlbumAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>(),
    GalleryAlbumAdapterPresenter.View {
    var TAG = "ProviderGallaryAdapter"
    var galleryAlbumAdapterPresenter: GalleryAlbumAdapterPresenter? = null

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.provider_profile_gallary_album_custom_row, viewGroup, false)
        galleryAlbumAdapterPresenter = GalleryAlbumAdapterPresenter(this)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        position: Int,
        payloads: List<Any>
    ) {
        super.onBindViewHolder(holder, position, payloads)

        val currentItem =
            AppCoreCode.getInstance?.cache?.profileResponse?.result?.mediaFiles?.get(position)
        Log.d(TAG, "onBindViewHolder() --> mediaFile: " + currentItem)
        val imgProviderProfileImage =
            holder.itemView.findViewById<ImageView>(R.id.imgProviderProfileImage)
        val ibtnDeleteGallery: ImageButton =
            holder.itemView.findViewById(R.id.ibtnDeleteGallery)

        Glide.with(AppCoreCode.getInstance?.cache?.mainViewContext?.applicationContext!!)
            .load(AppCoreCode.getInstance?.cache?.mainURL + "/" + currentItem)
            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
            .fitCenter()
            .into(imgProviderProfileImage)

        imgProviderProfileImage.setOnClickListener(View.OnClickListener {
            AppCoreCode.getInstance?.cache?.currentImageSlider = "Gallery"
            Log.d(
                TAG,
                "onBindViewHolder() --> imgProviderProfileImage --> currentImageSlider: " + AppCoreCode.getInstance?.cache?.currentImageSlider
            )
            AppCoreCode.getInstance?.cache?.currentImageSliderIndex = position
            if (AppCoreCode.getInstance?.cache?.mainViewContext != null)
                (AppCoreCode.getInstance?.cache?.mainViewContext as MainActivity).changeFragment(
                    10
                )
        })

        ibtnDeleteGallery.setOnClickListener(View.OnClickListener {
            AppCoreCode.getInstance?.showLoadingDialog(AppCoreCode.getInstance.cache.mainViewContext)
            /*galleryAlbumAdapterPresenter?.postDeleteGallery(currentItem?.substring(0,currentItem?.indexOf("/"))?.substring(0,currentItem?.indexOf("/"))!!)*/
            galleryAlbumAdapterPresenter?.postDeleteGallery(
                currentItem?.replace(
                    "mediaFiles/axProviderMediaFiles/",
                    ""
                )!!
            )
        })
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, i: Int) {

    }

    override fun getItemCount(): Int {
        return AppCoreCode.getInstance?.cache?.profileResponse?.result?.mediaFiles?.size!!
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onDeleteGallerySuccess() {
        galleryAlbumAdapterPresenter?.getProfileGallery()
    }

    override fun onDeleteGallerySuccessWithErrorMsg(msg: String) {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(AppCoreCode.getInstance?.cache?.mainViewContext, msg, Toast.LENGTH_LONG)
            .show()
    }

    override fun onDeleteGalleryFailure() {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(
            AppCoreCode.getInstance?.cache?.mainViewContext,
            AppCoreCode.getInstance?.cache?.mainViewContext?.getString(R.string.toast_msg_something_went_wrong),
            Toast.LENGTH_LONG
        ).show()
    }

    override fun onGetGallerySuccess() {
        this.notifyDataSetChanged()
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(
            AppCoreCode.getInstance?.cache?.mainViewContext,
            "Gallery has been deleted successfully!",
            Toast.LENGTH_LONG
        )
            .show()
    }

    override fun onGetGallerySuccessWithErrorMsg(msg: String) {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(AppCoreCode.getInstance?.cache?.mainViewContext, msg, Toast.LENGTH_LONG)
            .show()
    }

    override fun onGetGalleryFailure() {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(
            AppCoreCode.getInstance?.cache?.mainViewContext,
            AppCoreCode.getInstance?.cache?.mainViewContext?.getString(R.string.toast_msg_something_went_wrong),
            Toast.LENGTH_LONG
        ).show()
    }
}