package com.axon.axonprovider.Adapters.Adapters

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.axon.axonprovider.Global.AppCoreCode
import com.axon.axonprovider.R
import com.axon.axonprovider.View.ProviderMainView.Fragments.MainVewFragment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy

class LatestReviewsAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.lastest_reviews_custom_row, viewGroup, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int, payloads: List<Any>) {
        super.onBindViewHolder(holder, position, payloads)
        val currentReview = AppCoreCode.getInstance?.cache?.latestReviewsResponse?.result!![position]

        val txtReviewsMemberName = holder.itemView.findViewById<TextView>(R.id.txtReviewsMemberName)
        val txtReviewsDate = holder.itemView.findViewById<TextView>(R.id.txtReviewsDate)
        val txtReviewsText = holder.itemView.findViewById<TextView>(R.id.txtReviewsText)
        val rateBarReviews = holder.itemView.findViewById<RatingBar>(R.id.rateBarReviews)
        val imgReviewsAvatar = holder.itemView.findViewById<ImageView>(R.id.imgReviewsAvatar)

        txtReviewsMemberName.setText(currentReview?.memberName)
        txtReviewsDate.setText(currentReview?.reviewDate)
        txtReviewsText.setText(currentReview?.reviewText)
        rateBarReviews.rating = currentReview?.rate?.toFloat()!!

        Glide.with(AppCoreCode.getInstance.cache.mainViewContext?.applicationContext!!)
            .load(AppCoreCode.getInstance.cache.mainURL + "/" + currentReview.memberAvatarUrl)
            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
            .override(100, 100)
            .into(imgReviewsAvatar)
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, i: Int) {

    }

    override fun getItemCount(): Int {
        return AppCoreCode.getInstance?.cache?.latestReviewsResponse?.result?.size!!
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}