package com.axon.axonprovider.Adapters.Adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.axon.axonprovider.Global.AppCoreCode
import com.axon.axonprovider.Presenter.OfferAlbumFragmentPresenter
import com.axon.axonprovider.Presenter.OffersAlbumAdapterPresenter
import com.axon.axonprovider.R
import com.axon.axonprovider.View.ProviderMainView.MainActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy

class OffersAlbumAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>(), OffersAlbumAdapterPresenter.View {
    var TAG = "OffersAlbumAdapter"
    var offersAlbumAdapterPresenter:OffersAlbumAdapterPresenter? = null

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.provider_profile_offer_album_custom_row, viewGroup, false)
        offersAlbumAdapterPresenter = OffersAlbumAdapterPresenter(this)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int, payloads: List<Any>) {
        super.onBindViewHolder(holder, position, payloads)

        val currentItem =
            AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerOffers?.get(position)
        Log.d(TAG, "onBindViewHolder() --> providerOffers: " + currentItem?.avatar)
        val ibtnDeleteOffer = holder.itemView.findViewById<ImageButton>(R.id.ibtnDeleteOffer)
        val imgProviderProfileOfferImage = holder.itemView.findViewById<ImageView>(R.id.imgProviderProfileOfferImage)
        val txtProviderProfileOfferDiscount = holder.itemView.findViewById<TextView>(R.id.txtProviderProfileOfferDiscount)

        txtProviderProfileOfferDiscount.text = currentItem?.discount.toString() + " %"

        Glide.with(AppCoreCode.getInstance?.cache?.mainViewContext?.applicationContext!!)
            .load(AppCoreCode.getInstance?.cache?.mainURL + "/" + currentItem?.avatar)
            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
            .fitCenter()
            .into(imgProviderProfileOfferImage)

        imgProviderProfileOfferImage.setOnClickListener(View.OnClickListener {
            AppCoreCode.getInstance?.cache?.currentImageSlider = "Offers"
            Log.d(TAG, "onBindViewHolder() --> imgProviderProfileOfferImage --> currentImageSlider: " + AppCoreCode.getInstance?.cache?.currentImageSlider)
            AppCoreCode.getInstance?.cache?.currentImageSliderIndex = position
            if (AppCoreCode.getInstance?.cache?.mainViewContext != null)
                (AppCoreCode.getInstance?.cache?.mainViewContext as MainActivity).changeFragment(10)
        })

        ibtnDeleteOffer.setOnClickListener(View.OnClickListener {
            AppCoreCode.getInstance?.showLoadingDialog(AppCoreCode.getInstance.cache.mainViewContext)
            offersAlbumAdapterPresenter?.postDeleteOffer(currentItem?.id.toString())
        })
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, i: Int) {

    }

    override fun getItemCount(): Int {
        return AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerOffers?.size!!
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onDeleteOfferSuccess() {
        offersAlbumAdapterPresenter?.getProfileOffers()
    }

    override fun onDeleteOfferSuccessWithErrorMsg(msg: String) {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(AppCoreCode.getInstance?.cache?.mainViewContext, msg, Toast.LENGTH_LONG)
            .show()
    }

    override fun onDeleteOfferFailure() {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(
            AppCoreCode.getInstance?.cache?.mainViewContext,
            AppCoreCode.getInstance?.cache?.mainViewContext?.getString(R.string.toast_msg_something_went_wrong),
            Toast.LENGTH_LONG
        ).show()
    }

    override fun onGetOffersSuccess() {
        this.notifyDataSetChanged()
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(
            AppCoreCode.getInstance?.cache?.mainViewContext,
            "Offer has been deleted successfully!",
            Toast.LENGTH_LONG
        )
            .show()
    }

    override fun onGetOffersSuccessWithErrorMsg(msg: String) {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(AppCoreCode.getInstance?.cache?.mainViewContext, msg, Toast.LENGTH_LONG)
            .show()
    }

    override fun onGetOffersFailure() {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(
            AppCoreCode.getInstance?.cache?.mainViewContext,
            AppCoreCode.getInstance?.cache?.mainViewContext?.getString(R.string.toast_msg_something_went_wrong),
            Toast.LENGTH_LONG
        ).show()
    }
}