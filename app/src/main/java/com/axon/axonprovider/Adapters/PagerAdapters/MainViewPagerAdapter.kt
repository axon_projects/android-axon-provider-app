package com.axon.axonprovider.Adapters.PagerAdapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.axon.axonprovider.View.ProviderMainView.Fragments.*

class MainViewPagerAdapter : FragmentStatePagerAdapter {
    private var mainVewFragment: MainVewFragment? = null // 0
    private var scanQRCodeFragment: ScanQRCodeFragment? = null // 1
    private var checkOutFragment: CheckOutFragment? = null // 2
    private var cancelAppointmentFragment: CancelAppointmentFragment? = null // 3
    private var bookingConfigFragment: BookingConfigFragment? = null // 4
    private var reviewsFragment: ReviewsFragment? = null // 5
    private var scheduleSettingFragment: ScheduleSettingFragment? = null // 6
    private var workingHoursSettingFragment: WorkingHoursSettingFragment? = null // 7
    private var appointmentFragment: AppointmentsFragment? = null // 8
    private var providerProfileFragment: ProviderProfileFragment? = null // 9
    private var imageViewerFragment: ImageViewerFragment? = null // 10
    private var aboutUsFragment: AboutUsFragment? = null // 11
    private var contactUsFragment: ContactUsFragment? = null // 12
    private var galleryAlbumFragment: GalleryAlbumFragment? = null // 13
    private var offerAlbumFragment: OfferAlbumFragment? = null // 14
    private var notificationFragment: NotificationFragment? = null // 15
    private var memberViewDetailsFragment: MemberViewDetailsFragment? = null // 16

    public constructor(fm: FragmentManager) : super(fm) {

    }

    /**
     * Return the Fragment associated with a specified position.
     */
    override fun getItem(position: Int): Fragment? {
        if (position == 0) run {
            if (mainVewFragment == null)
                mainVewFragment = MainVewFragment()
            return mainVewFragment
        } else if (position == 1) run {
            if (scanQRCodeFragment == null)
                scanQRCodeFragment = ScanQRCodeFragment()
            return scanQRCodeFragment
        } else if (position == 2) run {
            if (checkOutFragment == null)
                checkOutFragment = CheckOutFragment()
            return checkOutFragment
        } else if (position == 3) run {
            if (cancelAppointmentFragment == null)
                cancelAppointmentFragment = CancelAppointmentFragment()
            return cancelAppointmentFragment
        } else if (position == 4) run {
            if (bookingConfigFragment == null)
                bookingConfigFragment = BookingConfigFragment()
            return bookingConfigFragment
        } else if (position == 5) run {
            if (reviewsFragment == null)
                reviewsFragment = ReviewsFragment()
            return reviewsFragment
        } else if (position == 6) run {
            if (scheduleSettingFragment == null)
                scheduleSettingFragment = ScheduleSettingFragment()
            return scheduleSettingFragment
        } else if (position == 7) run {
            if (workingHoursSettingFragment == null)
                workingHoursSettingFragment = WorkingHoursSettingFragment()
            return workingHoursSettingFragment
        } else if (position == 8) run {
            if (appointmentFragment == null)
                appointmentFragment = AppointmentsFragment()
            return appointmentFragment
        } else if (position == 9) run {
            if (providerProfileFragment == null)
                providerProfileFragment = ProviderProfileFragment()
            return providerProfileFragment
        } else if (position == 10) run {
            /*if (imageViewerFragment == null)*/
                imageViewerFragment = ImageViewerFragment()
            return imageViewerFragment
        } else if (position == 11) run {
            if (aboutUsFragment == null)
                aboutUsFragment = AboutUsFragment()
            return aboutUsFragment
        } else if (position == 12) run {
            if (contactUsFragment == null)
                contactUsFragment = ContactUsFragment()
            return contactUsFragment
        } else if (position == 13) run {
            if (galleryAlbumFragment == null)
                galleryAlbumFragment = GalleryAlbumFragment()
            return galleryAlbumFragment
        }else if (position == 14) run {
            if (offerAlbumFragment == null)
                offerAlbumFragment = OfferAlbumFragment()
            return offerAlbumFragment
        }else if (position == 15) run {
            if (notificationFragment == null)
                notificationFragment = NotificationFragment()
            return notificationFragment
        }else if (position == 16) run {
            if (memberViewDetailsFragment == null)
                memberViewDetailsFragment = MemberViewDetailsFragment()
            return memberViewDetailsFragment
        } else {
            return null
        }
    }

    /**
     * Return the number of views available.
     */
    override fun getCount(): Int {
        return 17
    }


}