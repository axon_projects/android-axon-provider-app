package com.axon.axonprovider.Presenter

import android.util.Log
import com.axon.axonprovider.Global.AppCoreCode
import com.axon.axonprovider.Model.ErrorResponse
import com.axon.axonprovider.Model.GetNotificationsResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

class MainViewFragmentPresenter {
    private val TAG = "MainViewPresenter"

    private var view: View? = null

    public constructor(view: View) {
        this.view = view
    }

    public fun getNotifications() {
        var call = AppCoreCode.getInstance?.cache?.api?.getNotifications(
            "Bearer " + AppCoreCode.getInstance.cache.loginResponse?.result?.result.toString(),
            "en"
        )

        Log.d(TAG, "postPreviousAppointments() -> call enqueue")
        call?.enqueue(object : Callback<GetNotificationsResponse> {
            override fun onResponse(
                call: Call<GetNotificationsResponse>,
                response: Response<GetNotificationsResponse>
            ) {
                Log.d(TAG, "postPreviousAppointments() --> onResponse() --> " + response?.code())
                if (response.body() != null && response.code() == 200) {
                    val responseBody = response.body()
                    if (responseBody?.success == true) {
                        AppCoreCode.getInstance?.cache?.notificationsResponse = responseBody
                        view?.onNotificationSuccess()

                    }
                } else {
                    try {
                        // https://stackoverflow.com/questions/38304316/how-to-handle-error-in-retrofit-2-0
                        var pojo = AppCoreCode.getInstance?.cache?.gson?.fromJson(
                            response.errorBody()?.string(),
                            ErrorResponse::class.java
                        )
                        view?.onNotificationSuccessWithErrorMsg(pojo?.error?.details.toString())
                    } catch (e: IOException) {
                    }

                }
            }

            override fun onFailure(call: Call<GetNotificationsResponse>, t: Throwable) {
                Log.d(TAG, "postPreviousAppointments() --> onFailure() --> " + t.message)
                view?.onNotificationFailure()
            }
        })
    }

    interface View {
        fun onNotificationSuccess()
        fun onNotificationSuccessWithErrorMsg(msg: String)
        fun onNotificationFailure()
    }
}