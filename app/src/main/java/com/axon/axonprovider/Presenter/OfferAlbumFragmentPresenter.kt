package com.axon.axonprovider.Presenter

import android.util.Log
import com.axon.axonprovider.Global.AppCoreCode
import com.axon.axonprovider.Model.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.IOException

class OfferAlbumFragmentPresenter {
    private val TAG = "GalleryAlbumPresenter"

    private var view: View? = null

    public constructor(view: View) {
        this.view = view
    }

    public fun postAddOffer(nameAr: String, nameEn: String, startDate: String, endDate: String, discount: String) {
        var call = AppCoreCode.getInstance?.cache?.api?.postAddOffer(
            nameAr, nameEn, startDate, endDate, discount,
            "Bearer " + AppCoreCode.getInstance.cache.loginResponse?.result?.result.toString(),
            "en"
        )

        Log.d(TAG, "getAddOffer() -> call enqueue")
        call?.enqueue(object : Callback<AddOfferResponse> {
            override fun onResponse(
                call: Call<AddOfferResponse>,
                response: Response<AddOfferResponse>
            ) {
                Log.d(TAG, "getAddOffer() --> onResponse() --> " + response?.code())
                if (response.body() != null && response.code() == 200) {
                    val responseBody = response.body()
                    AppCoreCode.getInstance?.cache?.addOfferResponse = responseBody
                    view?.onAddOfferSuccess()
                } else {
                    try {
                        // https://stackoverflow.com/questions/38304316/how-to-handle-error-in-retrofit-2-0
                        var pojo = AppCoreCode.getInstance?.cache?.gson?.fromJson(
                            response.errorBody()?.string(),
                            ErrorResponse::class.java
                        )
                        view?.onAddOfferSuccessWithErrorMsg(pojo?.error?.details.toString())
                    } catch (e: IOException) {
                    }

                }
            }

            override fun onFailure(call: Call<AddOfferResponse>, t: Throwable) {
                Log.d(TAG, "getAddOffer() --> onFailure() --> " + t.message)
                view?.onAddOfferFailure()
            }
        })
    }

    public fun postUploadOfferImage(selectedNewAvatar: String, offerID: String) {
        var file = File(selectedNewAvatar)
        var requestedFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
        var profilePic =
            MultipartBody.Part.createFormData("OfferImage", file.name, requestedFile)

        var id:RequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), offerID)

        var call = AppCoreCode.getInstance?.cache?.api?.postUploadOfferImage(
            profilePic,
            id,
            "Bearer " + AppCoreCode.getInstance.cache.loginResponse?.result?.result.toString(),
            "en"
        )

        Log.d(TAG, "postUploadGallery() -> call enqueue")
        call?.enqueue(object : Callback<ErrorResponse> {
            override fun onResponse(
                call: Call<ErrorResponse>,
                response: Response<ErrorResponse>
            ) {
                Log.d(TAG, "postUploadGallery() --> onResponse() --> " + response?.code())
                if (response.body() != null && response.code() == 200) {
                    val responseBody = response.body()
                    if (responseBody?.success == true) {
                        AppCoreCode.getInstance?.cache?.errorResponse = responseBody
                        if (AppCoreCode.getInstance?.cache?.errorResponse?.success == true)
                            view?.onUploadOfferImageSuccess()
                        else
                            view?.onUploadOfferImageFailure()
                    }
                } else {
                    try {
                        // https://stackoverflow.com/questions/38304316/how-to-handle-error-in-retrofit-2-0
                        var pojo = AppCoreCode.getInstance?.cache?.gson?.fromJson(
                            response.errorBody()?.string(),
                            ErrorResponse::class.java
                        )
                        view?.onUploadOfferImageSuccessWithErrorMsg(pojo?.error?.details.toString())
                    } catch (e: IOException) {
                    }

                }
            }

            override fun onFailure(call: Call<ErrorResponse>, t: Throwable) {
                Log.d(TAG, "postUploadGallery() --> onFailure() --> " + t.message)
                view?.onUploadOfferImageFailure()
            }
        })
    }

    public fun getProfileOffers() {
        var call = AppCoreCode.getInstance?.cache?.api?.getProfileOffers(
            "Bearer " + AppCoreCode.getInstance.cache.loginResponse?.result?.result.toString(),
            "en"
        )

        Log.d(TAG, "GetProfileGallery() -> call enqueue")
        call?.enqueue(object : Callback<GetProfileOffersResponse> {
            override fun onResponse(
                call: Call<GetProfileOffersResponse>,
                response: Response<GetProfileOffersResponse>
            ) {
                Log.d(TAG, "GetProfileGallery() --> onResponse() --> " + response?.code())
                if (response.body() != null && response.code() == 200) {
                    val responseBody = response.body()
                    AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerOffers = responseBody?.result
                    view?.onGetOffersSuccess()
                } else {
                    try {
                        // https://stackoverflow.com/questions/38304316/how-to-handle-error-in-retrofit-2-0
                        var pojo = AppCoreCode.getInstance?.cache?.gson?.fromJson(
                            response.errorBody()?.string(),
                            ErrorResponse::class.java
                        )
                        view?.onGetOffersSuccessWithErrorMsg(pojo?.error?.details.toString())
                    } catch (e: IOException) {
                    }

                }
            }

            override fun onFailure(call: Call<GetProfileOffersResponse>, t: Throwable) {
                Log.d(TAG, "GetProfileGallery() --> onFailure() --> " + t.message)
                view?.onGetOffersFailure()
            }
        })
    }

    interface View {
        fun onAddOfferSuccess()
        fun onAddOfferSuccessWithErrorMsg(msg: String)
        fun onAddOfferFailure()

        fun onUploadOfferImageSuccess()
        fun onUploadOfferImageSuccessWithErrorMsg(msg: String)
        fun onUploadOfferImageFailure()

        fun onGetOffersSuccess()
        fun onGetOffersSuccessWithErrorMsg(msg: String)
        fun onGetOffersFailure()
    }
}