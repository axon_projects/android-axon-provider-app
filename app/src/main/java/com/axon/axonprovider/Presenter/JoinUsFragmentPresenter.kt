package com.axon.axonprovider.Presenter

import android.util.Log
import androidx.fragment.app.FragmentActivity
import com.axon.axonprovider.Global.AppCoreCode
import com.axon.axonprovider.Model.*
import com.axon.axonprovider.R
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

class JoinUsFragmentPresenter {
    private val TAG = "LoginFragmentPresenter"

    private var view: View? = null

    public constructor(view: View) {
        this.view = view
    }

    public fun getProviderTypes() {
        var call = AppCoreCode.getInstance?.cache?.api?.getProviderTypes("en")
        Log.d(TAG, "getProviderTypes() -> call enqueue")
        call?.enqueue(object : Callback<ProviderTypesResponse> {
            override fun onResponse(call: Call<ProviderTypesResponse>, response: Response<ProviderTypesResponse>) {
                Log.d(TAG, "getProviderTypes() --> onResponse() --> " + response?.code())
                if (response.body() != null && response.code() == 200) {
                    val responseBody = response.body()
                    if (responseBody?.success == true) {
                        AppCoreCode.getInstance?.cache?.providerTypesResponse = responseBody
                        view?.onProviderTypeSuccess()
                    }
                } else {
                    try {
                        // https://stackoverflow.com/questions/38304316/how-to-handle-error-in-retrofit-2-0
                        var pojo = AppCoreCode.getInstance?.cache?.gson?.fromJson(
                            response.errorBody()?.string(),
                            ErrorResponse::class.java
                        )
                        if (pojo?.error?.details.toString().contains("["))
                            view?.onProviderTypeSuccessWithErrorMsg(
                                pojo?.error?.details.toString().replace("[", "").replace(
                                    "]",
                                    ""
                                )
                            )
                    } catch (e: IOException) {
                    }

                }
            }

            override fun onFailure(call: Call<ProviderTypesResponse>, t: Throwable) {
                Log.d(TAG, "getProviderTypes() --> onFailure() --> " + t.message)
                view?.onProviderTypeFailure()
            }
        })
    }

    public fun getCities() {
        var call = AppCoreCode.getInstance?.cache?.api?.getCities(1, "en")
        Log.d(TAG, "getCities() -> call enqueue")
        call?.enqueue(object : Callback<CitiesResponse> {
            override fun onResponse(call: Call<CitiesResponse>, response: Response<CitiesResponse>) {
                Log.d(TAG, "getCities() --> onResponse() --> " + response?.code())
                if (response.body() != null && response.code() == 200) {
                    val responseBody = response.body()
                    if (responseBody?.success == true) {
                        AppCoreCode.getInstance?.cache?.citiesResponse = responseBody
                        view?.onCitiesSuccess()
                    }
                } else {
                    try {
                        // https://stackoverflow.com/questions/38304316/how-to-handle-error-in-retrofit-2-0
                        var pojo = AppCoreCode.getInstance?.cache?.gson?.fromJson(
                            response.errorBody()?.string(),
                            ErrorResponse::class.java
                        )
                        Log.d(
                            TAG,
                            "getCities() -> error: " + pojo?.error?.details.toString() + "\n" + pojo?.error?.message.toString()
                        )
                        if (pojo?.error?.details.toString().contains("["))
                            view?.onCitiesSuccessWithErrorMsg(
                                pojo?.error?.details.toString().replace("[", "").replace(
                                    "]",
                                    ""
                                )
                            )
                    } catch (e: IOException) {
                    }

                }
            }

            override fun onFailure(call: Call<CitiesResponse>, t: Throwable) {
                Log.d(TAG, "getCities() --> onFailure() --> " + t.message)
                view?.onCitiesFailure()
            }
        })
    }

    public fun getAreas(cityID: Int) {
        var call = AppCoreCode.getInstance?.cache?.api?.getAreas(cityID, "en")
        Log.d(TAG, "getAreas() -> call enqueue")
        call?.enqueue(object : Callback<AreasResponse> {
            override fun onResponse(call: Call<AreasResponse>, response: Response<AreasResponse>) {
                Log.d(TAG, "getAreas() --> onResponse() --> " + response?.code())
                if (response.body() != null && response.code() == 200) {
                    val responseBody = response.body()
                    if (responseBody?.success == true) {
                        AppCoreCode.getInstance?.cache?.areasResponse = responseBody
                        view?.onAreasSuccess()
                    }
                } else {
                    try {
                        // https://stackoverflow.com/questions/38304316/how-to-handle-error-in-retrofit-2-0
                        var pojo = AppCoreCode.getInstance?.cache?.gson?.fromJson(
                            response.errorBody()?.string(),
                            ErrorResponse::class.java
                        )
                        if (pojo?.error?.details.toString().contains("["))
                            view?.onAreasSuccessWithErrorMsg(
                                pojo?.error?.details.toString().replace("[", "").replace(
                                    "]",
                                    ""
                                )
                            )
                    } catch (e: IOException) {
                    }
                }
            }

            override fun onFailure(call: Call<AreasResponse>, t: Throwable) {
                Log.d(TAG, "getAreas() --> onFailure() --> " + t.message)
                view?.onAreasFailure()
            }
        })
    }

    public fun postJoinUs(
        name: String,
        mobileNumber: String,
        emailAddress: String,
        address: String,
        areaId: Int,
        providersTypeId: Int
    ) {
        var call = AppCoreCode.getInstance?.cache?.api?.postJoinUs(
            name,
            mobileNumber,
            emailAddress,
            address,
            areaId,
            providersTypeId,
            "en"
        )
        Log.d(TAG, "postJoinUs() -> call enqueue")
        call?.enqueue(object : Callback<JoinUsResponse> {
            override fun onResponse(call: Call<JoinUsResponse>, response: Response<JoinUsResponse>) {
                Log.d(TAG, "postJoinUs() --> onResponse() --> " + response?.code())
                if (response.body() != null && response.code() == 200) {
                    val responseBody = response.body()
                    if (responseBody?.success == true) {
                        AppCoreCode.getInstance?.cache?.joinUsResponse = responseBody
                        view?.onJoinUsSuccess()
                    }
                } else {
                    try {
                        // https://stackoverflow.com/questions/38304316/how-to-handle-error-in-retrofit-2-0
                        var pojo = AppCoreCode.getInstance?.cache?.gson?.fromJson(
                            response.errorBody()?.string(),
                            ErrorResponse::class.java
                        )
                        if (pojo?.error?.details.toString().contains("["))
                            view?.onJoinUsSuccessWithErrorMsg(
                                pojo?.error?.details.toString().replace("[", "").replace(
                                    "]",
                                    ""
                                )
                            )
                    } catch (e: IOException) {
                    }
                }
            }

            override fun onFailure(call: Call<JoinUsResponse>, t: Throwable) {
                Log.d(TAG, "postJoinUs() --> onFailure() --> " + t.message)
                view?.onJoinUsFailure()
            }
        })
    }

    interface View {
        fun onProviderTypeSuccess()
        fun onProviderTypeSuccessWithErrorMsg(msg: String)
        fun onProviderTypeFailure()

        fun onCitiesSuccess()
        fun onCitiesSuccessWithErrorMsg(msg: String)
        fun onCitiesFailure()

        fun onAreasSuccess()
        fun onAreasSuccessWithErrorMsg(msg: String)
        fun onAreasFailure()

        fun onJoinUsSuccess()
        fun onJoinUsSuccessWithErrorMsg(msg: String)
        fun onJoinUsFailure()
    }
}