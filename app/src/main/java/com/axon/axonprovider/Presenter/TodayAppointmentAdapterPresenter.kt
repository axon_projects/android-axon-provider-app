package com.axon.axonprovider.Presenter

import android.util.Log
import com.axon.axonprovider.Global.AppCoreCode
import com.axon.axonprovider.Model.ErrorResponse
import com.axon.axonprovider.Model.GetAppointmentInfoByIDResponse
import com.axon.axonprovider.Model.GetProviderServicesResponse
import com.axon.axonprovider.Model.TodayAppointmentsResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

class TodayAppointmentAdapterPresenter {
    private val TAG = "TodayAppAdaptPresenter"

    private var view: View? = null

    public constructor(view: View) {
        this.view = view
    }

    public fun postCheckInAppointment(appointmentID: String) {
        var call = AppCoreCode.getInstance?.cache?.api?.postCheckInAppointment(
            appointmentID,
            "Bearer " + AppCoreCode.getInstance.cache.loginResponse?.result?.result.toString(),
            "en"
        )

        Log.d(TAG, "postCheckInAppointment() -> appointment ID: " + appointmentID)
        Log.d(TAG, "postCheckInAppointment() -> call enqueue")
        call?.enqueue(object : Callback<ErrorResponse> {
            override fun onResponse(
                call: Call<ErrorResponse>,
                response: Response<ErrorResponse>
            ) {
                Log.d(TAG, "postCheckInAppointment() --> onResponse() --> " + response?.code())
                if (response.body() != null && response.code() == 200) {
                    val responseBody = response.body()
                    if (responseBody?.success == true)
                        view?.onCheckinAppointmentSuccess()
                } else {
                    try {
                        // https://stackoverflow.com/questions/38304316/how-to-handle-error-in-retrofit-2-0
                        var pojo = AppCoreCode.getInstance?.cache?.gson?.fromJson(
                            response.errorBody()?.string(),
                            ErrorResponse::class.java
                        )
                        view?.onCheckinAppointmentSuccessWithErrorMsg(pojo?.error?.details.toString())
                    } catch (e: IOException) {
                    }

                }
            }

            override fun onFailure(call: Call<ErrorResponse>, t: Throwable) {
                Log.d(TAG, "postCheckInAppointment() --> onFailure() --> " + t.message)
                view?.onCheckinAppointmentFailure()
            }
        })
    }

    public fun getTodayAppointments() {
        var call = AppCoreCode.getInstance?.cache?.api?.getTodayAppointments(
            "Bearer " + AppCoreCode.getInstance.cache.loginResponse?.result?.result.toString(),
            "en"
        )

        Log.d(TAG, "getTodayAppointments() -> call enqueue")
        call?.enqueue(object : Callback<TodayAppointmentsResponse> {
            override fun onResponse(
                call: Call<TodayAppointmentsResponse>,
                response: Response<TodayAppointmentsResponse>
            ) {
                Log.d(TAG, "getTodayAppointments() --> onResponse() --> " + response?.code())
                if (response.body() != null && response.code() == 200) {
                    val responseBody = response.body()
                    if (responseBody?.success == true) {
                        AppCoreCode.getInstance?.cache?.todayAppointmentsResponse = responseBody
                        view?.onTodayAppointmentsSuccess()
                    }
                } else {
                    try {
                        // https://stackoverflow.com/questions/38304316/how-to-handle-error-in-retrofit-2-0
                        var pojo = AppCoreCode.getInstance?.cache?.gson?.fromJson(
                            response.errorBody()?.string(),
                            ErrorResponse::class.java
                        )
                        view?.onTodayAppointmentsSuccesWithErrorMsg(pojo?.error?.details.toString())
                    } catch (e: IOException) {
                    }

                }
            }

            override fun onFailure(call: Call<TodayAppointmentsResponse>, t: Throwable) {
                Log.d(TAG, "getTodayAppointments() --> onFailure() --> " + t.message)
                view?.onTodayAppointmentsFailure()
            }
        })
    }

    public fun getAppointmentInfoById(appointmentID: String) {
        var call = AppCoreCode.getInstance?.cache?.api?.getAppointmentInfoById(
            appointmentID,
            "Bearer " + AppCoreCode.getInstance.cache.loginResponse?.result?.result.toString(),
            "en"
        )

        Log.d(TAG, "getAppointmentInfoById() -> call enqueue")
        call?.enqueue(object : Callback<GetAppointmentInfoByIDResponse> {
            override fun onResponse(
                call: Call<GetAppointmentInfoByIDResponse>,
                response: Response<GetAppointmentInfoByIDResponse>
            ) {
                Log.d(TAG, "getAppointmentInfoById() --> onResponse() --> " + response?.code())
                if (response.body() != null && response.code() == 200) {
                    val responseBody = response.body()
                    if (responseBody?.success == true) {
                        AppCoreCode.getInstance?.cache?.getAppointmentInfoByIDResponse =
                            responseBody
                        view?.onAppointmentInfoByIdSuccess()
                    }
                } else {
                    try {
                        // https://stackoverflow.com/questions/38304316/how-to-handle-error-in-retrofit-2-0
                        var pojo = AppCoreCode.getInstance?.cache?.gson?.fromJson(
                            response.errorBody()?.string(),
                            ErrorResponse::class.java
                        )
                        view?.onAppointmentInfoByIdSuccesWithErrorMsg(pojo?.error?.details.toString())
                    } catch (e: IOException) {
                    }

                }
            }

            override fun onFailure(call: Call<GetAppointmentInfoByIDResponse>, t: Throwable) {
                Log.d(TAG, "getAppointmentInfoById() --> onFailure() --> " + t.message)
                view?.onAppointmentInfoByIdFailure()
            }
        })
    }

    public fun getProviderServices() {
        var call = AppCoreCode.getInstance?.cache?.api?.getProviderServices(
            "Bearer " + AppCoreCode.getInstance.cache.loginResponse?.result?.result.toString(),
            "en"
        )

        Log.d(TAG, "getProviderServices() -> call enqueue")
        call?.enqueue(object : Callback<GetProviderServicesResponse> {
            override fun onResponse(
                call: Call<GetProviderServicesResponse>,
                response: Response<GetProviderServicesResponse>
            ) {
                Log.d(TAG, "getProviderServices() --> onResponse() --> " + response?.code())
                if (response.body() != null && response.code() == 200) {
                    val responseBody = response.body()
                    if (responseBody?.success == true) {
                        AppCoreCode.getInstance?.cache?.getProviderServicesResponse = responseBody
                        view?.onProviderServiceSuccess()
                    }
                } else {
                    try {
                        // https://stackoverflow.com/questions/38304316/how-to-handle-error-in-retrofit-2-0
                        var pojo = AppCoreCode.getInstance?.cache?.gson?.fromJson(
                            response.errorBody()?.string(),
                            ErrorResponse::class.java
                        )
                        view?.onProviderServiceSuccesWithErrorMsg(pojo?.error?.details.toString())
                    } catch (e: IOException) {
                    }

                }
            }

            override fun onFailure(call: Call<GetProviderServicesResponse>, t: Throwable) {
                Log.d(TAG, "getProviderServices() --> onFailure() --> " + t.message)
                view?.onProviderServiceFailure()
            }
        })
    }

    interface View {
        fun onCheckinAppointmentSuccess()
        fun onCheckinAppointmentSuccessWithErrorMsg(msg: String)
        fun onCheckinAppointmentFailure()

        fun onTodayAppointmentsSuccess()
        fun onTodayAppointmentsSuccesWithErrorMsg(msg: String)
        fun onTodayAppointmentsFailure()

        fun onAppointmentInfoByIdSuccess()
        fun onAppointmentInfoByIdSuccesWithErrorMsg(msg: String)
        fun onAppointmentInfoByIdFailure()

        fun onProviderServiceSuccess()
        fun onProviderServiceSuccesWithErrorMsg(msg: String)
        fun onProviderServiceFailure()
    }
}