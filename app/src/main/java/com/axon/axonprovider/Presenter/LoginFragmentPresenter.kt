package com.axon.axonprovider.Presenter

import android.util.Log
import androidx.fragment.app.FragmentActivity
import com.axon.axonprovider.Global.AppCoreCode
import com.axon.axonprovider.Model.*
import com.axon.axonprovider.R
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException


class LoginFragmentPresenter {
    private val TAG = "LoginFragmentPresenter"

    private var view: View? = null

    public constructor(view: View) {
        this.view = view
    }

    public fun getLoginAuthFromServer(
        accountVerification: String,
        username: String,
        password: String,
        fragmentActivity: FragmentActivity?
    ) {
        var call = AppCoreCode.getInstance?.cache?.api?.login(
            accountVerification,
            username,
            password,
            "en"
        )

        Log.d(TAG, "login() -> call enqueue")
        call?.enqueue(object : Callback<LoginResponse> {
            override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {
                Log.d(TAG, "login() --> onResponse() --> " + response?.code())
                if (response.body() != null && response.code() == 200) {
                    val responseBody = response.body()
                    if (responseBody?.success == true) {
                        if (responseBody.result?.accountType.equals("AxProvider")) {
                            AppCoreCode.getInstance?.cache?.loginResponse = responseBody
                            view?.onLoginSuccess()
                        } else {
                            view?.onLoginSuccessWithErrorMsg(fragmentActivity?.getString(R.string.toast_msg_member_app_required).toString())
                        }
                    }
                } else {
                    try {
                        // https://stackoverflow.com/questions/38304316/how-to-handle-error-in-retrofit-2-0
                        var pojo = AppCoreCode.getInstance?.cache?.gson?.fromJson(
                            response.errorBody()?.string(),
                            ErrorResponse::class.java
                        )
                        view?.onLoginSuccessWithErrorMsg(
                            pojo?.error?.details.toString().replace("[", "").replace(
                                "]",
                                ""
                            )
                        )
                    } catch (e: IOException) {
                    }

                }
            }

            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                Log.d(TAG, "login() --> onFailure() --> " + t.message)
                view?.onLoginFailure()
            }
        })
    }

/*    public fun getNotifications() {
        var call = AppCoreCode.getInstance?.cache?.api?.getNotifications(
            "Bearer " + AppCoreCode.getInstance.cache.loginResponse?.result?.result.toString(),
            "en"
        )

        Log.d(TAG, "postPreviousAppointments() -> call enqueue")
        call?.enqueue(object : Callback<GetNotificationsResponse> {
            override fun onResponse(
                call: Call<GetNotificationsResponse>,
                response: Response<GetNotificationsResponse>
            ) {
                Log.d(TAG, "postPreviousAppointments() --> onResponse() --> " + response?.code())
                if (response.body() != null && response.code() == 200) {
                    val responseBody = response.body()
                    if (responseBody?.success == true) {
                        AppCoreCode.getInstance?.cache?.notificationsResponse = responseBody
                        view?.onNotificationSuccess()

                    }
                } else {
                    try {
                        // https://stackoverflow.com/questions/38304316/how-to-handle-error-in-retrofit-2-0
                        var pojo = AppCoreCode.getInstance?.cache?.gson?.fromJson(
                            response.errorBody()?.string(),
                            ErrorResponse::class.java
                        )
                        view?.onNotificationSuccessWithErrorMsg(pojo?.error?.details.toString())
                    } catch (e: IOException) {
                    }

                }
            }

            override fun onFailure(call: Call<GetNotificationsResponse>, t: Throwable) {
                Log.d(TAG, "postPreviousAppointments() --> onFailure() --> " + t.message)
                view?.onNotificationFailure()
            }
        })
    }*/

    public fun postPreviousAppointments() {
        var call = AppCoreCode.getInstance?.cache?.api?.postPreviousAppointments(
            "",
            "",
            1,
            7,
            "Bearer " + AppCoreCode.getInstance.cache.loginResponse?.result?.result.toString(),
            "en"
        )

        Log.d(TAG, "postPreviousAppointments() -> call enqueue")
        call?.enqueue(object : Callback<PreviousAppointmentsResponse> {
            override fun onResponse(
                call: Call<PreviousAppointmentsResponse>,
                response: Response<PreviousAppointmentsResponse>
            ) {
                Log.d(TAG, "postPreviousAppointments() --> onResponse() --> " + response?.code())
                if (response.body() != null && response.code() == 200) {
                    val responseBody = response.body()
                    if (responseBody?.success == true) {
                        AppCoreCode.getInstance?.cache?.previousAppointmentsResponse = responseBody
                        view?.onPreviousAppointmentsSuccess()

                    }
                } else {
                    try {
                        // https://stackoverflow.com/questions/38304316/how-to-handle-error-in-retrofit-2-0
                        var pojo = AppCoreCode.getInstance?.cache?.gson?.fromJson(
                            response.errorBody()?.string(),
                            ErrorResponse::class.java
                        )
                        view?.onPreviousAppointmentsSuccessWithErrorMsg(pojo?.error?.details.toString())
                    } catch (e: IOException) {
                    }

                }
            }

            override fun onFailure(call: Call<PreviousAppointmentsResponse>, t: Throwable) {
                Log.d(TAG, "postPreviousAppointments() --> onFailure() --> " + t.message)
                view?.onPreviousAppointmentsFailure()
            }
        })
    }

    public fun postNextAppointments() {
        var call = AppCoreCode.getInstance?.cache?.api?.postNextAppointments(
            "",
            "",
            1,
            7,
            "Bearer " + AppCoreCode.getInstance.cache.loginResponse?.result?.result.toString(),
            "en"
        )

        Log.d(TAG, "postNextAppointments() -> call enqueue")
        call?.enqueue(object : Callback<NextAppointmentsResponse> {
            override fun onResponse(
                call: Call<NextAppointmentsResponse>,
                response: Response<NextAppointmentsResponse>
            ) {
                Log.d(TAG, "postNextAppointments() --> onResponse() --> " + response?.code())
                if (response.body() != null && response.code() == 200) {
                    val responseBody = response.body()
                    if (responseBody?.success == true) {
                        AppCoreCode.getInstance?.cache?.nextAppointmentsResponse = responseBody
                        view?.onNextAppointmentsSuccess()

                    }
                } else {
                    try {
                        // https://stackoverflow.com/questions/38304316/how-to-handle-error-in-retrofit-2-0
                        var pojo = AppCoreCode.getInstance?.cache?.gson?.fromJson(
                            response.errorBody()?.string(),
                            ErrorResponse::class.java
                        )
                        view?.onNextAppointmentsSuccessWithErrorMsg(pojo?.error?.details.toString())
                    } catch (e: IOException) {
                    }

                }
            }

            override fun onFailure(call: Call<NextAppointmentsResponse>, t: Throwable) {
                Log.d(TAG, "postNextAppointments() --> onFailure() --> " + t.message)
                view?.onNextAppointmentsFailure()
            }
        })
    }

    public fun postAllReviews() {
        var call = AppCoreCode.getInstance?.cache?.api?.postAllReviews(
            1,
            7,
            "Bearer " + AppCoreCode.getInstance.cache.loginResponse?.result?.result.toString(),
            "en"
        )

        Log.d(TAG, "postAllReviews() -> call enqueue")
        call?.enqueue(object : Callback<AllReviewsResponse> {
            override fun onResponse(
                call: Call<AllReviewsResponse>,
                response: Response<AllReviewsResponse>
            ) {
                Log.d(TAG, "postAllReviews() --> onResponse() --> " + response?.code())
                if (response.body() != null && response.code() == 200) {
                    val responseBody = response.body()
                    if (responseBody?.success == true) {
                        AppCoreCode.getInstance?.cache?.allReviewsResponse = responseBody
                        view?.onAllReviewsSuccess()

                    }
                } else {
                    try {
                        // https://stackoverflow.com/questions/38304316/how-to-handle-error-in-retrofit-2-0
                        var pojo = AppCoreCode.getInstance?.cache?.gson?.fromJson(
                            response.errorBody()?.string(),
                            ErrorResponse::class.java
                        )
                        view?.onAllReviewsSuccessWithErrorMsg(pojo?.error?.details.toString())
                    } catch (e: IOException) {
                    }

                }
            }

            override fun onFailure(call: Call<AllReviewsResponse>, t: Throwable) {
                Log.d(TAG, "postAllReviews() --> onFailure() --> " + t.message)
                view?.onAllReviewsFailure()
            }
        })
    }

    public fun getProfile() {
        var call = AppCoreCode.getInstance?.cache?.api?.getProfile(
            "Bearer " + AppCoreCode.getInstance.cache.loginResponse?.result?.result.toString(),
            "en"
        )

        Log.d(TAG, "getProfile() -> call enqueue")
        call?.enqueue(object : Callback<ProfileResponse> {
            override fun onResponse(
                call: Call<ProfileResponse>,
                response: Response<ProfileResponse>
            ) {
                Log.d(TAG, "getProfile() --> onResponse() --> " + response?.code())
                if (response.body() != null && response.code() == 200) {
                    val responseBody = response.body()
                    if (responseBody?.success == true) {
                        AppCoreCode.getInstance?.cache?.profileResponse = responseBody
                        view?.onProfileSuccess()
                    }
                } else {
                    try {
                        // https://stackoverflow.com/questions/38304316/how-to-handle-error-in-retrofit-2-0
                        var pojo = AppCoreCode.getInstance?.cache?.gson?.fromJson(
                            response.errorBody()?.string(),
                            ErrorResponse::class.java
                        )
                        view?.onProfileSuccessWithErrorMsg(pojo?.error?.details.toString())
                    } catch (e: IOException) {
                    }

                }
            }

            override fun onFailure(call: Call<ProfileResponse>, t: Throwable) {
                Log.d(TAG, "getProfile() --> onFailure() --> " + t.message)
                view?.onProfileFailure()
            }
        })
    }

    public fun getDashboardCounts() {
        var call = AppCoreCode.getInstance?.cache?.api?.getDashboardCounts(
            "Bearer " + AppCoreCode.getInstance.cache.loginResponse?.result?.result.toString(),
            "en"
        )

        Log.d(TAG, "getDashboardCounts() -> call enqueue")
        call?.enqueue(object : Callback<DashboardCountsResponse> {
            override fun onResponse(
                call: Call<DashboardCountsResponse>,
                response: Response<DashboardCountsResponse>
            ) {
                Log.d(TAG, "getDashboardCounts() --> onResponse() --> " + response?.code())
                if (response.body() != null && response.code() == 200) {
                    val responseBody = response.body()
                    if (responseBody?.success == true) {
                        AppCoreCode.getInstance?.cache?.dashboardCountsResponse = responseBody
                        view?.onDashboardCountsSuccess()
                    }
                } else {
                    try {
                        // https://stackoverflow.com/questions/38304316/how-to-handle-error-in-retrofit-2-0
                        var pojo = AppCoreCode.getInstance?.cache?.gson?.fromJson(
                            response.errorBody()?.string(),
                            ErrorResponse::class.java
                        )
                        view?.onDashboardCountsSuccessWithErrorMsg(pojo?.error?.details.toString())
                    } catch (e: IOException) {
                    }

                }
            }

            override fun onFailure(call: Call<DashboardCountsResponse>, t: Throwable) {
                Log.d(TAG, "getDashboardCounts() --> onFailure() --> " + t.message)
                view?.onDashboardCountsFailure()
            }
        })
    }

    public fun getTodayAppointments() {
        var call = AppCoreCode.getInstance?.cache?.api?.getTodayAppointments(
            "Bearer " + AppCoreCode.getInstance.cache.loginResponse?.result?.result.toString(),
            "en"
        )

        Log.d(TAG, "getTodayAppointments() -> call enqueue")
        call?.enqueue(object : Callback<TodayAppointmentsResponse> {
            override fun onResponse(
                call: Call<TodayAppointmentsResponse>,
                response: Response<TodayAppointmentsResponse>
            ) {
                Log.d(TAG, "getTodayAppointments() --> onResponse() --> " + response?.code())
                if (response.body() != null && response.code() == 200) {
                    val responseBody = response.body()
                    if (responseBody?.success == true) {
                        AppCoreCode.getInstance?.cache?.todayAppointmentsResponse = responseBody
                        view?.onTodayAppointmentsSuccess()
                    }
                } else {
                    try {
                        // https://stackoverflow.com/questions/38304316/how-to-handle-error-in-retrofit-2-0
                        var pojo = AppCoreCode.getInstance?.cache?.gson?.fromJson(
                            response.errorBody()?.string(),
                            ErrorResponse::class.java
                        )
                        view?.onTodayAppointmentsSuccesWithErrorMsg(pojo?.error?.details.toString())
                    } catch (e: IOException) {
                    }

                }
            }

            override fun onFailure(call: Call<TodayAppointmentsResponse>, t: Throwable) {
                Log.d(TAG, "getTodayAppointments() --> onFailure() --> " + t.message)
                view?.onTodayAppointmentsFailure()
            }
        })
    }

    public fun getLatestReviews() {
        var call = AppCoreCode.getInstance?.cache?.api?.getLatestReviews(
            "Bearer " + AppCoreCode.getInstance.cache.loginResponse?.result?.result.toString(),
            "en"
        )

        Log.d(TAG, "getLatestReviews() -> call enqueue")
        call?.enqueue(object : Callback<LatestReviewsResponse> {
            override fun onResponse(
                call: Call<LatestReviewsResponse>,
                response: Response<LatestReviewsResponse>
            ) {
                Log.d(TAG, "getLatestReviews() --> onResponse() --> " + response?.code())
                if (response.body() != null && response.code() == 200) {
                    val responseBody = response.body()
                    if (responseBody?.success == true) {
                        AppCoreCode.getInstance?.cache?.latestReviewsResponse = responseBody
                        view?.onLatestReviewsSuccess()
                    }
                } else {
                    try {
                        // https://stackoverflow.com/questions/38304316/how-to-handle-error-in-retrofit-2-0
                        var pojo = AppCoreCode.getInstance?.cache?.gson?.fromJson(
                            response.errorBody()?.string(),
                            ErrorResponse::class.java
                        )
                        view?.onLatestReviewsSuccesWithErrorMsg(pojo?.error?.details.toString())
                    } catch (e: IOException) {
                    }
                }
            }

            override fun onFailure(call: Call<LatestReviewsResponse>, t: Throwable) {
                Log.d(TAG, "getLatestReviews() --> onFailure() --> " + t.message)
                view?.onLatestReviewsFailure()
            }
        })
    }

    interface View {
        fun onLoginSuccess()
        fun onLoginSuccessWithErrorMsg(msg: String)
        fun onLoginFailure()

        /*fun onNotificationSuccess()
        fun onNotificationSuccessWithErrorMsg(msg: String)
        fun onNotificationFailure()*/

        fun onPreviousAppointmentsSuccess()
        fun onPreviousAppointmentsSuccessWithErrorMsg(msg: String)
        fun onPreviousAppointmentsFailure()

        fun onNextAppointmentsSuccess()
        fun onNextAppointmentsSuccessWithErrorMsg(msg: String)
        fun onNextAppointmentsFailure()

        fun onAllReviewsSuccess()
        fun onAllReviewsSuccessWithErrorMsg(msg: String)
        fun onAllReviewsFailure()

        fun onProfileSuccess()
        fun onProfileSuccessWithErrorMsg(msg: String)
        fun onProfileFailure()

        fun onDashboardCountsSuccess()
        fun onDashboardCountsSuccessWithErrorMsg(msg: String)
        fun onDashboardCountsFailure()

        fun onTodayAppointmentsSuccess()
        fun onTodayAppointmentsSuccesWithErrorMsg(msg: String)
        fun onTodayAppointmentsFailure()

        fun onLatestReviewsSuccess()
        fun onLatestReviewsSuccesWithErrorMsg(msg: String)
        fun onLatestReviewsFailure()
    }
}