package com.axon.axonprovider.Presenter

import android.util.Log
import com.axon.axonprovider.Global.AppCoreCode
import com.axon.axonprovider.Model.ErrorResponse
import com.axon.axonprovider.Model.GetProfileGalleryResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

class GalleryAlbumAdapterPresenter {
    private val TAG = "GalleryAdaptPresenter"

    private var view: View? = null

    public constructor(view: View) {
        this.view = view
    }

    public fun postDeleteGallery(imageID: String) {
        var call = AppCoreCode.getInstance?.cache?.api?.postDeleteGallery(
            imageID,
            "Bearer " + AppCoreCode.getInstance.cache.loginResponse?.result?.result.toString(),
            "en"
        )

        Log.d(TAG, "postDeleteGallery() -> image ID: " + imageID)
        Log.d(TAG, "postDeleteGallery() -> call enqueue")
        call?.enqueue(object : Callback<ErrorResponse> {
            override fun onResponse(
                call: Call<ErrorResponse>,
                response: Response<ErrorResponse>
            ) {
                Log.d(TAG, "postDeleteGallery() --> onResponse() --> " + response?.code())
                if (response.body() != null && response.code() == 200) {
                    val responseBody = response.body()
                    if (responseBody?.success == true)
                        view?.onDeleteGallerySuccess()
                } else {
                    try {
                        // https://stackoverflow.com/questions/38304316/how-to-handle-error-in-retrofit-2-0
                        var pojo = AppCoreCode.getInstance?.cache?.gson?.fromJson(
                            response.errorBody()?.string(),
                            ErrorResponse::class.java
                        )
                        view?.onDeleteGallerySuccessWithErrorMsg(pojo?.error?.details.toString())
                    } catch (e: IOException) {
                    }

                }
            }

            override fun onFailure(call: Call<ErrorResponse>, t: Throwable) {
                Log.d(TAG, "postDeleteGallery() --> onFailure() --> " + t.message)
                view?.onDeleteGalleryFailure()
            }
        })
    }

    public fun getProfileGallery() {
        var call = AppCoreCode.getInstance?.cache?.api?.getProfileGallery(
            "Bearer " + AppCoreCode.getInstance.cache.loginResponse?.result?.result.toString(),
            "en"
        )

        Log.d(TAG, "GetProfileGallery() -> call enqueue")
        call?.enqueue(object : Callback<GetProfileGalleryResponse> {
            override fun onResponse(
                call: Call<GetProfileGalleryResponse>,
                response: Response<GetProfileGalleryResponse>
            ) {
                Log.d(TAG, "GetProfileGallery() --> onResponse() --> " + response?.code())
                if (response.body() != null && response.code() == 200) {
                    val responseBody = response.body()
                    AppCoreCode.getInstance?.cache?.profileResponse?.result?.mediaFiles =
                        responseBody?.result
                    view?.onGetGallerySuccess()
                } else {
                    try {
                        // https://stackoverflow.com/questions/38304316/how-to-handle-error-in-retrofit-2-0
                        var pojo = AppCoreCode.getInstance?.cache?.gson?.fromJson(
                            response.errorBody()?.string(),
                            ErrorResponse::class.java
                        )
                        view?.onGetGallerySuccessWithErrorMsg(pojo?.error?.details.toString())
                    } catch (e: IOException) {
                    }

                }
            }

            override fun onFailure(call: Call<GetProfileGalleryResponse>, t: Throwable) {
                Log.d(TAG, "GetProfileGallery() --> onFailure() --> " + t.message)
                view?.onGetGalleryFailure()
            }
        })
    }

    interface View {
        fun onDeleteGallerySuccess()
        fun onDeleteGallerySuccessWithErrorMsg(msg: String)
        fun onDeleteGalleryFailure()

        fun onGetGallerySuccess()
        fun onGetGallerySuccessWithErrorMsg(msg: String)
        fun onGetGalleryFailure()
    }
}