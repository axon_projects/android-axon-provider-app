package com.axon.axonprovider.Presenter

import android.util.Log
import com.axon.axonprovider.Global.AppCoreCode
import com.axon.axonprovider.Model.AddServiceToReservationResponse
import com.axon.axonprovider.Model.DeleteServiceFromReservationResponse
import com.axon.axonprovider.Model.ErrorResponse
import com.axon.axonprovider.Model.GetProviderServicesResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

class MemberViewDetailsFragmentPresenter {
    private val TAG = "MemberFragmentPresenter"

    private var view: View? = null

    public constructor(view: View) {
        this.view = view
    }
    public fun postCheckOutAppointment(
        reservationId: String,
        totalPrice: String,
        totalNetPrice: String
    ) {
        var call = AppCoreCode.getInstance?.cache?.api?.postCheckOutAppointment(
            reservationId,
            totalPrice,
            totalNetPrice,
            "Bearer " + AppCoreCode.getInstance.cache.loginResponse?.result?.result.toString(),
            "en"
        )

        Log.d(TAG, "postCheckOutAppointment() -> call enqueue")
        call?.enqueue(object : Callback<ErrorResponse> {
            override fun onResponse(
                call: Call<ErrorResponse>,
                response: Response<ErrorResponse>
            ) {
                Log.d(TAG, "postCheckOutAppointment() --> onResponse() --> " + response?.code())
                if (response.body() != null && response.code() == 200) {
                    val responseBody = response.body()
                    if (responseBody?.success == true) {
                        AppCoreCode.getInstance?.cache?.errorResponse = responseBody
                        view?.onCheckOutAppointmentSuccess()
                    }
                } else {
                    try {
                        // https://stackoverflow.com/questions/38304316/how-to-handle-error-in-retrofit-2-0
                        var pojo = AppCoreCode.getInstance?.cache?.gson?.fromJson(
                            response.errorBody()?.string(),
                            ErrorResponse::class.java
                        )
                        view?.onCheckOutAppointmentSuccesWithErrorMsg(pojo?.error?.details.toString())
                    } catch (e: IOException) {
                    }

                }
            }

            override fun onFailure(call: Call<ErrorResponse>, t: Throwable) {
                Log.d(TAG, "postCheckOutAppointment() --> onFailure() --> " + t.message)
                view?.onCheckOutAppointmentFailure()
            }
        })
    }

    public fun postAddServiceToReservation(
        id: String,
        price: String,
        discountPercentage: String,
        reservationId: String
    ) {
        var call = AppCoreCode.getInstance?.cache?.api?.postAddServiceToReservation(
            id,
            price,
            discountPercentage,
            reservationId,
            "Bearer " + AppCoreCode.getInstance.cache.loginResponse?.result?.result.toString(),
            "en"
        )

        Log.d(TAG, "postAddServiceToReservation() -> call enqueue")
        call?.enqueue(object : Callback<AddServiceToReservationResponse> {
            override fun onResponse(
                call: Call<AddServiceToReservationResponse>,
                response: Response<AddServiceToReservationResponse>
            ) {
                Log.d(TAG, "postAddServiceToReservation() --> onResponse() --> " + response?.code())
                if (response.body() != null && response.code() == 200) {
                    val responseBody = response.body()
                    if (responseBody?.success == true) {
                        AppCoreCode.getInstance?.cache?.addServiceToReservationResponse = responseBody
                        view?.onAddServiceToReservationSuccess()
                    }
                } else {
                    try {
                        // https://stackoverflow.com/questions/38304316/how-to-handle-error-in-retrofit-2-0
                        var pojo = AppCoreCode.getInstance?.cache?.gson?.fromJson(
                            response.errorBody()?.string(),
                            ErrorResponse::class.java
                        )
                        view?.onAddServiceToReservationSuccesWithErrorMsg(pojo?.error?.details.toString())
                    } catch (e: IOException) {
                    }

                }
            }

            override fun onFailure(call: Call<AddServiceToReservationResponse>, t: Throwable) {
                Log.d(TAG, "postCheckOutAppointment() --> onFailure() --> " + t.message)
                view?.onAddServiceToReservationFailure()
            }
        })
    }

    interface View {
        fun onCheckOutAppointmentSuccess()
        fun onCheckOutAppointmentSuccesWithErrorMsg(msg: String)
        fun onCheckOutAppointmentFailure()

        fun onAddServiceToReservationSuccess()
        fun onAddServiceToReservationSuccesWithErrorMsg(msg: String)
        fun onAddServiceToReservationFailure()
    }
}