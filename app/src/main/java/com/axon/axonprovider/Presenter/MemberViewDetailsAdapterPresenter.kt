package com.axon.axonprovider.Presenter

import android.util.Log
import com.axon.axonprovider.Global.AppCoreCode
import com.axon.axonprovider.Model.DeleteServiceFromReservationResponse
import com.axon.axonprovider.Model.ErrorResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

class MemberViewDetailsAdapterPresenter {

    private val TAG = "MemberAdapterPresenter"

    private var view: View? = null

    public constructor(view: View) {
        this.view = view
    }

    public fun deleteServiceFromReservation(
        reservationServiceId: String,
        reservationId: String
    ) {
        var call = AppCoreCode.getInstance?.cache?.api?.deleteServiceFromReservation(
            reservationServiceId,
            reservationId,
            "Bearer " + AppCoreCode.getInstance.cache.loginResponse?.result?.result.toString(),
            "en"
        )

        Log.d(TAG, "postAddServiceToReservation() -> call enqueue")
        call?.enqueue(object : Callback<DeleteServiceFromReservationResponse> {
            override fun onResponse(
                call: Call<DeleteServiceFromReservationResponse>,
                response: Response<DeleteServiceFromReservationResponse>
            ) {
                Log.d(TAG, "postAddServiceToReservation() --> onResponse() --> " + response?.code())
                if (response.body() != null && response.code() == 200) {
                    val responseBody = response.body()
                    if (responseBody?.success == true) {
                        AppCoreCode.getInstance?.cache?.deleteServiceFromReservationResponse =
                            responseBody
                        view?.onDeleteServiceFromReservationSuccess()
                    }
                } else {
                    try {
                        // https://stackoverflow.com/questions/38304316/how-to-handle-error-in-retrofit-2-0
                        var pojo = AppCoreCode.getInstance?.cache?.gson?.fromJson(
                            response.errorBody()?.string(),
                            ErrorResponse::class.java
                        )
                        view?.onDeleteServiceFromReservationSuccesWithErrorMsg(pojo?.error?.details.toString())
                    } catch (e: IOException) {
                    }

                }
            }

            override fun onFailure(call: Call<DeleteServiceFromReservationResponse>, t: Throwable) {
                Log.d(TAG, "postCheckOutAppointment() --> onFailure() --> " + t.message)
                view?.onDeleteServiceFromReservationFailure()
            }
        })
    }

    interface View {
        fun onDeleteServiceFromReservationSuccess()
        fun onDeleteServiceFromReservationSuccesWithErrorMsg(msg: String)
        fun onDeleteServiceFromReservationFailure()
    }
}