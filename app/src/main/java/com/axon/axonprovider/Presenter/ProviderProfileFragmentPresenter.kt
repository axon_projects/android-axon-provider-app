package com.axon.axonprovider.Presenter

import android.net.Uri
import android.util.Log
import com.axon.axonprovider.Global.AppCoreCode
import com.axon.axonprovider.Model.ErrorResponse
import com.axon.axonprovider.Model.PreviousAppointmentsResponse
import com.github.mikephil.charting.utils.FileUtils
import okhttp3.MediaType
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import okhttp3.RequestBody
import java.io.File
import okhttp3.MultipartBody

class ProviderProfileFragmentPresenter {
    private val TAG = "ProviderProfiPresenter"

    private var view: View? = null

    public constructor(view: View) {
        this.view = view
    }

    public fun postUpdateDesc(newDescription: String) {
        var call = AppCoreCode.getInstance?.cache?.api?.postUpdateDescription(
            newDescription,
            "Bearer " + AppCoreCode.getInstance.cache.loginResponse?.result?.result.toString(),
            "en"
        )

        Log.d(TAG, "postUpdateDesc() -> call enqueue")
        call?.enqueue(object : Callback<ErrorResponse> {
            override fun onResponse(
                call: Call<ErrorResponse>,
                response: Response<ErrorResponse>
            ) {
                Log.d(TAG, "postUpdateDesc() --> onResponse() --> " + response?.code())
                if (response.body() != null && response.code() == 200) {
                    val responseBody = response.body()
                    if (responseBody?.success == true) {
                        AppCoreCode.getInstance?.cache?.errorResponse = responseBody
                        if (AppCoreCode.getInstance?.cache?.errorResponse?.success == true)
                            view?.onUpdateDescSuccess()
                        else
                            view?.onUpdateDescFailure()
                    }
                } else {
                    try {
                        // https://stackoverflow.com/questions/38304316/how-to-handle-error-in-retrofit-2-0
                        var pojo = AppCoreCode.getInstance?.cache?.gson?.fromJson(
                            response.errorBody()?.string(),
                            ErrorResponse::class.java
                        )
                        view?.onUpdateDescSuccessWithErrorMsg(pojo?.error?.details.toString())
                    } catch (e: IOException) {
                    }

                }
            }

            override fun onFailure(call: Call<ErrorResponse>, t: Throwable) {
                Log.d(TAG, "postUpdateDesc() --> onFailure() --> " + t.message)
                view?.onUpdateDescFailure()
            }
        })
    }

    public fun postUploadAvatar(selectedNewAvatar: String) {
        var file = File(selectedNewAvatar)
        var requestedFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
        var profilePic = MultipartBody.Part.createFormData("ProviderAvatar", file.name, requestedFile)

        var call = AppCoreCode.getInstance?.cache?.api?.postUploadAvatar(
            profilePic,
            "Bearer " + AppCoreCode.getInstance.cache.loginResponse?.result?.result.toString(),
            "en"
        )

        Log.d(TAG, "postUpdateDesc() -> call enqueue")
        call?.enqueue(object : Callback<ErrorResponse> {
            override fun onResponse(
                call: Call<ErrorResponse>,
                response: Response<ErrorResponse>
            ) {
                Log.d(TAG, "postUploadAvatar() --> onResponse() --> " + response?.code())
                if (response.body() != null && response.code() == 200) {
                    val responseBody = response.body()
                    if (responseBody?.success == true) {
                        AppCoreCode.getInstance?.cache?.errorResponse = responseBody
                        if (AppCoreCode.getInstance?.cache?.errorResponse?.success == true)
                            view?.onUploadAvatarSuccess()
                        else
                            view?.onUploadAvatarFailure()
                    }
                } else {
                    try {
                        // https://stackoverflow.com/questions/38304316/how-to-handle-error-in-retrofit-2-0
                        var pojo = AppCoreCode.getInstance?.cache?.gson?.fromJson(
                            response.errorBody()?.string(),
                            ErrorResponse::class.java
                        )
                        view?.onUploadAvatarSuccessWithErrorMsg(pojo?.error?.details.toString())
                    } catch (e: IOException) {
                    }

                }
            }

            override fun onFailure(call: Call<ErrorResponse>, t: Throwable) {
                Log.d(TAG, "postUploadAvatar() --> onFailure() --> " + t.message)
                view?.onUploadAvatarFailure()
            }
        })
    }

    interface View {
        fun onUpdateDescSuccess()
        fun onUpdateDescSuccessWithErrorMsg(msg: String)
        fun onUpdateDescFailure()

        fun onUploadAvatarSuccess()
        fun onUploadAvatarSuccessWithErrorMsg(msg: String)
        fun onUploadAvatarFailure()
    }
}