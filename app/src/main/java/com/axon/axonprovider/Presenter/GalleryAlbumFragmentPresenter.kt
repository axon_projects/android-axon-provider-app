package com.axon.axonprovider.Presenter

import android.util.Log
import com.axon.axonprovider.Global.AppCoreCode
import com.axon.axonprovider.Model.ErrorResponse
import com.axon.axonprovider.Model.GetProfileGalleryResponse
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.IOException

class GalleryAlbumFragmentPresenter {
    private val TAG = "GalleryAlbumPresenter"

    private var view: View? = null

    public constructor(view: View) {
        this.view = view
    }

    public fun postUploadGallery(selectedNewAvatar: String) {
        var file = File(selectedNewAvatar)
        var requestedFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
        var profilePic =
            MultipartBody.Part.createFormData("ProviderGallery", file.name, requestedFile)

        var call = AppCoreCode.getInstance?.cache?.api?.postUploadGallery(
            profilePic,
            "Bearer " + AppCoreCode.getInstance.cache.loginResponse?.result?.result.toString(),
            "en"
        )

        Log.d(TAG, "postUploadGallery() -> call enqueue")
        call?.enqueue(object : Callback<ErrorResponse> {
            override fun onResponse(
                call: Call<ErrorResponse>,
                response: Response<ErrorResponse>
            ) {
                Log.d(TAG, "postUploadGallery() --> onResponse() --> " + response?.code())
                if (response.body() != null && response.code() == 200) {
                    val responseBody = response.body()
                    if (responseBody?.success == true) {
                        AppCoreCode.getInstance?.cache?.errorResponse = responseBody
                        if (AppCoreCode.getInstance?.cache?.errorResponse?.success == true)
                            view?.onUploadGallerySuccess()
                        else
                            view?.onUploadGalleryFailure()
                    }
                } else {
                    try {
                        // https://stackoverflow.com/questions/38304316/how-to-handle-error-in-retrofit-2-0
                        var pojo = AppCoreCode.getInstance?.cache?.gson?.fromJson(
                            response.errorBody()?.string(),
                            ErrorResponse::class.java
                        )
                        view?.onUploadGallerySuccessWithErrorMsg(pojo?.error?.details.toString())
                    } catch (e: IOException) {
                    }

                }
            }

            override fun onFailure(call: Call<ErrorResponse>, t: Throwable) {
                Log.d(TAG, "postUploadGallery() --> onFailure() --> " + t.message)
                view?.onUploadGalleryFailure()
            }
        })
    }

    public fun getProfileGallery() {
        var call = AppCoreCode.getInstance?.cache?.api?.getProfileGallery(
            "Bearer " + AppCoreCode.getInstance.cache.loginResponse?.result?.result.toString(),
            "en"
        )

        Log.d(TAG, "GetProfileGallery() -> call enqueue")
        call?.enqueue(object : Callback<GetProfileGalleryResponse> {
            override fun onResponse(
                call: Call<GetProfileGalleryResponse>,
                response: Response<GetProfileGalleryResponse>
            ) {
                Log.d(TAG, "GetProfileGallery() --> onResponse() --> " + response?.code())
                if (response.body() != null && response.code() == 200) {
                    val responseBody = response.body()
                    AppCoreCode.getInstance?.cache?.profileResponse?.result?.mediaFiles =
                        responseBody?.result
                    view?.onGetGallerySuccess()
                } else {
                    try {
                        // https://stackoverflow.com/questions/38304316/how-to-handle-error-in-retrofit-2-0
                        var pojo = AppCoreCode.getInstance?.cache?.gson?.fromJson(
                            response.errorBody()?.string(),
                            ErrorResponse::class.java
                        )
                        view?.onGetGallerySuccessWithErrorMsg(pojo?.error?.details.toString())
                    } catch (e: IOException) {
                    }

                }
            }

            override fun onFailure(call: Call<GetProfileGalleryResponse>, t: Throwable) {
                Log.d(TAG, "GetProfileGallery() --> onFailure() --> " + t.message)
                view?.onGetGalleryFailure()
            }
        })
    }

    interface View {
        fun onUploadGallerySuccess()
        fun onUploadGallerySuccessWithErrorMsg(msg: String)
        fun onUploadGalleryFailure()

        fun onGetGallerySuccess()
        fun onGetGallerySuccessWithErrorMsg(msg: String)
        fun onGetGalleryFailure()
    }
}