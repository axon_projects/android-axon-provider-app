package com.axon.axonprovider.Presenter

import android.util.Log
import com.axon.axonprovider.Global.AppCoreCode
import com.axon.axonprovider.Model.ErrorResponse
import com.axon.axonprovider.Model.GetProfileGalleryResponse
import com.axon.axonprovider.Model.GetProfileOffersResponse
import com.axon.axonprovider.Model.ProfileResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

class OffersAlbumAdapterPresenter {
    private val TAG = "GalleryAdaptPresenter"

    private var view: View? = null

    public constructor(view: View) {
        this.view = view
    }

    public fun postDeleteOffer(offerID: String) {
        var call = AppCoreCode.getInstance?.cache?.api?.postDeleteOffer(
            offerID,
            "Bearer " + AppCoreCode.getInstance.cache.loginResponse?.result?.result.toString(),
            "en"
        )

        Log.d(TAG, "postDeleteOffer() -> image ID: " + offerID)
        Log.d(TAG, "postDeleteOffer() -> call enqueue")
        call?.enqueue(object : Callback<ErrorResponse> {
            override fun onResponse(
                call: Call<ErrorResponse>,
                response: Response<ErrorResponse>
            ) {
                Log.d(TAG, "postDeleteOffer() --> onResponse() --> " + response?.code())
                if (response.body() != null && response.code() == 200) {
                    val responseBody = response.body()
                    if (responseBody?.success == true)
                        view?.onDeleteOfferSuccess()
                } else {
                    try {
                        // https://stackoverflow.com/questions/38304316/how-to-handle-error-in-retrofit-2-0
                        var pojo = AppCoreCode.getInstance?.cache?.gson?.fromJson(
                            response.errorBody()?.string(),
                            ErrorResponse::class.java
                        )
                        view?.onDeleteOfferSuccessWithErrorMsg(pojo?.error?.details.toString())
                    } catch (e: IOException) {
                    }

                }
            }

            override fun onFailure(call: Call<ErrorResponse>, t: Throwable) {
                Log.d(TAG, "postDeleteOffer() --> onFailure() --> " + t.message)
                view?.onDeleteOfferFailure()
            }
        })
    }

    public fun getProfileOffers() {
        var call = AppCoreCode.getInstance?.cache?.api?.getProfileOffers(
            "Bearer " + AppCoreCode.getInstance.cache.loginResponse?.result?.result.toString(),
            "en"
        )

        Log.d(TAG, "GetProfileGallery() -> call enqueue")
        call?.enqueue(object : Callback<GetProfileOffersResponse> {
            override fun onResponse(
                call: Call<GetProfileOffersResponse>,
                response: Response<GetProfileOffersResponse>
            ) {
                Log.d(TAG, "GetProfileGallery() --> onResponse() --> " + response?.code())
                if (response.body() != null && response.code() == 200) {
                    val responseBody = response.body()
                    AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerOffers = responseBody?.result
                    view?.onGetOffersSuccess()
                } else {
                    try {
                        // https://stackoverflow.com/questions/38304316/how-to-handle-error-in-retrofit-2-0
                        var pojo = AppCoreCode.getInstance?.cache?.gson?.fromJson(
                            response.errorBody()?.string(),
                            ErrorResponse::class.java
                        )
                        view?.onGetOffersSuccessWithErrorMsg(pojo?.error?.details.toString())
                    } catch (e: IOException) {
                    }

                }
            }

            override fun onFailure(call: Call<GetProfileOffersResponse>, t: Throwable) {
                Log.d(TAG, "GetProfileGallery() --> onFailure() --> " + t.message)
                view?.onGetOffersFailure()
            }
        })
    }

    interface View {
        fun onDeleteOfferSuccess()
        fun onDeleteOfferSuccessWithErrorMsg(msg: String)
        fun onDeleteOfferFailure()

        fun onGetOffersSuccess()
        fun onGetOffersSuccessWithErrorMsg(msg: String)
        fun onGetOffersFailure()
    }
}