package com.axon.axonprovider.Presenter

import android.util.Log
import android.view.View
import com.axon.axonprovider.Global.AppCoreCode
import com.axon.axonprovider.Model.ErrorResponse
import com.axon.axonprovider.Model.NextAppointmentsResponse
import com.axon.axonprovider.Model.PreviousAppointmentsResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

class AppointmentsFragmentPresenter {
    private val TAG = "AppointFragPresenter"

    private var view: View? = null

    public constructor(view: View) {
        this.view = view
    }

    public fun postPreviousAppointments(memberCode: String, reservationDate: String) {
        var call = AppCoreCode.getInstance?.cache?.api?.postPreviousAppointments(
            memberCode,
            reservationDate,
            1,
            7,
            "Bearer " + AppCoreCode.getInstance.cache.loginResponse?.result?.result.toString(),
            "en"
        )

        Log.d(TAG, "postPreviousAppointments() -> call enqueue")
        call?.enqueue(object : Callback<PreviousAppointmentsResponse> {
            override fun onResponse(
                call: Call<PreviousAppointmentsResponse>,
                response: Response<PreviousAppointmentsResponse>
            ) {
                Log.d(TAG, "postPreviousAppointments() --> onResponse() --> " + response?.code())
                if (response.body() != null && response.code() == 200) {
                    val responseBody = response.body()
                    if (responseBody?.success == true) {
                        AppCoreCode.getInstance?.cache?.previousAppointmentsResponse = responseBody
                        view?.onPreviousAppointmentsSuccess()

                    }
                } else {
                    try {
                        // https://stackoverflow.com/questions/38304316/how-to-handle-error-in-retrofit-2-0
                        var pojo = AppCoreCode.getInstance?.cache?.gson?.fromJson(
                            response.errorBody()?.string(),
                            ErrorResponse::class.java
                        )
                        view?.onPreviousAppointmentsSuccessWithErrorMsg(pojo?.error?.details.toString())
                    } catch (e: IOException) {
                    }

                }
            }

            override fun onFailure(call: Call<PreviousAppointmentsResponse>, t: Throwable) {
                Log.d(TAG, "postPreviousAppointments() --> onFailure() --> " + t.message)
                view?.onPreviousAppointmentsFailure()
            }
        })
    }

    public fun postNextAppointments(memberCode: String, reservationDate: String) {
        var call = AppCoreCode.getInstance?.cache?.api?.postNextAppointments(
            memberCode,
            reservationDate,
            1,
            7,
            "Bearer " + AppCoreCode.getInstance.cache.loginResponse?.result?.result.toString(),
            "en"
        )

        Log.d(TAG, "postNextAppointments() -> call enqueue")
        call?.enqueue(object : Callback<NextAppointmentsResponse> {
            override fun onResponse(
                call: Call<NextAppointmentsResponse>,
                response: Response<NextAppointmentsResponse>
            ) {
                Log.d(TAG, "postNextAppointments() --> onResponse() --> " + response?.code())
                if (response.body() != null && response.code() == 200) {
                    val responseBody = response.body()
                    if (responseBody?.success == true) {
                        AppCoreCode.getInstance?.cache?.nextAppointmentsResponse = responseBody
                        view?.onNextAppointmentsSuccess()

                    }
                } else {
                    try {
                        // https://stackoverflow.com/questions/38304316/how-to-handle-error-in-retrofit-2-0
                        var pojo = AppCoreCode.getInstance?.cache?.gson?.fromJson(
                            response.errorBody()?.string(),
                            ErrorResponse::class.java
                        )
                        view?.onNextAppointmentsSuccessWithErrorMsg(pojo?.error?.details.toString())
                    } catch (e: IOException) {
                    }

                }
            }

            override fun onFailure(call: Call<NextAppointmentsResponse>, t: Throwable) {
                Log.d(TAG, "postNextAppointments() --> onFailure() --> " + t.message)
                view?.onNextAppointmentsFailure()
            }
        })
    }

    interface View {
        fun onPreviousAppointmentsSuccess()
        fun onPreviousAppointmentsSuccessWithErrorMsg(msg: String)
        fun onPreviousAppointmentsFailure()

        fun onNextAppointmentsSuccess()
        fun onNextAppointmentsSuccessWithErrorMsg(msg: String)
        fun onNextAppointmentsFailure()
    }
}