package com.axon.axonprovider.UI

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent

class CustomViewPager : NonSwipeableViewPager {
    private var isPagingEnabled: Boolean = true

    override fun setOffscreenPageLimit(limit: Int) {
        super.setOffscreenPageLimit(4)
    }

    constructor(context: Context) : super(context) {

    }

    constructor(context: Context, attrs: AttributeSet): super(context, attrs) {

    }

   override fun onTouchEvent(event: MotionEvent): Boolean {
        return this.isPagingEnabled && super.onTouchEvent(event)
    }

    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        return this.isPagingEnabled && super.onInterceptTouchEvent(event)
    }

    fun setPagingEnabled(b: Boolean) {
        this.isPagingEnabled = b
    }
}