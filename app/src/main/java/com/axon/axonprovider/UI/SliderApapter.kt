package com.axon.axonprovider.UI

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.viewpager.widget.PagerAdapter
import com.axon.axonprovider.R

class SliderApapter : PagerAdapter {
    internal var context: Context
    internal lateinit var layoutInflater: LayoutInflater

    // Arrays
    var slide_images = intArrayOf(R.drawable.intro, R.drawable.intro, R.drawable.intro)
    var slide_strings = intArrayOf(R.string.intro_string, R.string.intro_string, R.string.intro_string)

    constructor(context: Context) {
        this.context = context
    }

    override fun getCount(): Int {
        return slide_images.size
    }

    override fun isViewFromObject(view: View, o: Any): Boolean {
        return view === o as RelativeLayout
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        layoutInflater = LayoutInflater.from(context) as LayoutInflater
        val view = layoutInflater.inflate(R.layout.slide_layout, container, false)

        val imgSlideImage = view.findViewById<ImageView>(R.id.imgSlideImage)
        val txtDescString = view.findViewById<TextView>(R.id.txtDescString)

        imgSlideImage.setImageResource(slide_images[position])
        txtDescString.setText(slide_strings[position])

        container.addView(view)

        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as RelativeLayout)
    }
}