package com.axon.axonprovider.Global

import android.app.Activity
import android.content.SharedPreferences
import androidx.fragment.app.FragmentActivity
import cn.pedant.SweetAlert.SweetAlertDialog
import com.axon.axonprovider.R
import com.axon.axonprovider.View.Login.LoginActivity
import com.axon.axonprovider.View.ProviderMainView.MainActivity
import java.util.regex.Pattern

class AppCoreCode {
    /*Shared Preferences*/
    public var sharedpreferences: SharedPreferences? = null

    public var editor: SharedPreferences.Editor? = null

    public var sweetAlertDialog: SweetAlertDialog? = null

    public var cache: Cache

    /**
     * Singleton Object
     * will do the job because the companion object itself is a language-level singleton.
     * (The instance will be assigned when the companion object is first called.)
     */
    companion object {
        public val getInstance: AppCoreCode? = AppCoreCode()
    }

    constructor() {
        cache = Cache()
    }

    public fun showLoadingDialog(activity: Activity?) {
        if (activity != null) {
            AppCoreCode.getInstance?.sweetAlertDialog = SweetAlertDialog(activity, SweetAlertDialog.PROGRESS_TYPE)
            AppCoreCode.getInstance?.sweetAlertDialog!!.getProgressHelper().setBarColor(R.color.axonTextDarkPurpleColor)
            AppCoreCode.getInstance?.sweetAlertDialog!!.setTitleText(activity.resources.getString(R.string.sweet_alert_loading))
            AppCoreCode.getInstance?.sweetAlertDialog!!.setCancelable(false)
            AppCoreCode.getInstance?.sweetAlertDialog!!.show()
        }
    }

    public fun isValidPhone(phone: String): Boolean {
        // The given argument to compile() method
        // is regular expression. With the help of
        // regular expression we can validate mobile
        // number.
        // 1) Begins with 0 or 91
        // 2) Then contains 7 or 8 or 9.
        // 3) Then contains 9 digits
        val p = Pattern.compile("(0)?[0-9]{9}")

        // Pattern class contains matcher() method
        // to find matching between given number
        // and regular expression
        val m = p.matcher(phone)
        return m.find() && m.group() == phone
    }

    // isValidEmailAddress: Check the email address is OK
    public fun isValidEmailAddress(emailAddress: String): Boolean {
        val emailRegEx: String
        val pattern: Pattern
        // Regex for a valid email address
        emailRegEx = "^[A-Za-z0-9._%+\\-]+@[A-Za-z0-9.\\-]+\\.[A-Za-z]{2,4}$"
        // Compare the regex with the email address
        pattern = Pattern.compile(emailRegEx)
        val matcher = pattern.matcher(emailAddress)
        return if (!matcher.find()) {
            false
        } else true
    }

    fun saveValuesToSharedPreferences(key: String, flag: Boolean) {
        if (editor != null) {
            editor!!.putBoolean(key, flag)
            editor!!.commit()
        }
    }

    fun saveValuesToSharedPreferences(key: String, value: String) {
        if (editor != null) {
            editor!!.putString(key, value)
            editor!!.commit()
        }
    }

    fun getBooleanValuesFromSharedPreferences(key: String): Boolean {
        return if (sharedpreferences != null) {
            sharedpreferences!!.getBoolean(key, false)
        } else {
            false
        }
    }

    fun getStringValuesFromSharedPreferences(key: String): String {
        return if (sharedpreferences != null) {
            sharedpreferences!!.getString(key, "").toString()
        } else {
            ""
        }
    }
}