package com.axon.axonprovider.Global

import android.content.Intent
import android.net.Uri
import com.axon.axonprovider.BuildConfig
import com.axon.axonprovider.Interfaces.APiInterface
import com.axon.axonprovider.Model.*
import com.axon.axonprovider.View.Login.Fragments.LoginFragment
import com.axon.axonprovider.View.ProviderMainView.MainActivity
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import com.google.gson.Gson
import com.itkacher.okhttpprofiler.OkHttpProfilerInterceptor
import com.jaredsburrows.retrofit2.adapter.synchronous.SynchronousCallAdapterFactory
import okhttp3.OkHttpClient
import java.net.URI
import java.security.Provider
import java.util.concurrent.TimeUnit

class Cache {
    public val mainURL = "https://www.axontest.axon1.com"

    public var gson = Gson()

    public var retrofit: Retrofit? = null
    public var api: APiInterface? = null

    /**
     * Retrofit Variables
     */
    public fun initRetrofit() {
        val builder = OkHttpClient.Builder()
            .connectTimeout(3000, TimeUnit.SECONDS)
            .writeTimeout(3000, TimeUnit.SECONDS)
            .readTimeout(3000, TimeUnit.SECONDS)
            .callTimeout(3000, TimeUnit.SECONDS)
        if (BuildConfig.DEBUG) {
            builder.addInterceptor(OkHttpProfilerInterceptor())
        }

        val client = builder.build()

        retrofit = Retrofit.Builder()
            .baseUrl(mainURL)
            .client(client)
            .addCallAdapterFactory(SynchronousCallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        api = retrofit?.create(APiInterface::class.java)
    }

    /**
     * Context Activities Variables
     */
    public var mainViewContext: MainActivity? = null

    /**
     * MainActivity Variables
     */
    public var isPastAppointmentTabSelected: Boolean = false
    var currentImageSlider = ""
    public var currentImageSliderIndex: Int = 0
    public var selectedNewAvatarUri: Uri? = null
    public var memberOperationType: String = ""
    /*public var selectedNewAvatarStr: String? = null*/

    /**
     * Model Variables
     */
    public var loginResponse: LoginResponse? = null
    public var notificationsResponse: GetNotificationsResponse? = null
    public var previousAppointmentsResponse: PreviousAppointmentsResponse? = null
    public var nextAppointmentsResponse: NextAppointmentsResponse? = null
    public var allReviewsResponse: AllReviewsResponse? = null
    public var profileResponse: ProfileResponse? = null
    public var dashboardCountsResponse: DashboardCountsResponse? = null
    public var todayAppointmentsResponse: TodayAppointmentsResponse? = null
    public var getAppointmentInfoByIDResponse: GetAppointmentInfoByIDResponse? = null
    public var latestReviewsResponse: LatestReviewsResponse? = null
    public var providerTypesResponse: ProviderTypesResponse? = null
    public var citiesResponse: CitiesResponse? = null
    public var areasResponse: AreasResponse? = null
    public var joinUsResponse: JoinUsResponse? = null
    public var errorResponse: ErrorResponse? = null
    public var addOfferResponse: AddOfferResponse? = null
    public var getProviderServicesResponse: GetProviderServicesResponse? = null
    public var addServiceToReservationResponse: AddServiceToReservationResponse? = null
    public var deleteServiceFromReservationResponse: DeleteServiceFromReservationResponse? = null
}