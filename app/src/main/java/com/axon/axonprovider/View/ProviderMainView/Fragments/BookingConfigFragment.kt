package com.axon.axonprovider.View.ProviderMainView.Fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.axon.axonprovider.Adapters.Adapters.BookingConfigSettingsAdapter

import com.axon.axonprovider.R
import com.axon.axonprovider.View.ProviderMainView.MainActivity
import kotlinx.android.synthetic.main.booking_config_fragment.*

class BookingConfigFragment : Fragment() {
    private var recyclerViewBookingConfigSettings: RecyclerView? = null
    private var bookingConfigSettingsAppointmentAdapter: RecyclerView.Adapter<*>? = null

    private val TAG = "MainVewFragment"
    /*---------------------------------Fragment Override-------------------------------------------*/
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.booking_config_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initRecyclerView()
    }

    /*---------------------------------Methods-------------------------------------------*/
    private fun setViewsAction(){
        ibtnBookingConfClose.setOnClickListener(View.OnClickListener {
            if (activity != null)
                (activity as MainActivity).changeFragment(0)
        })
    }
    private fun initViews() {
        recyclerViewBookingConfigSettings = view?.findViewById(R.id.recyclerViewBookingConfigSettings);
    }

    private fun initRecyclerView() {
        /*init RecyclerView*/
        recyclerViewBookingConfigSettings?.setLayoutManager(LinearLayoutManager(activity, RecyclerView.VERTICAL, false))
        bookingConfigSettingsAppointmentAdapter = BookingConfigSettingsAdapter()
        recyclerViewBookingConfigSettings?.setAdapter(bookingConfigSettingsAppointmentAdapter)
    }

}
