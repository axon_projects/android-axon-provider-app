package com.axon.axonprovider.View.Login.Fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.axon.axonprovider.R
import com.axon.axonprovider.View.Login.LoginActivity
import kotlinx.android.synthetic.main.login_join_us_fragment.*

class LoginJoinUsFragment : Fragment() {
    /*private var btnLogin: Button? = null
    var btnSignUp: Button? = null
    private var btnChangeAppLanguage: Button? = null*/
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.login_join_us_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        setViewsAction()
    }

    private fun initViews() {
        btnChangeAppLanguage.visibility = View.GONE
    }

    private fun setViewsAction() {
        btnLogin?.setOnClickListener(View.OnClickListener {
            if (activity != null)
                (activity as LoginActivity).changeFragment(1)
        })

        btnJoinUs?.setOnClickListener(View.OnClickListener {
            if (activity != null)
                (activity as LoginActivity).changeFragment(4)
        })
    }
}
