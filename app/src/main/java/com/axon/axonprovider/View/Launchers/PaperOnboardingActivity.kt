package com.axon.axonprovider.View.Launchers

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.viewpager.widget.ViewPager
import com.axon.axonprovider.R
import com.axon.axonprovider.UI.SliderApapter
import com.axon.axonprovider.View.Login.LoginActivity

class PaperOnboardingActivity : AppCompatActivity() {
    // UI
    private var mSlideViewPager: ViewPager? = null
    private var mDotLayout: LinearLayout? = null
    private var btnSkip: Button? = null
    // Variables
    private var sliderApapter: SliderApapter? = null
    // Arrays
    private var mDots = arrayOf<TextView?>()

    private lateinit var params: LinearLayout.LayoutParams

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.paper_onboarding_activity)
        initViews()
        addDotsIndicator(0)
        mSlideViewPager?.addOnPageChangeListener(viewListener)
    }

    private fun initViews() {
        params =
            LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        params.setMargins(50, 0, 50, 0)

        mSlideViewPager = findViewById(R.id.sliderViewPager)
        mDotLayout = findViewById(R.id.llvDotsLayout)

        btnSkip = findViewById(R.id.btnSkip)

        sliderApapter = SliderApapter(this)
        mSlideViewPager?.setAdapter(sliderApapter)
        // Views actions
        setViewsListener()
    }

    private fun setViewsListener() {
        btnSkip?.setOnClickListener(View.OnClickListener {
            startActivity(
                Intent(
                    this@PaperOnboardingActivity,
                    LoginActivity::class.java
                )
            )
        })
    }

    private fun addDotsIndicator(position: Int) {
        mDots = arrayOfNulls<TextView>(3)
        mDotLayout?.removeAllViews()

        for (i in mDots!!.indices) {
            mDots[i] = TextView(this)
            mDots[i]?.text = Html.fromHtml("&#9679;")
            mDots[i]?.textSize = 25f
            mDots[i]?.layoutParams = params
            mDots[i]?.setTextColor(resources.getColor(R.color.axonTextDarkPurpleColor))

            mDotLayout?.addView(mDots[i])
        }

        if (mDots!!.size > 0) {
            if (position == mDots!!.size - 1) {
                btnSkip?.setText(getString(R.string.into_button_finish))
            } else {
                btnSkip?.setText(getString(R.string.into_button_skip))
            }
            mDots!![position]?.text = Html.fromHtml("&#9675;")
        }
    }

    private var viewListener: ViewPager.OnPageChangeListener = object : ViewPager.OnPageChangeListener {
        override fun onPageScrolled(i: Int, v: Float, i1: Int) {

        }

        override fun onPageSelected(i: Int) {
            addDotsIndicator(i)
        }

        override fun onPageScrollStateChanged(i: Int) {

        }
    }
}
