package com.axon.axonprovider.View.Login.Fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.axon.axonprovider.R
import com.axon.axonprovider.View.Login.LoginActivity
import kotlinx.android.synthetic.main.reset_password_fragment.*
import kotlinx.android.synthetic.main.reset_password_link_fragment.*

class ResetPasswordFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.reset_password_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setViewsAction()
    }

    private fun setViewsAction() {
        ibtnResetPasswordBack.setOnClickListener(View.OnClickListener {
            if (activity != null)
                (activity as LoginActivity).changeFragment(0)
        })
    }
}
