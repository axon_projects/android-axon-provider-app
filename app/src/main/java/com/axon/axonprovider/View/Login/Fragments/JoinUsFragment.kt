package com.axon.axonprovider.View.Login.Fragments

import `in`.galaxyofandroid.spinerdialog.OnSpinerItemClick
import `in`.galaxyofandroid.spinerdialog.SpinnerDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import com.axon.axonprovider.Global.AppCoreCode
import com.axon.axonprovider.Presenter.JoinUsFragmentPresenter

import com.axon.axonprovider.R
import com.axon.axonprovider.View.Login.LoginActivity
import kotlinx.android.synthetic.main.join_us_fragment.*
import kotlinx.android.synthetic.main.login_join_us_fragment.btnJoinUs
import java.util.ArrayList

class JoinUsFragment : Fragment(), JoinUsFragmentPresenter.View {
    private val TAG = "JoinUsFragment"
    private var spinnerDialog: SpinnerDialog? = null
    private var joinUsFragmentPresenter: JoinUsFragmentPresenter? = null

    private var providerTypesList: ArrayList<String>? = null
    private var citiesList: ArrayList<String>? = null
    private var areasList: ArrayList<String>? = null

    /*---------------------------------Fragment Override-------------------------------------------*/

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.join_us_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        joinUsFragmentPresenter = JoinUsFragmentPresenter(this)
        initSpinners()
        setViewsAction()
    }

    /*---------------------------------Methods-------------------------------------------*/
    private fun initSpinners() {
        if (txtJoinUsProviderType != null && txtJoinUsCity != null && txtJoinUsArea != null) {
            txtJoinUsProviderType.isEnabled = false
            txtJoinUsCity.isEnabled = false
            txtJoinUsArea.visibility = View.GONE
        }
        joinUsFragmentPresenter?.getProviderTypes()
        joinUsFragmentPresenter?.getCities()
    }

    private fun setViewsAction() {
        ibtnJoinUsBack.setOnClickListener(View.OnClickListener {
            if (activity != null)
                (activity as LoginActivity).changeFragment(0)
        })
        txtJoinUsProviderType?.setOnClickListener(View.OnClickListener {
            showSpinnerDialog("Select Provider Type", txtJoinUsProviderType, providerTypesList)
        })

        txtJoinUsCity?.setOnClickListener(View.OnClickListener {
            showSpinnerDialog("Select City", txtJoinUsCity, citiesList)
        })

        txtJoinUsArea?.setOnClickListener(View.OnClickListener {
            showSpinnerDialog("Select Area", txtJoinUsArea, areasList)
        })

        btnJoinUs?.setOnClickListener(View.OnClickListener {
            validateUserInput()
        })
    }

    private fun validateUserInput() {
        if (txtJoinUsProviderType.text.toString().isEmpty()) {
            txtJoinUsProviderType.requestFocus()
            txtJoinUsProviderType.setError("Please fill data")
            return
        }
        if (etxtJoinUsProviderName.text.toString().isEmpty()) {
            etxtJoinUsProviderName.requestFocus()
            etxtJoinUsProviderName.setError("Please fill data")
            return
        }
        if (etxtJoinUsContactNo.text.toString().isEmpty()) {
            etxtJoinUsContactNo.requestFocus()
            etxtJoinUsContactNo.setError("Please fill data")
            return
        }
        if (AppCoreCode.getInstance?.isValidPhone(etxtJoinUsContactNo.text.toString())!!) {
            etxtJoinUsContactNo.requestFocus()
            etxtJoinUsContactNo.setError("Please fill data")
            return
        }
        if (etxtJoinUsEmailAddress.text.toString().isEmpty()) {
            etxtJoinUsEmailAddress.requestFocus()
            etxtJoinUsEmailAddress.setError("Please fill data")
            return
        }
        if (!AppCoreCode.getInstance.isValidEmailAddress(etxtJoinUsEmailAddress.text.toString())) {
            etxtJoinUsEmailAddress.requestFocus()
            etxtJoinUsEmailAddress.setError("Please fill data")
            return
        }
        if (txtJoinUsCity.text.toString().isEmpty()) {
            txtJoinUsCity.requestFocus()
            txtJoinUsCity.setError("Please fill data")
            return
        }
        if (txtJoinUsArea.text.toString().isEmpty()) {
            txtJoinUsArea.requestFocus()
            txtJoinUsArea.setError("Please fill data")
            return
        }
        if (etxtJoinUsAddress.text.toString().isEmpty()) {
            etxtJoinUsAddress.requestFocus()
            etxtJoinUsAddress.setError("Please fill data")
            return
        }

        joinUsFragmentPresenter?.postJoinUs(
            etxtJoinUsProviderName.text.toString(),
            etxtJoinUsContactNo.text.toString(),
            etxtJoinUsEmailAddress.text.toString(),
            etxtJoinUsAddress.text.toString(),
            txtJoinUsArea.tag as Int,
            txtJoinUsProviderType.tag as Int
        )
    }

    private fun showSpinnerDialog(dialogTitle: String, textView: TextView, list: ArrayList<String>?) {
        spinnerDialog =
            SpinnerDialog(activity, list, dialogTitle, R.style.DialogAnimations_SmileWindow, "Close")// With 	Animation

        spinnerDialog?.setCancellable(true) // for cancellable
        spinnerDialog?.setShowKeyboard(false)// for open keyboard by default

        spinnerDialog?.bindOnSpinerListener(object : OnSpinerItemClick {
            override fun onClick(item: String, position: Int) {
                textView.text = item
                if (dialogTitle.equals("Select City")) {
                    txtJoinUsArea.setText("")
                    for (result in AppCoreCode.getInstance?.cache?.citiesResponse?.result!!) {
                        if (item.equals(result?.name)) {
                            textView.tag = result?.id
                            textView.setError(null)
                            joinUsFragmentPresenter?.getAreas(result?.id!!)
                        }
                    }
                } else if (dialogTitle.equals("Select Area")) {
                    for (result in AppCoreCode.getInstance?.cache?.areasResponse?.result!!) {
                        if (item.equals(result?.name)) {
                            textView.tag = result?.id
                            textView.setError(null)
                        }
                    }
                } else {
                    for (result in AppCoreCode.getInstance?.cache?.providerTypesResponse?.result!!) {
                        if (item.equals(result?.name)) {
                            textView.tag = result?.id
                            textView.setError(null)
                        }
                    }
                }
            }
        })

        spinnerDialog?.showSpinerDialog()
    }

    /*---------------------------------Presenter Override Methods-------------------------------------------*/

    override fun onProviderTypeSuccess() {
        if (txtJoinUsProviderType != null)
            txtJoinUsProviderType.isEnabled = true
        providerTypesList = ArrayList()
        providerTypesList?.add("")
        if (AppCoreCode.getInstance?.cache?.providerTypesResponse != null && AppCoreCode.getInstance?.cache?.providerTypesResponse?.result != null) {
            for (result in AppCoreCode.getInstance?.cache?.providerTypesResponse?.result!!) {
                providerTypesList?.add(result?.name!!)
            }
        }
    }

    override fun onProviderTypeSuccessWithErrorMsg(msg: String) {
        Toast.makeText(activity, msg, Toast.LENGTH_LONG).show()
    }

    override fun onProviderTypeFailure() {
        Toast.makeText(activity, getString(R.string.toast_msg_something_went_wrong), Toast.LENGTH_LONG).show()
    }

    override fun onCitiesSuccess() {
        if (txtJoinUsCity != null)
            txtJoinUsCity.isEnabled = true
        citiesList = ArrayList()
        citiesList?.add("")
        if (AppCoreCode.getInstance?.cache?.citiesResponse != null && AppCoreCode.getInstance?.cache?.citiesResponse?.result != null) {
            for (result in AppCoreCode.getInstance?.cache?.citiesResponse?.result!!) {
                citiesList?.add(result?.name!!)
            }
        }
    }

    override fun onCitiesSuccessWithErrorMsg(msg: String) {
        Toast.makeText(activity, msg, Toast.LENGTH_LONG).show()
    }

    override fun onCitiesFailure() {
        Toast.makeText(activity, getString(R.string.toast_msg_something_went_wrong), Toast.LENGTH_LONG).show()
    }

    override fun onAreasSuccess() {
        if (txtJoinUsArea != null)
            txtJoinUsArea.visibility = View.VISIBLE
        areasList = ArrayList()
        areasList?.add("")
        if (AppCoreCode.getInstance?.cache?.areasResponse != null && AppCoreCode.getInstance?.cache?.areasResponse?.result != null) {
            for (result in AppCoreCode.getInstance?.cache?.areasResponse?.result!!) {
                areasList?.add(result?.name!!)
            }
        }
    }

    override fun onAreasSuccessWithErrorMsg(msg: String) {
        Toast.makeText(activity, msg, Toast.LENGTH_LONG).show()
    }

    override fun onAreasFailure() {
        Toast.makeText(activity, getString(R.string.toast_msg_something_went_wrong), Toast.LENGTH_LONG).show()
    }

    override fun onJoinUsSuccess() {
        (activity as LoginActivity).changeFragment(0)
    }

    override fun onJoinUsSuccessWithErrorMsg(msg: String) {
        Toast.makeText(activity, msg, Toast.LENGTH_LONG).show()
    }

    override fun onJoinUsFailure() {
        Toast.makeText(activity, getString(R.string.toast_msg_something_went_wrong), Toast.LENGTH_LONG).show()
    }
}
