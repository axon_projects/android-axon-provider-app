package com.axon.axonprovider.View.ProviderMainView

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.core.view.GravityCompat
import android.view.MenuItem
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.View
import android.widget.RatingBar
import android.widget.TextView
import androidx.viewpager.widget.ViewPager
import com.axon.axonprovider.Adapters.PagerAdapters.MainViewPagerAdapter
import com.axon.axonprovider.Global.AppCoreCode
import com.axon.axonprovider.R
import com.axon.axonprovider.UI.CustomViewPager
import com.axon.axonprovider.View.Login.LoginActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.facebook.drawee.backends.pipeline.Fresco
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.login_fragment.*

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    val MAIN_VIEW_FRAGMENT_INDEX = 0
    val QR_CODE_FRAGMENT_INDEX = 1
    val CANCEL_APPOINTMENT_FRAGMENT_INDEX = 2
    val CHECKOUT_FRAGMENT_INDEX = 3
    val BOOKING_CONFIG_FRAGMENT_INDEX = 4
    val REVIEWS_FRAGMENT_INDEX = 5
    val SCHEDULE_SETTING_FRAGMENT_INDEX = 6
    val WORKING_HOURSE_SETTING_FRAGMENT_INDEX = 7
    val APPOINTMENT_FRAGMENT_INDEX = 8
    val PROFILE_PROVIDER_FRAGMENT_INDEX = 9
    val IMAGE_VIEWER_FRAGMENT_INDEX = 10
    val ABOUT_US_FRAGMENT_INDEX = 11
    val CONTACT_US_FRAGMENT_INDEX = 12
    val GALLERY_ALBUM_FRAGMENT_INDEX = 13
    val OFFER_ALBUM_FRAGMENT_INDEX = 14
    val NOTIFICATION_FRAGMENT_INDEX = 15
    val MEMBER_VIEW_DETAILS_FRAGMENT_INDEX = 16

    private val TAG = "MainActivity"
    private var viewPager: CustomViewPager? = null
    private var mainViewPagerAdapter: MainViewPagerAdapter? = null
    public var drawerLayout: DrawerLayout? = null
    private var sideMenuHeaderView: View? = null

    public var imgSideMenuProfilePhoto: CircleImageView? = null
    private var txtSideMenuProviderName: TextView? = null
    private var txtSideMenuProviderSpecialist: TextView? = null
    private var txtSideMenuProviderNeurologist: TextView? = null
    private var txtSideMenuProviderRate: TextView? = null
    private var ratingBarSideMenuProviderRate: RatingBar? = null

    var isProviderProfile: Boolean = false
    var isProfileGallery: Boolean = false

    /*---------------------------------Activity Override-------------------------------------------*/
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        Fresco.initialize(this);
        AppCoreCode.getInstance?.cache?.mainViewContext = this
        initSideMenu()
        setupViewPager()
    }

    override fun onBackPressed() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            /*super.onBackPressed()*/
            if (viewPager?.currentItem == IMAGE_VIEWER_FRAGMENT_INDEX) {
                Log.d(TAG, "onBackPressed() --> isProviderProfile: " + isProviderProfile)
                if (isProviderProfile) {
                    changeFragment(PROFILE_PROVIDER_FRAGMENT_INDEX)
                } else {
                    Log.d(TAG, "onBackPressed() --> isProfileGallery: " + isProfileGallery)
                    if (isProfileGallery)
                        changeFragment(GALLERY_ALBUM_FRAGMENT_INDEX)
                    else
                        changeFragment(OFFER_ALBUM_FRAGMENT_INDEX)
                }
            } else if (viewPager?.currentItem == GALLERY_ALBUM_FRAGMENT_INDEX) {
                changeFragment(PROFILE_PROVIDER_FRAGMENT_INDEX)
            } else if (viewPager?.currentItem == OFFER_ALBUM_FRAGMENT_INDEX) {
                changeFragment(PROFILE_PROVIDER_FRAGMENT_INDEX)
            } else {
                changeFragment(MAIN_VIEW_FRAGMENT_INDEX)
            }
        }
    }

    /*---------------------------------Side menu Override-------------------------------------------*/
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_check_in_out -> {
                // Handle the camera action
            }
            R.id.nav_provider_profile -> {
                changeFragment(PROFILE_PROVIDER_FRAGMENT_INDEX)
            }
            R.id.nav_booking_config -> {

            }
            R.id.nav_my_appointments -> {
                changeFragment(APPOINTMENT_FRAGMENT_INDEX)
            }
            R.id.nav_services -> {

            }
            R.id.nav_medical_reports -> {

            }
            R.id.nav_settings -> {

            }
            R.id.nav_support -> {
                changeFragment(CONTACT_US_FRAGMENT_INDEX)
            }
            R.id.nav_about_us -> {
                changeFragment(ABOUT_US_FRAGMENT_INDEX)
            }
            R.id.nav_logout -> {
                logout()
            }
        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun logout() {
        if (AppCoreCode.getInstance?.editor != null) {
            AppCoreCode.getInstance?.saveValuesToSharedPreferences(
                "Username",
                ""
            )
            AppCoreCode.getInstance?.saveValuesToSharedPreferences(
                "Password",
                ""
            )
            startActivity(Intent(this, LoginActivity::class.java))
        }
    }

    /*---------------------------------Methods-------------------------------------------*/
    private fun initSideMenu() {
        drawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        sideMenuHeaderView = navView.getHeaderView(0)

        imgSideMenuProfilePhoto = sideMenuHeaderView?.findViewById(R.id.imgSideMenuProfilePhoto)
        txtSideMenuProviderName = sideMenuHeaderView?.findViewById(R.id.txtSideMenuProviderName)
        txtSideMenuProviderSpecialist =
            sideMenuHeaderView?.findViewById(R.id.txtSideMenuProviderSpecialist)
        txtSideMenuProviderNeurologist =
            sideMenuHeaderView?.findViewById(R.id.txtSideMenuProviderNeurologist)
        txtSideMenuProviderRate = sideMenuHeaderView?.findViewById(R.id.txtSideMenuProviderRate)
        ratingBarSideMenuProviderRate =
            sideMenuHeaderView?.findViewById(R.id.ratingBarSideMenuProviderRate)

        navView.setNavigationItemSelectedListener(this)
        setViewsValues()
    }

    private fun setViewsValues() {
        Glide.with(applicationContext)
            .load(AppCoreCode.getInstance?.cache?.mainURL + "//" + AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerProfileData?.avatar)
            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
            .fitCenter()
            .into(imgSideMenuProfilePhoto!!)
        txtSideMenuProviderName?.setText(AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerProfileData?.providerName)
        txtSideMenuProviderSpecialist?.setText(AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerProfileData?.specializationName)
        txtSideMenuProviderNeurologist?.setText(AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerProfileData?.providerTypeName)
        ratingBarSideMenuProviderRate?.rating =
            AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerProfileData?.rate?.toFloat()!!
        txtSideMenuProviderRate?.setText(AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerProfileData?.rate.toString())
    }

    private fun setupViewPager() {
        Log.d(TAG, "MainViewLifeCycle --> setupViewPager() called")
        viewPager = findViewById(R.id.providerMainActivityContainer)
        mainViewPagerAdapter = MainViewPagerAdapter(supportFragmentManager)
        viewPager?.setAdapter(mainViewPagerAdapter)
        viewPager?.setPagingEnabled(false)
        /*viewPager?.setOffscreenPageLimit(11)*/
        changeFragment(MAIN_VIEW_FRAGMENT_INDEX)
    }

    fun changeFragment(fragmentIndex: Int) {
        if (fragmentIndex != IMAGE_VIEWER_FRAGMENT_INDEX) {
            if (fragmentIndex == PROFILE_PROVIDER_FRAGMENT_INDEX) {
                isProviderProfile = true
            } else {
                isProviderProfile = false
            }

            if (fragmentIndex == GALLERY_ALBUM_FRAGMENT_INDEX) {
                isProfileGallery = true
            } else {
                isProfileGallery = false
            }
        }

        Log.d(
            TAG,
            "changeFragment() --> isProviderProfile: " + isProviderProfile + " - isProfileGallery: " + isProfileGallery
        )

        Log.d(TAG, "MainViewLifeCycle --> changeFragment() called")
        Log.d(TAG, "changeFragment() --> fragmentType: $fragmentIndex -- ViewPager: $viewPager")
        viewPager?.setCurrentItem(fragmentIndex)
        viewPager?.addOnPageChangeListener(
            object : ViewPager.OnPageChangeListener {
                override fun onPageScrolled(i: Int, v: Float, i1: Int) {

                }

                override fun onPageSelected(i: Int) {
                    if (i == MAIN_VIEW_FRAGMENT_INDEX) {

                    }
                }

                override fun onPageScrollStateChanged(i: Int) {

                }
            })
    }
}
