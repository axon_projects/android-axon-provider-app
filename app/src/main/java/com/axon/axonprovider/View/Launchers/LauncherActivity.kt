package com.axon.axonprovider.View.Launchers

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.axon.axonprovider.Global.AppCoreCode
import com.axon.axonprovider.R
import com.axon.axonprovider.View.Login.LoginActivity

class LauncherActivity : AppCompatActivity() {
    /*Duration of wait*/
    private val SPLASH_DISPLAY_LENGTH = 1000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.launcher_activity)

        // Initial SharedPreferences
        AppCoreCode.getInstance?.sharedpreferences = getSharedPreferences("MyPrefs", MODE_PRIVATE)
        AppCoreCode.getInstance?.editor = AppCoreCode.getInstance?.sharedpreferences?.edit()

        startTimerToChangeToAnotherScreen()
    }

    private fun startTimerToChangeToAnotherScreen() {
        /* New Handler to start the Menu-Activity
         * and close this Splash-Screen after some seconds.*/
        Handler().postDelayed({
            /* Create an Intent that will start the Menu-Activity. */
            try {

                val mainIntent: Intent
                if (AppCoreCode.getInstance?.sharedpreferences != null && AppCoreCode.getInstance?.getBooleanValuesFromSharedPreferences(
                        "IntroScreenIsShowedBefore"
                    )
                ) {
                    mainIntent = Intent(this@LauncherActivity, LoginActivity::class.java) //
                } else {
                    mainIntent = Intent(this@LauncherActivity, PaperOnboardingActivity::class.java)
                    if (AppCoreCode.getInstance?.editor != null)
                        AppCoreCode.getInstance?.saveValuesToSharedPreferences("IntroScreenIsShowedBefore", true)
                }
                startActivity(mainIntent)
                finish()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }, SPLASH_DISPLAY_LENGTH.toLong())
    }
}
