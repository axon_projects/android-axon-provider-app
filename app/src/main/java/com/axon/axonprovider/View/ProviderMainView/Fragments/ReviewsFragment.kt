package com.axon.axonprovider.View.ProviderMainView.Fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.axon.axonprovider.Adapters.Adapters.ReviewsAdapter
import com.axon.axonprovider.Global.AppCoreCode

import com.axon.axonprovider.R
import com.axon.axonprovider.View.ProviderMainView.MainActivity
import kotlinx.android.synthetic.main.reviews_fragment.*

class ReviewsFragment : Fragment() {
    private var recyclerViewReviews: RecyclerView? = null
    private var reviewsAdapter: RecyclerView.Adapter<*>? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.reviews_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initRecyclerView()
        setViewsValues()
        setViewsAction()
    }

    /*---------------------------------Methods-------------------------------------------*/
    private fun setViewsAction() {
        ibtnReviewsBack.setOnClickListener(View.OnClickListener {
            if (activity != null)
                (activity as MainActivity).changeFragment(0)
        })
    }

    private fun initViews() {
        recyclerViewReviews = view?.findViewById(R.id.recyclerViewReviews);
    }

    private fun setViewsValues() {
        rateBarReviewsRate.rating =
            AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerProfileData?.rate?.toFloat()!!
        txtReviewsRate.setText(AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerProfileData?.rate.toString())
    }

    private fun initRecyclerView() {
        /*init Reviews RecyclerView*/
        recyclerViewReviews?.setLayoutManager(
            LinearLayoutManager(
                activity,
                RecyclerView.VERTICAL,
                false
            )
        )
        reviewsAdapter = ReviewsAdapter()
        recyclerViewReviews?.setAdapter(reviewsAdapter)
    }
}
