package com.axon.axonprovider.View.ProviderMainView.Fragments

import android.graphics.PointF
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.axon.axonprovider.R
import com.axon.axonprovider.View.ProviderMainView.MainActivity
import com.google.zxing.Result
import kotlinx.android.synthetic.main.scan_qrcode_fragment.*
import me.dm7.barcodescanner.zxing.ZXingScannerView

class ScanQRCodeFragment : Fragment(), ZXingScannerView.ResultHandler {
    private var mScannerView: ZXingScannerView? = null
    private val TAG = "ScanQRCodeFragment"
    private var isVisibleToUser = false

    /*---------------------------------Fragment Override-------------------------------------------*/
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.scan_qrcode_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setViewsActions()
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        this.isVisibleToUser = isVisibleToUser
        if (isVisibleToUser == true) {
            Log.d(TAG, "setUserVisibleHint() --> isVisibleToUser: " + isVisibleToUser)
            initQRCodeReader()
        }
    }

    override fun onResume() {
        super.onResume()
        if (isVisibleToUser) {
            mScannerView?.setResultHandler(this); // Register ourselves as a handler for scan results.
            mScannerView?.startCamera();          // Start camera on resume
        }
    }

    override fun onPause() {
        super.onPause()
        if (isVisibleToUser) {
            mScannerView?.setResultHandler(this); // Register ourselves as a handler for scan results.
            mScannerView?.stopCamera();          // Start camera on resume
        }
    }

    /*---------------------------------QRCodeReaderView Methods Override-------------------------------------------*/
    override fun handleResult(rawResult: Result?) {
        Log.d(TAG, "onQRCodeRead: " + rawResult?.text)
    }
    /*---------------------------------Methods-------------------------------------------*/

    private fun initQRCodeReader() {
        mScannerView = ZXingScannerView(activity);
    }

    private fun setViewsActions() {
        ibtnScanQRCodeClose.setOnClickListener(View.OnClickListener {
            if (activity != null)
                (activity as MainActivity).changeFragment(0)
        })
    }
}
