package com.axon.axonprovider.View.ProviderMainView.Fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.axon.axonprovider.Adapters.Adapters.NotificationAdapter
import com.axon.axonprovider.Global.AppCoreCode
import com.axon.axonprovider.Presenter.NotificationFragmentPresenter

import com.axon.axonprovider.R
import com.axon.axonprovider.View.ProviderMainView.MainActivity
import kotlinx.android.synthetic.main.notification_fragment.*

/**
 * A simple [Fragment] subclass.
 */
class NotificationFragment : Fragment(), NotificationFragmentPresenter.View {
    private var notificationAdapter: RecyclerView.Adapter<*>? = null
    var notificationFragmentPresenter: NotificationFragmentPresenter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.notification_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        notificationFragmentPresenter = NotificationFragmentPresenter(this)
        setViewsActions()
        initRecyclerView()
    }

    private fun setViewsActions(){
        ibtnNotificationBack.setOnClickListener(View.OnClickListener { if (activity != null)
            (activity as MainActivity).changeFragment(0) })
        ibtnNotificationReadAll.setOnClickListener(View.OnClickListener {
            AppCoreCode.getInstance?.showLoadingDialog(activity)
            notificationFragmentPresenter?.putUpdateAllNotification()
        })
    }

    private fun initRecyclerView() {
        /*init Reviews RecyclerView*/
        recyclerViewNotificationResults?.setLayoutManager(LinearLayoutManager(activity, RecyclerView.VERTICAL, false))
        notificationAdapter = NotificationAdapter()
        recyclerViewNotificationResults?.setAdapter(notificationAdapter)
    }

    override fun onUpdateAllNotificationSuccess() {
        notificationFragmentPresenter?.getNotifications()
    }

    override fun onUpdateAllNotificationSuccessWithErrorMsg(msg: String) {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(activity, msg, Toast.LENGTH_LONG).show()
    }

    override fun onUpdateAllNotificationFailure() {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(
            activity,
            getString(R.string.toast_msg_something_went_wrong),
            Toast.LENGTH_LONG
        ).show()
    }

    override fun onNotificationSuccess() {
        recyclerViewNotificationResults.adapter?.notifyDataSetChanged()
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
    }

    override fun onNotificationSuccessWithErrorMsg(msg: String) {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(activity, msg, Toast.LENGTH_LONG).show()
    }

    override fun onNotificationFailure() {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(
            activity,
            getString(R.string.toast_msg_something_went_wrong),
            Toast.LENGTH_LONG
        ).show()
    }
}
