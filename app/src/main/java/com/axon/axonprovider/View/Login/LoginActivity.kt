package com.axon.axonprovider.View.Login

import android.Manifest
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.axon.axonprovider.Adapters.PagerAdapters.LoginActivityPagerAdapter
import com.axon.axonprovider.Global.AppCoreCode
import com.axon.axonprovider.R
import com.axon.axonprovider.UI.CustomViewPager

class LoginActivity : AppCompatActivity() {
    internal val LOGIN_SIGN_UP_FRAGMENT_INDEX = 0
    internal val LOGIN_FRAGMENT_INDEX = 1
    internal val RESET_PASSWORD_FRAGMENT_INDEX = 2
    internal val RESET_PASSWORD_LINK_FRAGMENT_INDEX = 3
    internal val SIGNUP_WITHOUT_MAIL_FRAGMENT_INDEX = 4
    internal val SIGNUP_WITH_MAIL_FRAGMENT_INDEX = 5

    private val TAG = "LoginActivity"

    private var viewPager: CustomViewPager? = null
    lateinit var loginSignUpViewPagerAdapter: LoginActivityPagerAdapter

    var fullName: String? = null
    var birthDate: String? = null
    var mobileNo: String? = null
    var gender: String? = null
    var email: String? = null
    var password: String? = null
    /*---------------------------------Activity Override-------------------------------------------*/
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_activity)
        AppCoreCode.getInstance?.cache?.initRetrofit()

        setupViewPager()
        checkAppPermissions()
        checkIfUserAlreadyLoggedIn()
    }

    override fun onStart() {
        super.onStart()
    }

    private fun checkIfUserAlreadyLoggedIn(){
        if(!AppCoreCode.getInstance?.getStringValuesFromSharedPreferences("Username").equals("")){
            changeFragment(LOGIN_FRAGMENT_INDEX)
        }
    }

    override fun onBackPressed() {
        if (viewPager?.getCurrentItem() === LOGIN_SIGN_UP_FRAGMENT_INDEX)
            super.onBackPressed()
        else {
            changeFragment(LOGIN_SIGN_UP_FRAGMENT_INDEX)
        }
    }

    /*---------------------------------Methods-------------------------------------------*/
    private fun setupViewPager() {
        viewPager = findViewById(R.id.loginContainer)
        loginSignUpViewPagerAdapter =
            LoginActivityPagerAdapter(supportFragmentManager)
        viewPager?.setAdapter(loginSignUpViewPagerAdapter)
        viewPager?.setPagingEnabled(false)
    }

    fun changeFragment(fragmentIndex: Int) {
        Log.w("LoginActivity", "changeFragment() --> fragmentType: $fragmentIndex -- ViewPager: $viewPager")
        viewPager?.setCurrentItem(fragmentIndex)
    }

    private fun checkAppPermissions(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            Log.i(TAG, "Permission to record denied")
            ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE),
                11)
        }
    }
}
