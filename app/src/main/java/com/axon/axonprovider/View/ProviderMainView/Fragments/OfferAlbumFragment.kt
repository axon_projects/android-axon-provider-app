package com.axon.axonprovider.View.ProviderMainView.Fragments


import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.axon.axonprovider.Adapters.Adapters.OffersAlbumAdapter
import com.axon.axonprovider.Global.AppCoreCode
import com.axon.axonprovider.Global.FetchPath
import com.axon.axonprovider.Presenter.OfferAlbumFragmentPresenter
import com.axon.axonprovider.Presenter.OffersAlbumAdapterPresenter
import java.text.ParseException

import com.axon.axonprovider.R
import com.axon.axonprovider.View.ProviderMainView.MainActivity
import com.philliphsu.bottomsheetpickers.date.DatePickerDialog
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.offer_album_fragment.*
import java.text.SimpleDateFormat
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class OfferAlbumFragment : Fragment(), OfferAlbumFragmentPresenter.View,
    DatePickerDialog.OnDateSetListener {
    var TAG = "GalleryAlbumFragment"
    var customEditOffersDialog: Dialog? = null
    var imgAddOfferPhoto: ImageView? = null
    var offerAlbumFragmentPresenter: OfferAlbumFragmentPresenter? = null

    var txtAddOfferStartDate: TextView? = null
    var txtAddOfferEndDate: TextView? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.offer_album_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        offerAlbumFragmentPresenter = OfferAlbumFragmentPresenter(this)
        setViewsActions()
        setupRecyclerViews()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            1003 -> if (resultCode == Activity.RESULT_OK) {
                AppCoreCode.getInstance?.cache?.selectedNewAvatarUri = data!!.data
                Log.d(
                    TAG,
                    "onActivityResult --> resultCode: " + requestCode + " - selectedImage: " + AppCoreCode.getInstance?.cache?.selectedNewAvatarUri
                )
                if (imgAddOfferPhoto != null)
                    imgAddOfferPhoto?.setImageURI(AppCoreCode.getInstance?.cache?.selectedNewAvatarUri)
            }
        }
    }

    private fun setViewsActions() {
        ibtnOfferAlbumBack.setOnClickListener(View.OnClickListener {
            if (activity != null)
                (activity as MainActivity).changeFragment(9)
        })
        ibtOfferAlbumAdd.setOnClickListener(View.OnClickListener {
            showCustomEditAvatar()
        })
    }

    private fun setupRecyclerViews() {
        /*init Services RecyclerView*/
        val gridLayoutManager = GridLayoutManager(context, 3)
        gridLayoutManager.orientation = LinearLayoutManager.VERTICAL
        recyclerViewOffersAlbum.setLayoutManager(
            LinearLayoutManager(
                activity,
                LinearLayoutManager.HORIZONTAL,
                false
            )
        )
        recyclerViewOffersAlbum?.setLayoutManager(gridLayoutManager)
        var galleryAlbumAdapter = OffersAlbumAdapter()
        recyclerViewOffersAlbum?.setAdapter(galleryAlbumAdapter)
    }

    private fun showCustomEditAvatar() {
        customEditOffersDialog = Dialog(activity!!)
        customEditOffersDialog!!.setContentView(R.layout.add_provider_offer_custom_dialog)
        customEditOffersDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val ibtnAddOfferPhotoClose: ImageButton =
            customEditOffersDialog!!.findViewById(R.id.ibtnAddOfferPhotoClose)
        val btnAddOfferPhotoSave: Button =
            customEditOffersDialog!!.findViewById(R.id.btnAddOfferPhotoSave)
        imgAddOfferPhoto = customEditOffersDialog!!.findViewById(R.id.imgAddOfferPhoto)
        val btnAddOfferUploadPhoto: Button =
            customEditOffersDialog!!.findViewById(R.id.btnAddOfferUploadPhoto)
        val etxtAddOfferDiscount: EditText =
            customEditOffersDialog!!.findViewById(R.id.etxtAddOfferDiscount)
        txtAddOfferStartDate =
            customEditOffersDialog!!.findViewById(R.id.txtAddOfferStartDate)
        txtAddOfferEndDate =
            customEditOffersDialog!!.findViewById(R.id.txtAddOfferEndDate)
        val etxtAddOfferDescEng: EditText =
            customEditOffersDialog!!.findViewById(R.id.etxtAddOfferDescEng)
        val etxtAddOfferDescAr: EditText =
            customEditOffersDialog!!.findViewById(R.id.etxtAddOfferDescAr)

        txtAddOfferStartDate?.setOnClickListener(View.OnClickListener {
            txtAddOfferStartDate?.tag = 1
            txtAddOfferEndDate?.tag = 0
            showCustomDatePickerDialog()
        })

        txtAddOfferEndDate?.setOnClickListener(View.OnClickListener {
            txtAddOfferStartDate?.tag = 0
            txtAddOfferEndDate?.tag = 1
            showCustomDatePickerDialog()
        })

        ibtnAddOfferPhotoClose.setOnClickListener(View.OnClickListener { customEditOffersDialog!!.dismiss() })

        btnAddOfferPhotoSave.setOnClickListener(View.OnClickListener {
            if (AppCoreCode.getInstance?.cache?.selectedNewAvatarUri != null) {
                if (etxtAddOfferDiscount.text.toString().isEmpty()
                    && txtAddOfferStartDate?.text.toString().isEmpty()
                    && txtAddOfferEndDate?.text.toString().isEmpty()
                    && etxtAddOfferDescEng.text.toString().isEmpty()
                    && etxtAddOfferDescAr.text.toString().isEmpty()
                ) {
                    Toast.makeText(
                        activity,
                        "Please select complete data first!",
                        Toast.LENGTH_LONG
                    ).show()
                } else {
                    AppCoreCode.getInstance?.showLoadingDialog(activity)
                    offerAlbumFragmentPresenter?.postAddOffer(
                        etxtAddOfferDescAr.text.toString(),
                        etxtAddOfferDescEng.text.toString(),
                        txtAddOfferStartDate?.text.toString(),
                        txtAddOfferEndDate?.text.toString(),
                        etxtAddOfferDiscount.text.toString()
                    )
                }
            } else {
                Toast.makeText(activity, "Please select photo first!", Toast.LENGTH_LONG).show()
            }
        })

        btnAddOfferUploadPhoto.setOnClickListener(View.OnClickListener {
            val pickPhoto = Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            )
            startActivityForResult(pickPhoto, 1003)//one can be replaced with any action code
        })
        customEditOffersDialog!!.show()
    }

    private fun showCustomDatePickerDialog() {
        /*var date1: SimpleDateFormat? = null*/
        val now = Calendar.getInstance()
        /*try {
            date1 = SimpleDateFormat("yyyy-MM-dd")
            if (date1 != null) {
                now.time = date1.parse(txtAppointmentsFilterByDate.getText().toString())
            }
        } catch (e: ParseException) {
            e.printStackTrace()
        }*/

        val date = DatePickerDialog.Builder(
            this,
            now.get(Calendar.YEAR),
            now.get(Calendar.MONTH),
            now.get(Calendar.DAY_OF_MONTH)
        ).build()
        date.setHeaderTextColorSelected(Color.parseColor("#FFFFFF"))
        date.setHeaderTextColorUnselected(Color.parseColor("#FFFFFF"))
        date.setDayOfWeekHeaderTextColorSelected(Color.parseColor("#FFFFFF"))
        date.setDayOfWeekHeaderTextColorUnselected(Color.parseColor("#FFFFFF"))

        date.setAccentColor(Color.parseColor("#171743"))
        date.setBackgroundColor(Color.parseColor("#FFFFFF"))
        date.setHeaderColor(Color.parseColor("#171743"))
        date.setHeaderTextDark(false)
        date.show(activity?.supportFragmentManager, TAG)
    }

    /**
     * @param dialog The dialog associated with this listener.
     * @param year The year that was set.
     * @param monthOfYear The month that was set (0-11) for compatibility
     * with [Calendar].
     * @param dayOfMonth The day of the month that was set.
     */
    override fun onDateSet(
        dialog: DatePickerDialog?,
        year: Int,
        monthOfYear: Int,
        dayOfMonth: Int
    ) {
        var strMonth = (monthOfYear + 1).toString()
        var strDays = dayOfMonth.toString()
        if (monthOfYear + 1 < 10) {
            strMonth = "0" + (monthOfYear + 1)
        }
        if (dayOfMonth < 10) {
            strDays = "0$dayOfMonth"
        }
        if(txtAddOfferStartDate?.getTag() == 1){
            txtAddOfferStartDate?.text = "$year-$strMonth-$strDays"
        }else{
            txtAddOfferEndDate?.text = "$year-$strMonth-$strDays"
        }
        dialog?.dismiss()
    }

    override fun onAddOfferSuccess() {
        offerAlbumFragmentPresenter?.postUploadOfferImage(
            FetchPath.getPath(getActivity(), AppCoreCode.getInstance?.cache?.selectedNewAvatarUri)
            , AppCoreCode.getInstance?.cache?.addOfferResponse?.result?.id.toString()
        );
    }

    override fun onAddOfferSuccessWithErrorMsg(msg: String) {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(activity, msg, Toast.LENGTH_LONG).show()
    }

    override fun onAddOfferFailure() {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(
            activity,
            getString(R.string.toast_msg_something_went_wrong),
            Toast.LENGTH_LONG
        ).show()
    }

    override fun onUploadOfferImageSuccess() {
        offerAlbumFragmentPresenter?.getProfileOffers()
    }

    override fun onUploadOfferImageSuccessWithErrorMsg(msg: String) {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(activity, msg, Toast.LENGTH_LONG).show()
    }

    override fun onUploadOfferImageFailure() {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(
            activity,
            getString(R.string.toast_msg_something_went_wrong),
            Toast.LENGTH_LONG
        ).show()
    }

    override fun onGetOffersSuccess() {
        recyclerViewOffersAlbum.adapter?.notifyDataSetChanged()
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        customEditOffersDialog?.dismiss()
        Toast.makeText(activity, "Offer has been added successfully!", Toast.LENGTH_LONG)
            .show()
    }

    override fun onGetOffersSuccessWithErrorMsg(msg: String) {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(activity, msg, Toast.LENGTH_LONG).show()
    }

    override fun onGetOffersFailure() {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(
            activity,
            getString(R.string.toast_msg_something_went_wrong),
            Toast.LENGTH_LONG
        ).show()
    }

}
