package com.axon.axonprovider.View.ProviderMainView.Fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.axon.axonprovider.R
import com.axon.axonprovider.View.ProviderMainView.MainActivity
import kotlinx.android.synthetic.main.cancel_appointment_fragment.*

class CancelAppointmentFragment : Fragment() {
    /*---------------------------------Fragment Override-------------------------------------------*/
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.cancel_appointment_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setViewsAction()
    }

    private fun setViewsAction() {
        ibtnCancelAppointmentClose.setOnClickListener(View.OnClickListener {
            if (activity != null)
                (activity as MainActivity).changeFragment(0)
        })
    }
}
