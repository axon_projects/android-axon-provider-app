package com.axon.axonprovider.View.ProviderMainView.Fragments

import android.app.Activity.RESULT_OK
import android.app.Dialog
import android.content.Intent
import android.database.Cursor
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cn.pedant.SweetAlert.SweetAlertDialog
import com.axon.axonprovider.Adapters.Adapters.ProviderGallaryAdapter
import com.axon.axonprovider.Adapters.Adapters.ProviderOffersAdapter
import com.axon.axonprovider.Adapters.Adapters.ProviderServicesAdapter
import com.axon.axonprovider.Global.AppCoreCode
import com.axon.axonprovider.Global.FetchPath
import com.axon.axonprovider.Presenter.ProviderProfileFragmentPresenter

import com.axon.axonprovider.R
import com.axon.axonprovider.View.ProviderMainView.MainActivity
import com.facebook.drawee.view.SimpleDraweeView
import com.github.mikephil.charting.components.Description
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.profile_fragment_provider.*
import java.io.File
import java.util.*

class ProviderProfileFragment : Fragment(), ProviderProfileFragmentPresenter.View {
    var TAG = "ProviderProfileFragment"
    private var profileFragmentPresenter: ProviderProfileFragmentPresenter? = null

    private var sweetAlertDialog: SweetAlertDialog? = null
    private var providerServicesAdapter: ProviderServicesAdapter? = null
    private var providerGallaryAdapter: ProviderGallaryAdapter? = null
    private var providerOffersAdapter: ProviderOffersAdapter? = null

    private val ClinicsSysProviderType = 0
    private val ScanSysProviderType = 1
    private val FitnessSysProviderType = 2
    private val PhysiotherapySysProviderType = 3
    private val LabsSysProviderType = 4
    private val OpticsSysProviderType = 5
    private val PharmacySysProviderType = 6

    var customEditDescDialog: Dialog? = null
    var customEditAvatarDialog: Dialog? = null
    var imgUpdatePhotoAvatar: CircleImageView? = null

    private var newDesc = ""

    private var imgProfileProviderAvatar: SimpleDraweeView? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.profile_fragment_provider, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        profileFragmentPresenter = ProviderProfileFragmentPresenter(this)
        initViews()
        setupRecyclerViews()
        setViewsValues()
        setViewsActions()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            1001 -> if (resultCode == RESULT_OK) {
                AppCoreCode.getInstance?.cache?.selectedNewAvatarUri = data!!.data
                Log.d(
                    TAG,
                    "onActivityResult --> resultCode: " + requestCode + " - selectedImage: " + AppCoreCode.getInstance?.cache?.selectedNewAvatarUri
                )
                if (imgUpdatePhotoAvatar != null)
                    imgUpdatePhotoAvatar?.setImageURI(AppCoreCode.getInstance?.cache?.selectedNewAvatarUri)
            }
        }
    }

    private fun setViewsActions() {
        ibtnProviderProfileBack.setOnClickListener(View.OnClickListener {
            if (activity != null)
                (activity as MainActivity).changeFragment(0)
        })

        ibtnProfileProviderDescEdit.setOnClickListener(View.OnClickListener {
            showCustomEditDesc()
        })

        ibtnProfileProviderAvatarEdit.setOnClickListener(View.OnClickListener {
            showCustomEditAvatar()
        })

        ibtnProfileProviderGallaryEdit.setOnClickListener(View.OnClickListener {
            if (activity != null)
                (activity as MainActivity).changeFragment(13)
        })
        ibtnProfileProviderOffersEdit.setOnClickListener(View.OnClickListener {
            if (activity != null)
                (activity as MainActivity).changeFragment(14)
        })
    }

    private fun showCustomEditDesc() {
        customEditDescDialog = Dialog(activity!!)
        customEditDescDialog!!.setContentView(R.layout.edit_provider_desc_custom_dialog)
        customEditDescDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val ibtnEditDescClose: ImageButton =
            customEditDescDialog!!.findViewById(R.id.ibtnEditDescClose)
        val btnEditDescSave: Button = customEditDescDialog!!.findViewById(R.id.btnEditDescSave)
        val etxtEditDesc: EditText = customEditDescDialog!!.findViewById(R.id.etxtEditDesc)

        etxtEditDesc.setText(AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerProfileData?.description)

        ibtnEditDescClose.setOnClickListener(View.OnClickListener { customEditDescDialog!!.dismiss() })

        btnEditDescSave.setOnClickListener(View.OnClickListener {
            validateDescritpionInput(etxtEditDesc)
        })

        customEditDescDialog!!.show()
    }

    private fun showCustomEditAvatar() {
        customEditAvatarDialog = Dialog(activity!!)
        customEditAvatarDialog!!.setContentView(R.layout.edit_provider_avatar_custom_dialog)
        customEditAvatarDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val ibtnEditUpdatePhotoClose: ImageButton =
            customEditAvatarDialog!!.findViewById(R.id.ibtnEditUpdatePhotoClose)
        val btnEditUpdatePhotoSave: Button =
            customEditAvatarDialog!!.findViewById(R.id.btnEditUpdatePhotoSave)
        imgUpdatePhotoAvatar = customEditAvatarDialog!!.findViewById(R.id.imgUpdatePhotoAvatar)
        val btnUpdatePhotoUpload: Button =
            customEditAvatarDialog!!.findViewById(R.id.btnUpdatePhotoUpload)
        val txtUploadPhotoDialog: TextView =
            customEditAvatarDialog!!.findViewById(R.id.btnUpdatePhotoUpload)

        txtUploadPhotoDialog.setText(resources.getString(R.string.custom_dialog_profile_photo_title))

        ibtnEditUpdatePhotoClose.setOnClickListener(View.OnClickListener { customEditAvatarDialog!!.dismiss() })

        btnEditUpdatePhotoSave.setOnClickListener(View.OnClickListener {
            if (AppCoreCode.getInstance?.cache?.selectedNewAvatarUri != null) {
                AppCoreCode.getInstance?.showLoadingDialog(activity)
                profileFragmentPresenter?.postUploadAvatar(
                    FetchPath.getPath(
                        getActivity(),
                        AppCoreCode.getInstance?.cache?.selectedNewAvatarUri
                    )
                )
            } else {
                Toast.makeText(activity, "Please select photo first!", Toast.LENGTH_LONG).show()
            }
        })

        btnUpdatePhotoUpload.setOnClickListener(View.OnClickListener {
            val pickPhoto = Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            )
            startActivityForResult(pickPhoto, 1001)//one can be replaced with any action code
        })
        customEditAvatarDialog!!.show()
    }

    private fun validateDescritpionInput(etxtEditDesc: EditText) {
        if (etxtEditDesc.text.toString().trim().length > 0) {
            AppCoreCode.getInstance?.showLoadingDialog(activity)
            profileFragmentPresenter?.postUpdateDesc(etxtEditDesc.text.toString())
            this.newDesc = etxtEditDesc.text.toString()
        } else {
            etxtEditDesc.requestFocus()
            etxtEditDesc.setError("Can't be empty!")
            return
        }
    }

    private fun initViews() {
        imgProfileProviderAvatar =
            view?.findViewById<SimpleDraweeView>(R.id.imgProfileProviderAvatar);
    }

    private fun setupRecyclerViews() {
        /*init Services RecyclerView*/
        recyclerViewProviderServices?.setLayoutManager(
            LinearLayoutManager(
                activity,
                RecyclerView.VERTICAL,
                false
            )
        )
        providerServicesAdapter = ProviderServicesAdapter()
        recyclerViewProviderServices?.setAdapter(providerServicesAdapter)
        /*init Services RecyclerView*/
        recyclerViewProviderGallary?.setLayoutManager(
            LinearLayoutManager(
                activity,
                RecyclerView.HORIZONTAL,
                false
            )
        )
        providerGallaryAdapter = ProviderGallaryAdapter()
        recyclerViewProviderGallary?.setAdapter(providerGallaryAdapter)
        /*init Services RecyclerView*/
        recyclerViewProviderOffers?.setLayoutManager(
            LinearLayoutManager(
                activity,
                RecyclerView.HORIZONTAL,
                false
            )
        )
        providerOffersAdapter = ProviderOffersAdapter()
        recyclerViewProviderOffers?.setAdapter(providerOffersAdapter)
    }

    private fun setViewsValues() {
        txtProfileProviderType.setText(AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerProfileData?.providerTypeName)
        if (!AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerProfileData?.avatar?.contains(
                "nophoto-slider"
            )!!
        ) {
            AsyncTask.execute {
                if (activity != null) {
                    activity!!.runOnUiThread {
                        imgProfileProviderAvatar?.setImageURI(
                            AppCoreCode.getInstance?.cache?.mainURL
                                    + "/" + AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerProfileData?.avatar
                        )
                    }
                }
            }
        }

        txtProfileProviderName.setText(AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerProfileData?.providerName.toString())
        txtProfileProviderRate.setText((AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerProfileData?.rate).toString())
        ratingBarProfileProviderRate.rating =
            AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerProfileData?.rate?.toFloat()!!
        txtProfileProviderTitle.setText(AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerProfileData?.doctorTitleName.toString())
        txtProfileProviderTitleSpeciality.setText(AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerProfileData?.specializationName.toString())

        txtProviderProfileDesc.setText(AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerProfileData?.description)
        txtProviderProfileClinicName.setText(AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerProfileData?.providerName)
        txtProviderProfileCity.setText(AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerProfileData?.city)
        txtProviderProfileArea.setText(AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerProfileData?.area)
        if (AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerProfileData?.address != null) {
            txtProviderProfileAddress.text =
                "" + AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerProfileData?.address
            imgProviderInfoLocation.isEnabled = true
        } else {
            imgProviderInfoLocation.isEnabled = false
        }
        txtProviderProfileGallaryNum.text =
            "(" + AppCoreCode.getInstance?.cache?.profileResponse?.result?.mediaFiles?.size + " pics)"

        txtProviderProfileAddress.setOnClickListener {
            sweetAlertDialog = SweetAlertDialog(activity!!)
            sweetAlertDialog!!.setCanceledOnTouchOutside(true)
            sweetAlertDialog!!.setTitleText(resources.getString(R.string.sweet_alert_address))
            sweetAlertDialog!!.setContentText(AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerProfileData?.address)
            sweetAlertDialog!!.show()

            val btn = sweetAlertDialog!!.findViewById<View>(R.id.confirm_button)
            btn.setBackgroundColor(resources.getColor(R.color.axonTextDarkPurpleColor))
        }

        imgProviderInfoLocation.setOnClickListener {
            if (activity != null) {
                //"geo:%f,%f"
                // "geo:%f,%f?q="
                // https://stackoverflow.com/questions/6205827/how-to-open-standard-google-map-application-from-my-application
                // https://riptutorial.com/android/example/10912/open-google-map-with-specified-latitude--longitude
                // https://stackoverflow.com/questions/15700971/open-google-maps-app-and-show-marker-on-it-from-my-own-apps-activity
                // https://developers.google.com/maps/documentation/urls/android-intents
                // https://developers.google.com/maps/documentation/android-sdk/intents
                val uri = String.format(
                    Locale.ENGLISH,
                    "http://maps.google.com/maps?q=loc:%f,%f",
                    AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerProfileData?.latitude,
                    AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerProfileData?.longitude
                )
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
                activity!!.startActivity(intent)
            }
        }
        if (AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerServices != null && AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerServices?.size!! > 0) {
            lloProviderServices.visibility = View.VISIBLE
        } else {
            lloProviderServices.visibility = View.GONE
        }

        txtProviderNameTitle.setText(R.string.member_main_view_advanced_search_name_edittext)

        txtProviderProfileOffersNum.text =
            "(" + AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerOffers?.size + " offer)"

        if (AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerProfileData?.systemProviderType === ScanSysProviderType
            || AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerProfileData?.systemProviderType === OpticsSysProviderType
            || AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerProfileData?.systemProviderType === LabsSysProviderType
            || AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerProfileData?.systemProviderType === FitnessSysProviderType
            || AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerProfileData?.systemProviderType === PharmacySysProviderType
        ) {
            Log.d(
                TAG,
                "AppCoreCode.getInstance().cache.providerProfile.result.providerWorkinDays size: " + AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerWorkinDays
            )
            if (AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerWorkinDays != null && AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerWorkinDays?.size!! > 0) {
                lloOffDays.visibility = View.VISIBLE
                lloWorkingHours.visibility = View.GONE

                setDaysOff()

            } else {
                lloOffDays.visibility = View.GONE
                lloWorkingHours.visibility = View.GONE
            }
        } else {
            lloOffDays.visibility = View.GONE
            lloWorkingHours.visibility = View.GONE
        }
    }

    private fun setDaysOff() {
        resetDaysOffButtonsToNotActive(btnOffDaysSun)
        resetDaysOffButtonsToNotActive(btnOffDaysMon)
        resetDaysOffButtonsToNotActive(btnOffDaysTus)
        resetDaysOffButtonsToNotActive(btnOffDaysWed)
        resetDaysOffButtonsToNotActive(btnOffDaysThu)
        resetDaysOffButtonsToNotActive(btnOffDaysFri)
        resetDaysOffButtonsToNotActive(btnOffDaysSat)

        resetDaysToActiveState()

        setDaysOffButtonsActions()
    }

    private fun resetDaysOffButtonsToNotActive(button: Button) {
        button.setBackgroundResource(R.drawable.button_colored_bg_rounded_corners)
        button.setTextColor(Color.WHITE)
        button.tag = 0
    }

    private fun resetDaysToActiveState() {
        for (providerWorkinDays in AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerWorkinDays!!) {
            if (providerWorkinDays?.dayOfWeek === 0) {
                setDaysOffButtonActive(btnOffDaysSun)
            } else if (providerWorkinDays?.dayOfWeek === 1) {
                setDaysOffButtonActive(btnOffDaysMon)
            } else if (providerWorkinDays?.dayOfWeek === 2) {
                setDaysOffButtonActive(btnOffDaysTus)
            } else if (providerWorkinDays?.dayOfWeek === 3) {
                setDaysOffButtonActive(btnOffDaysWed)
            } else if (providerWorkinDays?.dayOfWeek === 4) {
                setDaysOffButtonActive(btnOffDaysThu)
            } else if (providerWorkinDays?.dayOfWeek === 5) {
                setDaysOffButtonActive(btnOffDaysFri)
            } else if (providerWorkinDays?.dayOfWeek === 6) {
                setDaysOffButtonActive(btnOffDaysSat)
            }
        }
    }

    private fun setDaysOffButtonActive(button: Button) {
        button.setBackgroundResource(R.drawable.button_bg_rounded_dimmed_color_corners)
        button.setTextColor(resources.getColor(R.color.axonTextDarkPurpleColor))
        button.tag = 1
    }

    private fun setDaysOffButtonsActions() {
        btnOffDaysSun.setOnClickListener {
            resetDaysToActiveState()
            if (btnOffDaysSun.tag as Int == 1) {
                btnOffDaysSun.setBackgroundResource(R.drawable.button_bg_rounded_colored_corners)
                lloWorkingHours.visibility = View.VISIBLE
                for (providerWorkinDays in AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerWorkinDays!!) {
                    if (providerWorkinDays?.dayOfWeek === 0) {
                        txtProviderProfileWorkingHours.setText(providerWorkinDays?.dateFrom + " to " + providerWorkinDays?.dateTo)
                        break
                    }
                }
            } else {
                lloWorkingHours.visibility = View.GONE
            }
        }
        btnOffDaysMon.setOnClickListener {
            resetDaysToActiveState()
            if (btnOffDaysMon.tag as Int == 1) {
                btnOffDaysMon.setBackgroundResource(R.drawable.button_bg_rounded_colored_corners)
                lloWorkingHours.visibility = View.VISIBLE
                for (providerWorkinDays in AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerWorkinDays!!) {
                    if (providerWorkinDays?.dayOfWeek === 1) {
                        txtProviderProfileWorkingHours.setText(providerWorkinDays?.dateFrom + " to " + providerWorkinDays?.dateTo)
                        break
                    }
                }
            } else {
                lloWorkingHours.visibility = View.GONE
            }
        }
        btnOffDaysTus.setOnClickListener {
            resetDaysToActiveState()
            if (btnOffDaysTus.tag as Int == 1) {
                btnOffDaysTus.setBackgroundResource(R.drawable.button_bg_rounded_colored_corners)
                lloWorkingHours.visibility = View.VISIBLE
                for (providerWorkinDays in AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerWorkinDays!!) {
                    if (providerWorkinDays?.dayOfWeek === 2) {
                        txtProviderProfileWorkingHours.setText(providerWorkinDays?.dateFrom + " to " + providerWorkinDays?.dateTo)
                        break
                    }
                }
            } else {
                lloWorkingHours.visibility = View.GONE
            }
        }
        btnOffDaysWed.setOnClickListener {
            resetDaysToActiveState()
            if (btnOffDaysWed.tag as Int == 1) {
                btnOffDaysWed.setBackgroundResource(R.drawable.button_bg_rounded_colored_corners)
                lloWorkingHours.visibility = View.VISIBLE
                for (providerWorkinDays in AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerWorkinDays!!) {
                    if (providerWorkinDays?.dayOfWeek === 3) {
                        txtProviderProfileWorkingHours.setText(providerWorkinDays?.dateFrom + " to " + providerWorkinDays?.dateTo)
                        break
                    }
                }
            } else {
                lloWorkingHours.visibility = View.GONE
            }
        }
        btnOffDaysThu.setOnClickListener {
            resetDaysToActiveState()
            if (btnOffDaysThu.tag as Int == 1) {
                btnOffDaysThu.setBackgroundResource(R.drawable.button_bg_rounded_colored_corners)
                lloWorkingHours.visibility = View.VISIBLE
                for (providerWorkinDays in AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerWorkinDays!!) {
                    if (providerWorkinDays?.dayOfWeek === 4) {
                        txtProviderProfileWorkingHours.setText(providerWorkinDays?.dateFrom + " to " + providerWorkinDays?.dateTo)
                        break
                    }
                }
            } else {
                lloWorkingHours.visibility = View.GONE
            }
        }
        btnOffDaysFri.setOnClickListener {
            resetDaysToActiveState()
            if (btnOffDaysFri.tag as Int == 1) {
                btnOffDaysFri.setBackgroundResource(R.drawable.button_bg_rounded_colored_corners)
                lloWorkingHours.visibility = View.VISIBLE
                for (providerWorkinDays in AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerWorkinDays!!) {
                    if (providerWorkinDays?.dayOfWeek === 5) {
                        txtProviderProfileWorkingHours.setText(providerWorkinDays?.dateFrom + " to " + providerWorkinDays?.dateTo)
                        break
                    }
                }
            } else {
                lloWorkingHours.visibility = View.GONE
            }
        }
        btnOffDaysSat.setOnClickListener {
            resetDaysToActiveState()
            if (btnOffDaysSat.tag as Int == 1) {
                btnOffDaysSat.setBackgroundResource(R.drawable.button_bg_rounded_colored_corners)
                lloWorkingHours.visibility = View.VISIBLE
                for (providerWorkinDays in AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerWorkinDays!!) {
                    if (providerWorkinDays?.dayOfWeek === 6) {
                        txtProviderProfileWorkingHours.setText(providerWorkinDays?.dateFrom + " to " + providerWorkinDays?.dateTo)
                        break
                    }
                }
            } else {
                lloWorkingHours.visibility = View.GONE
            }
        }

    }

    /*---------------------------------Presenter Override Methods-------------------------------------------*/
    override fun onUpdateDescSuccess() {
        customEditDescDialog?.dismiss()
        txtProviderProfileDesc.setText(this.newDesc)
        AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerProfileData?.description =
            this.newDesc
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(activity, "Description has been updated!", Toast.LENGTH_LONG).show()
    }

    override fun onUpdateDescSuccessWithErrorMsg(msg: String) {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(activity, msg, Toast.LENGTH_LONG).show()
    }

    override fun onUpdateDescFailure() {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(
            activity,
            getString(R.string.toast_msg_something_went_wrong),
            Toast.LENGTH_LONG
        ).show()
    }

    override fun onUploadAvatarSuccess() {
        Log.d(
            TAG,
            "onUploadAvatarSuccess() --> Image Uri: " + AppCoreCode.getInstance?.cache?.selectedNewAvatarUri
        )
        activity!!.runOnUiThread {
            if (imgProfileProviderAvatar != null)
                imgProfileProviderAvatar?.setImageURI(AppCoreCode.getInstance?.cache?.selectedNewAvatarUri)
            if (AppCoreCode.getInstance?.cache?.mainViewContext?.imgSideMenuProfilePhoto != null) {
                AppCoreCode.getInstance?.cache.mainViewContext?.imgSideMenuProfilePhoto!!.setImageURI(
                    AppCoreCode.getInstance?.cache?.selectedNewAvatarUri
                )
            }
        }
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        customEditAvatarDialog?.dismiss()
        Toast.makeText(activity, "Description has been uploaded successfully!", Toast.LENGTH_LONG)
            .show()
    }

    override fun onUploadAvatarSuccessWithErrorMsg(msg: String) {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(activity, msg, Toast.LENGTH_LONG).show()
    }

    override fun onUploadAvatarFailure() {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(
            activity,
            getString(R.string.toast_msg_something_went_wrong),
            Toast.LENGTH_LONG
        ).show()
    }
}
