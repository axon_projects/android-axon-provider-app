package com.axon.axonprovider.View.ProviderMainView.Fragments

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast

import com.axon.axonprovider.R
import kotlinx.android.synthetic.main.gallery_album_fragment.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.GridLayoutManager
import com.axon.axonprovider.Adapters.Adapters.GalleryAlbumAdapter
import com.axon.axonprovider.Global.AppCoreCode
import com.axon.axonprovider.Global.FetchPath
import com.axon.axonprovider.Presenter.GalleryAlbumFragmentPresenter
import com.axon.axonprovider.View.ProviderMainView.MainActivity
import de.hdodenhof.circleimageview.CircleImageView

class GalleryAlbumFragment : Fragment(), GalleryAlbumFragmentPresenter.View {
    var TAG = "GalleryAlbumFragment"
    var customEditGalleryDialog: Dialog? = null
    var imgUpdatePhotoAvatar: CircleImageView? = null
    var galleryAlbumFragmentPresenter: GalleryAlbumFragmentPresenter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.gallery_album_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        galleryAlbumFragmentPresenter = GalleryAlbumFragmentPresenter(this)
        setViewsActions()
        setupRecyclerViews()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            1002 -> if (resultCode == Activity.RESULT_OK) {
                AppCoreCode.getInstance?.cache?.selectedNewAvatarUri = data!!.data
                Log.d(
                    TAG,
                    "onActivityResult --> resultCode: " + requestCode + " - selectedImage: " + AppCoreCode.getInstance?.cache?.selectedNewAvatarUri
                )
                if (imgUpdatePhotoAvatar != null)
                    imgUpdatePhotoAvatar?.setImageURI(AppCoreCode.getInstance?.cache?.selectedNewAvatarUri)
            }
        }
    }

    private fun setViewsActions(){
        ibtnGalleryAlbumBack.setOnClickListener(View.OnClickListener {
            if (activity != null)
                (activity as MainActivity).changeFragment(9)
        })
        ibtGalleryAlbumAdd.setOnClickListener(View.OnClickListener {
            showCustomEditAvatar()
        })
    }

    private fun setupRecyclerViews() {
        /*init Services RecyclerView*/
        val gridLayoutManager = GridLayoutManager(context, 3)
        gridLayoutManager.orientation = LinearLayoutManager.VERTICAL
        recyclerViewGalleryAlbum.setLayoutManager(LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false))
        recyclerViewGalleryAlbum?.setLayoutManager(gridLayoutManager)
        var galleryAlbumAdapter = GalleryAlbumAdapter()
        recyclerViewGalleryAlbum?.setAdapter(galleryAlbumAdapter)
    }

    private fun showCustomEditAvatar() {
        customEditGalleryDialog = Dialog(activity!!)
        customEditGalleryDialog!!.setContentView(R.layout.edit_provider_avatar_custom_dialog)
        customEditGalleryDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val ibtnEditUpdatePhotoClose: ImageButton =
            customEditGalleryDialog!!.findViewById(R.id.ibtnEditUpdatePhotoClose)
        val btnEditUpdatePhotoSave: Button =
            customEditGalleryDialog!!.findViewById(R.id.btnEditUpdatePhotoSave)
        imgUpdatePhotoAvatar = customEditGalleryDialog!!.findViewById(R.id.imgUpdatePhotoAvatar)
        val btnUpdatePhotoUpload: Button =
            customEditGalleryDialog!!.findViewById(R.id.btnUpdatePhotoUpload)
        val txtUploadPhotoDialog: TextView =
            customEditGalleryDialog!!.findViewById(R.id.txtUploadPhotoDialog)

        txtUploadPhotoDialog.setText(resources.getString(R.string.custom_dialog_add_gallery_title))

        ibtnEditUpdatePhotoClose.setOnClickListener(View.OnClickListener { customEditGalleryDialog!!.dismiss() })

        btnEditUpdatePhotoSave.setOnClickListener(View.OnClickListener {
            if (AppCoreCode.getInstance?.cache?.selectedNewAvatarUri != null) {
                AppCoreCode.getInstance?.showLoadingDialog(activity)
                galleryAlbumFragmentPresenter?.postUploadGallery(
                    FetchPath.getPath(
                        getActivity(),
                        AppCoreCode.getInstance?.cache?.selectedNewAvatarUri
                    )
                )
            } else {
                Toast.makeText(activity, "Please select photo first!", Toast.LENGTH_LONG).show()
            }
        })

        btnUpdatePhotoUpload.setOnClickListener(View.OnClickListener {
            val pickPhoto = Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            )
            startActivityForResult(pickPhoto, 1002)//one can be replaced with any action code
        })
        customEditGalleryDialog!!.show()
    }

    override fun onUploadGallerySuccess() {
        galleryAlbumFragmentPresenter?.getProfileGallery()
    }

    override fun onUploadGallerySuccessWithErrorMsg(msg: String) {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(activity, msg, Toast.LENGTH_LONG).show()
    }

    override fun onUploadGalleryFailure() {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(
            activity,
            getString(R.string.toast_msg_something_went_wrong),
            Toast.LENGTH_LONG
        ).show()
    }

    override fun onGetGallerySuccess() {
        recyclerViewGalleryAlbum.adapter?.notifyDataSetChanged()
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        customEditGalleryDialog?.dismiss()
        Toast.makeText(activity, "Gallery has been added successfully!", Toast.LENGTH_LONG)
            .show()
    }

    override fun onGetGallerySuccessWithErrorMsg(msg: String) {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(activity, msg, Toast.LENGTH_LONG).show()
    }

    override fun onGetGalleryFailure() {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(
            activity,
            getString(R.string.toast_msg_something_went_wrong),
            Toast.LENGTH_LONG
        ).show()
    }
}
