package com.axon.axonprovider.View.ProviderMainView.Fragments

import `in`.galaxyofandroid.spinerdialog.OnSpinerItemClick
import `in`.galaxyofandroid.spinerdialog.SpinnerDialog
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.AsyncTask
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.axon.axonprovider.Adapters.Adapters.MemberViewDetailsAdapter
import com.axon.axonprovider.Global.AppCoreCode
import com.axon.axonprovider.Presenter.MemberViewDetailsFragmentPresenter

import com.axon.axonprovider.R
import com.axon.axonprovider.View.ProviderMainView.MainActivity
import kotlinx.android.synthetic.main.member_view_details_fragment.*
import java.util.ArrayList

/**
 * A simple [Fragment] subclass.
 */
class MemberViewDetailsFragment : Fragment(), MemberViewDetailsFragmentPresenter.View {
    private var customAddServiceDialog: Dialog? = null
    private var spinnerDialog: SpinnerDialog? = null
    private var providerServiceList: ArrayList<String>? = null
    private var memberViewDetailsAdapter: MemberViewDetailsAdapter? = null
    private var memberViewDetailsFragmentPresenter: MemberViewDetailsFragmentPresenter? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.member_view_details_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        memberViewDetailsFragmentPresenter = MemberViewDetailsFragmentPresenter(this)
        setViewsValues()
        setupRecyclerViews()
        setViewsActions()
    }

    private fun setViewsValues() {
        if (!AppCoreCode.getInstance?.cache?.getAppointmentInfoByIDResponse?.result?.memberAvatarUrl?.contains(
                "nophoto-slider"
            )!!
        ) {
            AsyncTask.execute {
                if (activity != null) {
                    activity!!.runOnUiThread {
                        imgMemberAvatar?.setImageURI(
                            AppCoreCode.getInstance?.cache?.mainURL
                                    + "/" + AppCoreCode.getInstance?.cache?.getAppointmentInfoByIDResponse?.result?.memberAvatarUrl
                        )
                    }
                }
            }
        }

        txtMemberName.setText(AppCoreCode.getInstance?.cache?.getAppointmentInfoByIDResponse?.result?.memberName)
        txtMemberGender.setText(AppCoreCode.getInstance?.cache?.getAppointmentInfoByIDResponse?.result?.genderName)
        txtMemberAge.setText(AppCoreCode.getInstance?.cache?.getAppointmentInfoByIDResponse?.result?.memberAge.toString())
        txtMemberShipID.setText(AppCoreCode.getInstance?.cache?.getAppointmentInfoByIDResponse?.result?.memberId.toString())
        txtMemberDate.setText(AppCoreCode.getInstance?.cache?.getAppointmentInfoByIDResponse?.result?.reservationDate)
        if (AppCoreCode.getInstance?.cache?.getAppointmentInfoByIDResponse?.result?.timeTo != null) {
            txtMemberTime.setText(AppCoreCode.getInstance?.cache?.getAppointmentInfoByIDResponse?.result?.timeFrom + " " + AppCoreCode.getInstance?.cache?.getAppointmentInfoByIDResponse?.result?.timeTo)
        } else {
            txtMemberTime.setText(AppCoreCode.getInstance?.cache?.getAppointmentInfoByIDResponse?.result?.timeFrom)
        }
        txtMemberCheckIn.setText(AppCoreCode.getInstance?.cache?.getAppointmentInfoByIDResponse?.result?.checkInTime)
        txtTotal.setText(AppCoreCode.getInstance?.cache?.getAppointmentInfoByIDResponse?.result?.reservationServices!![0]?.price.toString())
        txtTotalAfterDiscount.setText(AppCoreCode.getInstance?.cache?.getAppointmentInfoByIDResponse?.result?.reservationServices!![0]?.priceAfterDiscount.toString())
    }

    private fun setViewsActions() {
        ibtnProviderProfileBack.setOnClickListener(View.OnClickListener {
            if (activity != null)
                (activity as MainActivity).changeFragment(0)
        })
        ibtnAddService.setOnClickListener(View.OnClickListener {
            showAddServiceCustomDialog()
        })
        btnMemberViewDetailsSubmit.setOnClickListener(View.OnClickListener {
            if (AppCoreCode.getInstance?.cache?.getAppointmentInfoByIDResponse?.result?.reservationServices?.size!! > 0) {
                AppCoreCode.getInstance?.showLoadingDialog(activity)
                var singleAppointment =
                    AppCoreCode.getInstance.cache.getAppointmentInfoByIDResponse?.result?.reservationServices!![0]
                memberViewDetailsFragmentPresenter?.postCheckOutAppointment(
                    singleAppointment?.id.toString(),
                    singleAppointment?.price.toString(),
                    singleAppointment?.priceAfterDiscount.toString()
                )
            }
        })
    }

    private fun showAddServiceCustomDialog() {
        customAddServiceDialog = Dialog(activity!!)
        customAddServiceDialog!!.setContentView(R.layout.add_member_service_custom_dialog)
        customAddServiceDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        providerServiceList = ArrayList()
        for (result in AppCoreCode.getInstance?.cache?.getProviderServicesResponse?.result!!) {
            providerServiceList?.add(result?.name!!)
        }

        val ibtnMemberAddServiceClose: ImageButton =
            customAddServiceDialog!!.findViewById(R.id.ibtnMemberAddServiceClose)
        val btnMemberAddServiceSave: Button =
            customAddServiceDialog!!.findViewById(R.id.btnMemberAddServiceSave)
        val etxtMemberSelectService: TextView =
            customAddServiceDialog!!.findViewById(R.id.etxtMemberSelectService)
        val txtMemberServicePrice: TextView =
            customAddServiceDialog!!.findViewById(R.id.txtMemberServicePrice)
        val txtMemberServiceDiscountPercentage: TextView =
            customAddServiceDialog!!.findViewById(R.id.txtMemberServiceDiscountPercentage)
        val txtMemberServiceNetPrice: TextView =
            customAddServiceDialog!!.findViewById(R.id.txtMemberServiceNetPrice)

        ibtnMemberAddServiceClose.setOnClickListener(View.OnClickListener { customAddServiceDialog!!.dismiss() })
        btnMemberAddServiceSave.setOnClickListener(View.OnClickListener {
            if (etxtMemberSelectService.text.toString().trim().length > 0) {
                AppCoreCode.getInstance?.showLoadingDialog(activity)
                memberViewDetailsFragmentPresenter?.postAddServiceToReservation(
                    etxtMemberSelectService.tag.toString(),
                    txtMemberServicePrice.text.toString(),
                    txtMemberServiceDiscountPercentage.text.toString(),
                    AppCoreCode.getInstance?.cache?.getAppointmentInfoByIDResponse?.result?.id.toString()
                    )
            } else {
                etxtMemberSelectService.setError("Please select service!")
                etxtMemberSelectService.requestFocus()
                /*TODO: return*/
            }
        })

        etxtMemberSelectService.setOnClickListener(View.OnClickListener {
            showSpinnerDialog(
                "Select Service",
                etxtMemberSelectService,
                txtMemberServicePrice,
                txtMemberServiceDiscountPercentage,
                txtMemberServiceNetPrice,
                providerServiceList
            )
        })

        customAddServiceDialog!!.show()
    }

    private fun showSpinnerDialog(
        dialogTitle: String,
        etxtMemberSelectService: TextView,
        txtMemberServicePrice: TextView,
        txtMemberServiceDiscountPercentage: TextView,
        txtMemberServiceNetPrice: TextView,
        list: ArrayList<String>?
    ) {
        spinnerDialog =
            SpinnerDialog(
                activity,
                list,
                dialogTitle,
                R.style.DialogAnimations_SmileWindow,
                "Close"
            )// With 	Animation

        spinnerDialog?.setCancellable(true) // for cancellable
        spinnerDialog?.setShowKeyboard(false)// for open keyboard by default

        spinnerDialog?.bindOnSpinerListener(object : OnSpinerItemClick {
            override fun onClick(item: String, position: Int) {
                etxtMemberSelectService.text = item
                for (result in AppCoreCode.getInstance?.cache?.getProviderServicesResponse?.result!!) {
                    if (item.equals(result?.name)) {
                        etxtMemberSelectService.text = result?.name
                        etxtMemberSelectService.tag = result?.id
                        txtMemberServicePrice.text = result?.price.toString()
                        txtMemberServiceDiscountPercentage.text = result?.discountPercentage.toString()
                        txtMemberServiceNetPrice.text = result?.priceAfterDiscount.toString()
                        etxtMemberSelectService.setError(null)
                    }
                }
            }
        })

        spinnerDialog?.showSpinerDialog()
    }

    private fun setupRecyclerViews() {
        /*init Services RecyclerView*/
        recyclerViewServices?.setLayoutManager(
            LinearLayoutManager(
                activity,
                RecyclerView.VERTICAL,
                false
            )
        )
        memberViewDetailsAdapter = MemberViewDetailsAdapter()
        recyclerViewServices?.setAdapter(memberViewDetailsAdapter)
    }

    /*------------------------------------Check-Out Requests---------------------------------------*/
    override fun onCheckOutAppointmentSuccess() {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        if (activity != null)
            (activity as MainActivity).changeFragment(0)
    }

    override fun onCheckOutAppointmentSuccesWithErrorMsg(msg: String) {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(AppCoreCode.getInstance?.cache?.mainViewContext, msg, Toast.LENGTH_LONG)
            .show()
    }

    override fun onCheckOutAppointmentFailure() {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(
            AppCoreCode.getInstance?.cache?.mainViewContext,
            AppCoreCode.getInstance?.cache?.mainViewContext?.getString(R.string.toast_msg_something_went_wrong),
            Toast.LENGTH_LONG
        ).show()
    }

    override fun onAddServiceToReservationSuccess() {
        AppCoreCode.getInstance?.cache?.memberOperationType = "AddService"
        recyclerViewServices.adapter?.notifyDataSetChanged()
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        customAddServiceDialog!!.dismiss()
    }

    override fun onAddServiceToReservationSuccesWithErrorMsg(msg: String) {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(AppCoreCode.getInstance?.cache?.mainViewContext, msg, Toast.LENGTH_LONG)
            .show()
    }

    override fun onAddServiceToReservationFailure() {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(
            AppCoreCode.getInstance?.cache?.mainViewContext,
            AppCoreCode.getInstance?.cache?.mainViewContext?.getString(R.string.toast_msg_something_went_wrong),
            Toast.LENGTH_LONG
        ).show()
    }
}
