package com.axon.axonprovider.View.ProviderMainView.Fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Adapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.axon.axonprovider.Adapters.Adapters.BookingConfigSettingsAdapter
import com.axon.axonprovider.Adapters.Adapters.WorkingHoursAdapter

import com.axon.axonprovider.R
import com.axon.axonprovider.View.ProviderMainView.MainActivity
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.working_hours_setting_fragment.*

class WorkingHoursSettingFragment : Fragment() {
    private var tabLayoutWorkingHours: TabLayout? = null
    private var recyclerViewWorkingHours: RecyclerView? = null
    private var workingHoursAdapter: RecyclerView.Adapter<*>? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.working_hours_setting_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initRecyclerView()
        setViewsAction()
    }

    private fun initViews() {
        recyclerViewWorkingHours = view?.findViewById(R.id.recyclerViewWorkingHours)
        tabLayoutWorkingHours = view?.findViewById(R.id.tabLayoutWorkingHours)
    }

    private fun initRecyclerView() {
        /*init RecyclerView*/
        recyclerViewWorkingHours?.setLayoutManager(LinearLayoutManager(activity, RecyclerView.VERTICAL, false))
        workingHoursAdapter = WorkingHoursAdapter()
        recyclerViewWorkingHours?.setAdapter(workingHoursAdapter)
    }

    private fun setViewsAction() {
        ibtnWorkingHoursClose.setOnClickListener(View.OnClickListener {
            if (activity != null)
                (activity as MainActivity).changeFragment(0)
        })
        tabLayoutWorkingHours?.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                recyclerViewWorkingHours?.adapter?.notifyDataSetChanged()
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })
    }
}
