package com.axon.axonprovider.View.ProviderMainView.Fragments

import android.graphics.Color
import android.os.Bundle
import android.view.Gravity
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.GravityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.axon.axonprovider.Adapters.Adapters.LatestReviewsAdapter
import com.axon.axonprovider.Adapters.Adapters.TodayAppointmentAdapter
import com.axon.axonprovider.Global.AppCoreCode
import com.axon.axonprovider.Presenter.MainViewFragmentPresenter
import com.axon.axonprovider.R
import com.axon.axonprovider.View.ProviderMainView.MainActivity
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import com.github.mikephil.charting.formatter.LargeValueFormatter
import kotlinx.android.synthetic.main.main_vew_fragment.*

class MainVewFragment : Fragment(), MainViewFragmentPresenter.View {
    private var mainViewRecyclerViewNextAppointments: RecyclerView? = null
    private var nextAppointmentAdapter: RecyclerView.Adapter<*>? = null
    private var mainViewRecyclerViewLatestReviews: RecyclerView? = null
    private var latestReviewsAdapter: RecyclerView.Adapter<*>? = null
    private var mainViewFragmentPresenter: MainViewFragmentPresenter? = null

    private val TAG = "MainVewFragment"

    /*---------------------------------Fragment Override-------------------------------------------*/
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.main_vew_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainViewFragmentPresenter = MainViewFragmentPresenter(this)
        initViews()
        initLineChart()
        initRecyclerView()
        setValuesToView()
        setViewsAction()
    }

    override fun onStart() {
        super.onStart()
        mainViewFragmentPresenter?.getNotifications()
    }

    /*---------------------------------Methods-------------------------------------------*/
    private fun initViews() {
        mainViewRecyclerViewNextAppointments = view?.findViewById(R.id.mainViewRecyclerViewNextAppointments);
        mainViewRecyclerViewLatestReviews = view?.findViewById(R.id.mainViewRecyclerViewLatestReviews);
    }

    private fun setViewsAction(){
        btnMainViewAllAppointments.setOnClickListener(View.OnClickListener {
            if (activity != null)
                (activity as MainActivity).changeFragment(8)
        })
        btnMainViewAllReviews.setOnClickListener(View.OnClickListener {
            if (activity != null)
                (activity as MainActivity).changeFragment(5)
        })
        ibtnSideMenu.setOnClickListener(View.OnClickListener {
            if (activity != null)
                (activity as MainActivity).drawerLayout?.openDrawer(GravityCompat.START)
        })
        ibtnMainNotification.setOnClickListener(View.OnClickListener {
            if (activity != null)
                (activity as MainActivity).changeFragment(15)
        })
    }

    private fun setValuesToView() {
        txtMainViewTotalCount.setText(AppCoreCode.getInstance?.cache?.dashboardCountsResponse?.result?.providerTotalVisitors?.total.toString())
        rateBarMainViewRate.rating = AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerProfileData?.rate?.toFloat()!!
        txtMainViewRate.setText(AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerProfileData?.rate.toString())
    }

    /**
     * Ref -> http://developine.com/android-grouped-stacked-bar-chart-using-mpchart-kotlin/
     */
    private fun initLineChart() {
        /*val barWidth: Float
        val barSpace: Float
        val groupSpace: Float
        val groupCount = 12

        barWidth = 0.1f
        barSpace = 0.07f
        groupSpace = 0.56f*/

        var xAxisValues = ArrayList<String>()
        /*xAxisValues.add("")*/
        xAxisValues.add("Jan")
        xAxisValues.add("Feb")
        xAxisValues.add("Mar")
        xAxisValues.add("Apr")
        xAxisValues.add("May")
        xAxisValues.add("June")
        xAxisValues.add("Jul")
        xAxisValues.add("Aug")
        xAxisValues.add("Sep")
        xAxisValues.add("Oct")
        xAxisValues.add("Nov")
        xAxisValues.add("Dec")

        var yValueGroup1 = ArrayList<Entry>()

        // draw the graph
        var barDataSet1: LineDataSet

        yValueGroup1.add(
            BarEntry(
                0f,
                AppCoreCode.getInstance?.cache?.dashboardCountsResponse?.result?.providerVisitsCountPerMonth!![0]?.count?.toFloat()!!
            )
        )

        yValueGroup1.add(
            BarEntry(
                1f,
                AppCoreCode.getInstance?.cache?.dashboardCountsResponse?.result?.providerVisitsCountPerMonth!![1]?.count?.toFloat()!!
            )
        )

        yValueGroup1.add(
            BarEntry(
                2f,
                AppCoreCode.getInstance?.cache?.dashboardCountsResponse?.result?.providerVisitsCountPerMonth!![2]?.count?.toFloat()!!
            )
        )

        yValueGroup1.add(
            BarEntry(
                3f,
                AppCoreCode.getInstance?.cache?.dashboardCountsResponse?.result?.providerVisitsCountPerMonth!![3]?.count?.toFloat()!!
            )
        )

        yValueGroup1.add(
            BarEntry(
                4f,
                AppCoreCode.getInstance?.cache?.dashboardCountsResponse?.result?.providerVisitsCountPerMonth!![4]?.count?.toFloat()!!
            )
        )

        yValueGroup1.add(
            BarEntry(
                5f,
                AppCoreCode.getInstance?.cache?.dashboardCountsResponse?.result?.providerVisitsCountPerMonth!![5]?.count?.toFloat()!!
            )
        )

        yValueGroup1.add(
            BarEntry(
                6f,
                AppCoreCode.getInstance?.cache?.dashboardCountsResponse?.result?.providerVisitsCountPerMonth!![6]?.count?.toFloat()!!
            )
        )

        yValueGroup1.add(
            BarEntry(
                7f,
                AppCoreCode.getInstance?.cache?.dashboardCountsResponse?.result?.providerVisitsCountPerMonth!![7]?.count?.toFloat()!!
            )
        )

        yValueGroup1.add(
            BarEntry(
                8f,
                AppCoreCode.getInstance?.cache?.dashboardCountsResponse?.result?.providerVisitsCountPerMonth!![8]?.count?.toFloat()!!
            )
        )

        yValueGroup1.add(
            BarEntry(
                9f,
                AppCoreCode.getInstance?.cache?.dashboardCountsResponse?.result?.providerVisitsCountPerMonth!![9]?.count?.toFloat()!!
            )
        )

        yValueGroup1.add(
            BarEntry(
                10f,
                AppCoreCode.getInstance?.cache?.dashboardCountsResponse?.result?.providerVisitsCountPerMonth!![10]?.count?.toFloat()!!
            )
        )

        yValueGroup1.add(
            BarEntry(
                11f,
                AppCoreCode.getInstance?.cache?.dashboardCountsResponse?.result?.providerVisitsCountPerMonth!![11]?.count?.toFloat()!!
            )
        )

        barDataSet1 = LineDataSet(yValueGroup1, "Visitors")
        barDataSet1.setColors(Color.WHITE)
        /*barDataSet1.label = "2016"*/
        barDataSet1.setDrawIcons(false)
        barDataSet1.setDrawValues(false)

        var barData = LineData(barDataSet1)

        lineChartView.description.isEnabled = false
        lineChartView.description.textSize = 0f
        lineChartView.legend.isEnabled = false
        /*barData.setValueFormatter(LargeValueFormatter())*/
        lineChartView.setData(barData)
        /*lineChartView.getBarData().setBarWidth(barWidth)*/
        lineChartView.getXAxis().setAxisMinimum(0f)
        lineChartView.getXAxis()
            .setAxisMaximum(AppCoreCode.getInstance?.cache?.dashboardCountsResponse?.result?.providerTotalVisitors?.total?.toFloat()!!) //12f
        /*barChartView.groupBars(0f, groupSpace, barSpace)*/
        //   barChartView.setFitBars(true)
        lineChartView.getData().setHighlightEnabled(false)
        lineChartView.invalidate()

        // set bar label
        /* var legend = lineChartView.legend
         legend.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM)
         legend.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT)
         legend.setOrientation(Legend.LegendOrientation.HORIZONTAL)
         legend.setDrawInside(false)

         var legenedEntries = arrayListOf<LegendEntry>()

         legenedEntries.add(LegendEntry("Visitors", Legend.LegendForm.CIRCLE, 8f, 8f, null, Color.WHITE))
         legenedEntries.add(LegendEntry("2017", Legend.LegendForm.SQUARE, 8f, 8f, null, Color.YELLOW))

         legend.setCustom(legenedEntries)

         legend.setYOffset(2f)
         legend.setXOffset(2f)
         legend.setYEntrySpace(0f)
         legend.setTextSize(5f)*/

        val xAxis = lineChartView.getXAxis()
        xAxis.setGranularity(1f)
        xAxis.setGranularityEnabled(true)
        xAxis.setCenterAxisLabels(false)
        xAxis.setDrawGridLines(true)
        xAxis.textSize = 9f
        xAxis.textColor = Color.WHITE

        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM)
        xAxis.setValueFormatter(IndexAxisValueFormatter(xAxisValues))

        xAxis.setLabelCount(12)
        xAxis.mAxisMaximum = 12f
        xAxis.setCenterAxisLabels(false)
        xAxis.setAvoidFirstLastClipping(false)
        xAxis.spaceMin = 4f
        xAxis.spaceMax = 4f

        lineChartView.setVisibleXRangeMaximum(12f)
        lineChartView.setVisibleXRangeMinimum(12f)
        lineChartView.setDragEnabled(true)

        //Y-axis
        lineChartView.getAxisRight().setEnabled(false)
        lineChartView.setScaleEnabled(false)

        val leftAxis = lineChartView.getAxisLeft()
        /*leftAxis.setValueFormatter(LargeValueFormatter())*/
        leftAxis.setDrawGridLines(true)
        leftAxis.setSpaceTop(1f)
        leftAxis.setAxisMinimum(0f)
        leftAxis.textColor = Color.WHITE


        lineChartView.data = barData
        lineChartView.setVisibleXRange(1f, 12f)

    }

    private fun initRecyclerView() {
        /*init Next Appointments RecyclerView*/
        mainViewRecyclerViewNextAppointments?.setLayoutManager(
            LinearLayoutManager(
                activity,
                RecyclerView.VERTICAL,
                false
            )
        )
        nextAppointmentAdapter = TodayAppointmentAdapter()
        mainViewRecyclerViewNextAppointments?.setAdapter(nextAppointmentAdapter)
        /*init Latest Reviews RecyclerView*/
        mainViewRecyclerViewLatestReviews?.setLayoutManager(LinearLayoutManager(activity, RecyclerView.VERTICAL, false))
        latestReviewsAdapter = LatestReviewsAdapter()
        mainViewRecyclerViewLatestReviews?.setAdapter(latestReviewsAdapter)
    }

    override fun onNotificationSuccess() {
        AppCoreCode.getInstance?.cache?.notificationsResponse?.result?.forEach {result ->
            if(result?.state == 0){
                ibtnMainNotification.setImageResource(R.drawable.notification_numberd_icon)
                return
            }else{
                ibtnMainNotification.setImageResource(R.drawable.notification_icon)
            }
        }
    }

    override fun onNotificationSuccessWithErrorMsg(msg: String) {
        ibtnMainNotification.setImageResource(R.drawable.notification_icon)
    }

    override fun onNotificationFailure() {
        ibtnMainNotification.setImageResource(R.drawable.notification_icon)
    }
}
