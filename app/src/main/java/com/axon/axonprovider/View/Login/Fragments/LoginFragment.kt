package com.axon.axonprovider.View.Login.Fragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import com.axon.axonprovider.R
import kotlinx.android.synthetic.main.login_fragment.*
import com.axon.axonprovider.Global.AppCoreCode
import com.axon.axonprovider.Presenter.LoginFragmentPresenter
import com.axon.axonprovider.View.ProviderMainView.MainActivity

class LoginFragment : Fragment(), LoginFragmentPresenter.View {
    private val TAG = "LoginFragment"
    private var loginFragmentPresenter: LoginFragmentPresenter? = null

    /*---------------------------------Fragment Override-------------------------------------------*/
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.login_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loginFragmentPresenter = LoginFragmentPresenter(this)
        setViewsActions()
        checkIfUserAlreadyLoggedIn()
    }

    /*---------------------------------Methods-------------------------------------------*/
    private fun setViewsActions() {
        btnLoginLoading?.setOnClickListener(View.OnClickListener {
            validateUserInputs()
        })
    }

    private fun validateUserInputs() {
        if (txtLoginUsername.text.toString().trim().isEmpty()) {
            txtLoginUsername.requestFocus()
            txtLoginUsername.setError(getResources().getString(R.string.view_error_username))
            return
        }
        if (txtLoginPassword.text.toString().trim().isEmpty()) {
            txtLoginPassword.requestFocus()
            txtLoginPassword.setError(getResources().getString(R.string.view_error_username))
            return
        }
        startLoginOperation(txtLoginUsername.text.toString(), txtLoginPassword.text.toString())
    }

    private fun checkIfUserAlreadyLoggedIn() {
        if (!AppCoreCode.getInstance?.getStringValuesFromSharedPreferences("Username").equals("")) {
            txtLoginUsername.setText(AppCoreCode.getInstance?.getStringValuesFromSharedPreferences("Username").toString())
            txtLoginPassword.setText(AppCoreCode.getInstance?.getStringValuesFromSharedPreferences("Password").toString())
            startLoginOperation(
                AppCoreCode.getInstance?.getStringValuesFromSharedPreferences("Username").toString(),
                AppCoreCode.getInstance?.getStringValuesFromSharedPreferences("Password").toString()
            )
        }
    }

    private fun startLoginOperation(username: String, password: String) {
        AppCoreCode.getInstance?.showLoadingDialog(activity)
        loginFragmentPresenter?.getLoginAuthFromServer(
            "",
            username,
            password,
            activity
        )
    }

    private fun saveLoginInfoIntoSavedPreferences() {
        if (AppCoreCode.getInstance?.editor != null) {
            AppCoreCode.getInstance?.saveValuesToSharedPreferences(
                "Username",
                txtLoginUsername.text.toString()
            )
            AppCoreCode.getInstance?.saveValuesToSharedPreferences(
                "Password",
                txtLoginPassword.text.toString()
            )
        }
    }

    /*---------------------------------Presenter Override Methods-------------------------------------------*/
    override fun onLoginSuccess() {
        // TODO: Call Home data here
        /*loginFragmentPresenter?.getNotifications()*/
        loginFragmentPresenter?.postPreviousAppointments()
        saveLoginInfoIntoSavedPreferences()
    }

    override fun onLoginSuccessWithErrorMsg(msg: String) {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(activity, msg, Toast.LENGTH_LONG).show()
    }

    override fun onLoginFailure() {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(
            activity,
            getString(R.string.toast_msg_something_went_wrong),
            Toast.LENGTH_LONG
        ).show()
    }

/*    override fun onNotificationSuccess() {
        loginFragmentPresenter?.postPreviousAppointments()
    }

    override fun onNotificationSuccessWithErrorMsg(msg: String) {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(activity, msg, Toast.LENGTH_LONG).show()
    }

    override fun onNotificationFailure() {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(
            activity,
            getString(R.string.toast_msg_something_went_wrong),
            Toast.LENGTH_LONG
        ).show()
    }*/

    override fun onPreviousAppointmentsSuccess() {
        loginFragmentPresenter?.postNextAppointments()
    }

    override fun onPreviousAppointmentsSuccessWithErrorMsg(msg: String) {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(activity, msg, Toast.LENGTH_LONG).show()
    }

    override fun onPreviousAppointmentsFailure() {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(
            activity,
            getString(R.string.toast_msg_something_went_wrong),
            Toast.LENGTH_LONG
        ).show()
    }

    override fun onNextAppointmentsSuccess() {
        loginFragmentPresenter?.postAllReviews()
    }

    override fun onNextAppointmentsSuccessWithErrorMsg(msg: String) {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(activity, msg, Toast.LENGTH_LONG).show()
    }

    override fun onNextAppointmentsFailure() {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(
            activity,
            getString(R.string.toast_msg_something_went_wrong),
            Toast.LENGTH_LONG
        ).show()
    }

    override fun onAllReviewsSuccess() {
        loginFragmentPresenter?.getProfile()
    }

    override fun onAllReviewsSuccessWithErrorMsg(msg: String) {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(activity, msg, Toast.LENGTH_LONG).show()
    }

    override fun onAllReviewsFailure() {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(
            activity,
            getString(R.string.toast_msg_something_went_wrong),
            Toast.LENGTH_LONG
        ).show()
    }

    override fun onProfileSuccess() {
        loginFragmentPresenter?.getDashboardCounts()
    }

    override fun onProfileSuccessWithErrorMsg(msg: String) {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(activity, msg, Toast.LENGTH_LONG).show()
    }

    override fun onProfileFailure() {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(
            activity,
            getString(R.string.toast_msg_something_went_wrong),
            Toast.LENGTH_LONG
        ).show()
    }

    override fun onDashboardCountsSuccess() {
        loginFragmentPresenter?.getTodayAppointments()
    }

    override fun onDashboardCountsSuccessWithErrorMsg(msg: String) {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(activity, msg, Toast.LENGTH_LONG).show()
    }

    override fun onDashboardCountsFailure() {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(
            activity,
            getString(R.string.toast_msg_something_went_wrong),
            Toast.LENGTH_LONG
        ).show()
    }

    override fun onTodayAppointmentsSuccess() {
        loginFragmentPresenter?.getLatestReviews()
    }

    override fun onTodayAppointmentsSuccesWithErrorMsg(msg: String) {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(activity, msg, Toast.LENGTH_LONG).show()
    }

    override fun onTodayAppointmentsFailure() {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(
            activity,
            getString(R.string.toast_msg_something_went_wrong),
            Toast.LENGTH_LONG
        ).show()
    }

    override fun onLatestReviewsSuccess() {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        startActivity(Intent(activity, MainActivity::class.java))
    }

    override fun onLatestReviewsSuccesWithErrorMsg(msg: String) {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(activity, msg, Toast.LENGTH_LONG).show()
    }

    override fun onLatestReviewsFailure() {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(
            activity,
            getString(R.string.toast_msg_something_went_wrong),
            Toast.LENGTH_LONG
        ).show()
    }
}
