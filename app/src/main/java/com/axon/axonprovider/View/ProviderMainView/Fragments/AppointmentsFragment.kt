package com.axon.axonprovider.View.ProviderMainView.Fragments

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.axon.axonprovider.Adapters.Adapters.AppointmentsAdapter
import com.axon.axonprovider.Global.AppCoreCode
import com.axon.axonprovider.Presenter.AppointmentsFragmentPresenter

import com.axon.axonprovider.R
import com.axon.axonprovider.View.ProviderMainView.MainActivity
import com.google.android.material.tabs.TabLayout
import com.philliphsu.bottomsheetpickers.date.DatePickerDialog
import kotlinx.android.synthetic.main.appointments_fragment.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class AppointmentsFragment : Fragment(), AppointmentsFragmentPresenter.View, DatePickerDialog.OnDateSetListener {
    private val TAG = "AppointmentsFragment"
    private var appointmentsFragmentPresenter: AppointmentsFragmentPresenter? = null
    private var appointmentsAdapter: RecyclerView.Adapter<*>? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.appointments_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        appointmentsFragmentPresenter = AppointmentsFragmentPresenter(this)

        etxtAppointmentsFilterByMembershipID.setText("")
        txtAppointmentsFilterByDate.setText("")

        initRecyclerView()
        setViewsAction()
    }

    override fun onStart() {
        super.onStart()
        tabLayoutAppointments.getTabAt(0)?.customView?.isSelected = true
        AppCoreCode.getInstance?.cache?.isPastAppointmentTabSelected = false
        notifyRecyclerViewOnDataChanged()
    }

    private fun initRecyclerView() {
        /*init Appointments RecyclerView*/
        recyclerViewAppointments?.setLayoutManager(
            LinearLayoutManager(
                activity,
                RecyclerView.VERTICAL,
                false
            )
        )
        appointmentsAdapter = AppointmentsAdapter()
        recyclerViewAppointments?.setAdapter(appointmentsAdapter)
    }

    private fun notifyRecyclerViewOnDataChanged(){
        recyclerViewAppointments.recycledViewPool.clear()
        recyclerViewAppointments?.adapter?.notifyDataSetChanged()
    }

    private fun setViewsAction() {
        ibtnCheckInClose.setOnClickListener(View.OnClickListener {
            if (activity != null)
                (activity as MainActivity).changeFragment(0)
        })
        tabLayoutAppointments?.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                if (tab.position == 0) {
                    AppCoreCode.getInstance?.cache?.isPastAppointmentTabSelected = false
                } else {
                    AppCoreCode.getInstance?.cache?.isPastAppointmentTabSelected = true
                }
                notifyRecyclerViewOnDataChanged()
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })

        imgAppointmentsClearDateFilter.setOnClickListener(View.OnClickListener {
            txtAppointmentsFilterByDate.setText("")
            AppCoreCode.getInstance?.showLoadingDialog(activity)
            if (AppCoreCode.getInstance?.cache?.isPastAppointmentTabSelected == true) {
                appointmentsFragmentPresenter?.postPreviousAppointments(
                    etxtAppointmentsFilterByMembershipID.text.toString(), ""
                )
            } else {
                appointmentsFragmentPresenter?.postNextAppointments(
                    etxtAppointmentsFilterByMembershipID.text.toString(),
                    ""
                )
            }
        })

        imgAppointmentsClearMembershipIDFilter.setOnClickListener(View.OnClickListener {
            etxtAppointmentsFilterByMembershipID.setText("")
            AppCoreCode.getInstance?.showLoadingDialog(activity)
            if (AppCoreCode.getInstance?.cache?.isPastAppointmentTabSelected == true) {
                appointmentsFragmentPresenter?.postPreviousAppointments(
                    "", txtAppointmentsFilterByDate.text.toString()
                )
            } else {
                appointmentsFragmentPresenter?.postNextAppointments(
                    "",
                    txtAppointmentsFilterByDate.text.toString()
                )
            }
        })

        txtAppointmentsFilterByDate.setOnClickListener(View.OnClickListener {
            showCustomDatePickerDialog()
        })

        etxtAppointmentsFilterByMembershipID.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER || actionId == EditorInfo.IME_ACTION_DONE) {
                txtAppointmentsFilterByDate.setText("")
                AppCoreCode.getInstance?.showLoadingDialog(activity)
                if (AppCoreCode.getInstance?.cache?.isPastAppointmentTabSelected == true) {
                    appointmentsFragmentPresenter?.postPreviousAppointments(
                        etxtAppointmentsFilterByMembershipID.text.toString(),
                        txtAppointmentsFilterByDate.text.toString()
                    )
                } else {
                    appointmentsFragmentPresenter?.postNextAppointments(
                        etxtAppointmentsFilterByMembershipID.text.toString(),
                        txtAppointmentsFilterByDate.text.toString()
                    )
                }
            }
            false
        })
    }

    private fun showCustomDatePickerDialog() {
        var date1: SimpleDateFormat? = null
        val now = Calendar.getInstance()
        try {
            date1 = SimpleDateFormat("yyyy-MM-dd")
            if (date1 != null) {
                now.time = date1.parse(txtAppointmentsFilterByDate.getText().toString())
            }
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        val date = DatePickerDialog.Builder(
            this,
            now.get(Calendar.YEAR),
            now.get(Calendar.MONTH),
            now.get(Calendar.DAY_OF_MONTH)
        ).build()
        date.setHeaderTextColorSelected(Color.parseColor("#FFFFFF"))
        date.setHeaderTextColorUnselected(Color.parseColor("#FFFFFF"))
        date.setDayOfWeekHeaderTextColorSelected(Color.parseColor("#FFFFFF"))
        date.setDayOfWeekHeaderTextColorUnselected(Color.parseColor("#FFFFFF"))

        date.setAccentColor(Color.parseColor("#171743"))
        date.setBackgroundColor(Color.parseColor("#FFFFFF"))
        date.setHeaderColor(Color.parseColor("#171743"))
        date.setHeaderTextDark(false)
        date.show(activity?.supportFragmentManager, TAG)
    }

    /**
     * @param dialog The dialog associated with this listener.
     * @param year The year that was set.
     * @param monthOfYear The month that was set (0-11) for compatibility
     * with [Calendar].
     * @param dayOfMonth The day of the month that was set.
     */
    override fun onDateSet(dialog: DatePickerDialog?, year: Int, monthOfYear: Int, dayOfMonth: Int) {
        var strMonth = (monthOfYear + 1).toString()
        var strDays = dayOfMonth.toString()
        if (monthOfYear + 1 < 10) {
            strMonth = "0" + (monthOfYear + 1)
        }
        if (dayOfMonth < 10) {
            strDays = "0$dayOfMonth"
        }
        txtAppointmentsFilterByDate.setText("$year-$strMonth-$strDays")
        etxtAppointmentsFilterByMembershipID.setText("")
        AppCoreCode.getInstance?.showLoadingDialog(activity)
        if (AppCoreCode.getInstance?.cache?.isPastAppointmentTabSelected == true) {
            appointmentsFragmentPresenter?.postPreviousAppointments(
                etxtAppointmentsFilterByMembershipID.text.toString(),
                txtAppointmentsFilterByDate.text.toString()
            )
        } else {
            appointmentsFragmentPresenter?.postNextAppointments(
                etxtAppointmentsFilterByMembershipID.text.toString(),
                txtAppointmentsFilterByDate.text.toString()
            )
        }

        dialog?.dismiss()
    }

    override fun onPreviousAppointmentsSuccess() {
        notifyRecyclerViewOnDataChanged()
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
    }

    override fun onPreviousAppointmentsSuccessWithErrorMsg(msg: String) {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(activity, msg, Toast.LENGTH_LONG).show()
    }

    override fun onPreviousAppointmentsFailure() {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(activity, getString(R.string.toast_msg_something_went_wrong), Toast.LENGTH_LONG).show()
    }

    override fun onNextAppointmentsSuccess() {
        notifyRecyclerViewOnDataChanged()
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
    }

    override fun onNextAppointmentsSuccessWithErrorMsg(msg: String) {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(activity, msg, Toast.LENGTH_LONG).show()
    }

    override fun onNextAppointmentsFailure() {
        AppCoreCode.getInstance?.sweetAlertDialog?.dismissWithAnimation()
        Toast.makeText(activity, getString(R.string.toast_msg_something_went_wrong), Toast.LENGTH_LONG).show()
    }
}
