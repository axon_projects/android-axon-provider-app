package com.axon.axonprovider.View.ProviderMainView.Fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.ViewPager
import com.axon.axonprovider.Adapters.PagerAdapters.ViewPagerAdapter
import com.axon.axonprovider.Global.AppCoreCode

import com.axon.axonprovider.R
import kotlinx.android.synthetic.main.image_viewer_fragment.*

class ImageViewerFragment : Fragment() {
    private val TAG = "ImageViewerFragment"
    private var viewPager: ViewPager? = null
    private var adapter: ViewPagerAdapter? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.image_viewer_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d(TAG, "onViewCreated() --> Called")
        initViews()
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser && view != null) {
            initViews()
        }
    }

    private fun initViews() {
        viewPager = view?.findViewById(R.id.viewPagerImageViewer)

        setViewActions()
    }

    private fun setupViewPager() {
        Log.d(TAG, "setupViewPager() --> Called")
        viewPager?.setAdapter(adapter)
        viewPager?.setCurrentItem(AppCoreCode.getInstance?.cache?.currentImageSliderIndex!!)
        Log.d(
            TAG,
            "setupViewPager() --> current index: " + AppCoreCode.getInstance?.cache?.currentImageSliderIndex
        )
    }

    private fun setViewActions() {
        Log.d(TAG, "setViewActions() --> Called")
        Log.d(
            TAG,
            "setViewActions() --> currentImageSlider: " + AppCoreCode.getInstance?.cache?.currentImageSlider
        )
        if (AppCoreCode.getInstance?.cache?.currentImageSlider.equals("Gallery")) {
            Log.d(TAG, "setViewActions() --> Gallery")
            if (txtDescription != null)
                txtDescription.setVisibility(View.GONE)
            adapter =
                ViewPagerAdapter(
                    this!!.activity!!,
                    AppCoreCode.getInstance?.cache?.profileResponse?.result?.mediaFiles!!, null
                )
        } else if (AppCoreCode.getInstance?.cache?.currentImageSlider.equals("Offers")) {
            Log.d(TAG, "setViewActions() --> Offers")
            txtDescription.setVisibility(View.VISIBLE)
            adapter = ViewPagerAdapter(
                this!!.activity!!,
                AppCoreCode.getInstance?.cache?.profileResponse?.result?.providerOffers!!,
                txtDescription
            )
            if (AppCoreCode.getInstance?.cache?.profileResponse?.result!!.providerOffers!!.size > 0 && AppCoreCode.getInstance?.cache?.profileResponse?.result!!.providerOffers?.get(
                    AppCoreCode.getInstance?.cache?.currentImageSliderIndex!!
                ) != null
            ) {
                txtDescription.setText(
                    AppCoreCode.getInstance?.cache.profileResponse?.result?.providerOffers?.get(
                        AppCoreCode.getInstance?.cache?.currentImageSliderIndex
                    )!!.description
                )
            }

            viewPager?.setOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                override fun onPageScrolled(i: Int, v: Float, i1: Int) {

                }

                override fun onPageSelected(i: Int) {
                    if (AppCoreCode.getInstance?.cache.profileResponse!!.result != null
                        && AppCoreCode.getInstance?.cache.profileResponse!!.result!!.providerOffers != null
                        && AppCoreCode.getInstance?.cache.profileResponse!!.result!!.providerOffers?.size!! > 0
                    ) {
                        txtDescription.setText(
                            AppCoreCode.getInstance?.cache.profileResponse!!.result!!.providerOffers?.get(
                                i
                            )?.description
                        )
                    }
                }

                override fun onPageScrollStateChanged(i: Int) {

                }
            })
        }
        setupViewPager()
    }
}
